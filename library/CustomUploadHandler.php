<?php
/**
 * Class and Function List:
 * Function list:
 * - initialize()
 * - handle_form_data()
 * - handle_file_upload()
 * - set_additional_file_properties()
 * - delete()
 * Classes list:
 * - CustomUploadHandler extends UploadHandler
 */
require ('UploadHandler.php');
require_once ("Zend/Loader/Autoloader.php");

class CustomUploadHandler extends UploadHandler
{
    
    protected function initialize() 
    {
        Zend_Loader_Autoloader::getInstance();
        $this->db  = Zend_Registry::get('db');
        $this->tbl = new Zend_Db_Table(array('name' => 'f_files', 'primary' => 'f_id'));
        
        parent::initialize();
    }
    
    protected function handle_form_data($file, $index) 
    {
        $file->title       = @$_REQUEST['title'][$index];
        $file->description = @$_REQUEST['description'][$index];
        $file->ucid        = @$_REQUEST['upload_ucid'];
        $file->uid         = @$_REQUEST['upload_id'];
    }
    
    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index             = null, $content_range     = null) 
    {
        $file              = parent::handle_file_upload($uploaded_file, $name, $size, $type, $error, $index, $content_range);
        
        if (empty($file
            ->error)) 
        {
            //$this->tbl         = new Zend_Db_Table(array('name'                   => 'f_files', 'primary'                   => 'f_id'));
            
            $db_array          = array('f_datetime'                   => date('Y-m-d H:i:s'), 
                'f_ucid'                   => $file
                ->ucid, 'f_uid'                   => $file
                ->uid, 'f_name'                   => $file
                ->name, 'f_size'                   => $file
                ->size, 'f_type'                   => $file
                ->type, 'f_description'                   => $file->description,);
            
            $tbl_row           = $this->tbl->createRow($db_array);
            $tbl_row->save();
        }
        
        return $file;
    }
    
    protected function set_additional_file_properties($file) 
    {
        parent::set_additional_file_properties($file);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') 
        {
            
            /*            $sql   = 'SELECT `id`, `type`, `title`, `description` FROM `' . $this->options['db_table'] . '` WHERE `name`=?';
                                                $query = $this
                                                    ->db
                                                    ->prepare($sql);
                                                $query
                                                    ->bind_param('s', $file->name);
                                                $query->execute();
                                                $query->bind_result($id, $type, $title, $description);
                                                while ($query
                                                    ->fetch()) 
                                                {
                                                    $file->id          = $id;
                                                    $file->type        = $type;
                                                    $file->title       = $title;
                                                    $file->description = $description;
                                                }
            */
        }
    }
    
    public function delete($print_response = true) 
    {
        $response       = parent::delete(false);

        foreach ($response as $name => $deleted) 
        {
            if ($deleted) 
            {
                $this->tbl->delete(array('f_name = ?' => $name));
            }
        }
        return $this->generate_response($response, $print_response);
    }
}
