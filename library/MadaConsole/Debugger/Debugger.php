<?php
class Debugger {
	protected $sqlMessages = array ();
	protected $sqlMessagesPrevious = array ();
	protected $debugMessages = array ();
	protected $debugMessagesPrevious = array ();
	protected $errorMessages = array ();
	protected $errorMessagesPrevoius = array ();
	
	protected $session;
	protected $logToFile = false;
	
	protected $timeStart;
	protected static $instance = null;
	
	/**
	 * Get a Debugger singleton instance.
	 *
	 * @return Debugger
	 */
	static public function getInstance() {
		if (self::$instance == null)
			self::$instance = new Debugger ( );
		return self::$instance;
	}
	protected function __construct() {
		$this->instance = $this;
		$this->session = new Zend_Session_Namespace ( 'log' );
		
		$this->sqlMessagesPrevious = $this->def( $this->session->sqlMessages, array () );
		$this->session->sqlMessages = array ();
		$this->debugMessagesPrevious = $this->def( $this->session->debugMessages, array () );
		$this->session->debugMessages = array ();
		$this->errorMessagesPrevious = $this->def( $this->session->errorMessages, array () );
		$this->session->errorMessages = array ();
		
		if ($this->logToFile)
			$this->fp = fopen ( 'log.txt', "w" );
		
		$this->timeStart = microtime ();
	}
	
	public function debugSql($query, $time = null) {
		if($time == null) {
			$this->_debug ( $query, 'sqlMessages' );
		}
		else {
			$this->_debug ( (round ( $time, 4 ) * 10000) . 'ms  - ' . $query, 'sqlMessages' );
		}
	}
	
	public function debug($msg) {
		$this->_debug ( $msg, 'debugMessages' );
	}
	
	public function error($msg) {
		$this->_debug ( $msg, 'errorMessages' );
	}
	
	public static function vardump($msg) {
		$dump = '';
		if(is_bool($msg)) {
			if($msg === false) $dump .= 'FALSE';
			else $dump .= 'TRUE';
		}
		else if ($msg instanceof Zend_Db_Select) {
			$dump .= "$msg";
		}
		else if ($msg instanceof Zend_Db_Table_Row_Abstract) {
			$dump .= $msg->getTable()->info(Zend_Db_Table_Abstract::NAME ) . ' row: ' . print_r($msg->toArray(), true );
		}
		else if ($msg instanceof Zend_Db_Table_Rowset_Abstract) {
			$dump .= $msg->getTable()->info(Zend_Db_Table_Abstract::NAME ) . ' rowset: ';
			foreach ($msg as $v) {
				$dump .= print_r($v->toArray(), true ) . '<br/>';
			}
		}
		else {
			$dump .= print_r ( $msg, true );
			//$dump .= Zend_Debug::dump($msg, null, false);
		}
		return $dump;
	}
	
	protected function _debug($msg, $type) {
		$dump = "<code>" . date ( "h:i:s", time () ) . "." . (round ( microtime (), 4 ) * 10000) . "</code> ";
		
		$dump .= self::vardump($msg);
		
		array_push ( $this->$type, $dump );
		$this->session->$type = $this->$type;
		
		if ($this->logToFile) {
			fwrite ( $this->fp, $dump . "\n" );
			fflush ( $this->fp );
		}
	}
	
	public function printMessages() {
		$debug = "<div style='font-size: 12px; text-align: left; font-weight: bold;'>";
		
		if (! empty ( $this->errorMessagesPrevious )) {
			$debug .= "<br /><div style=\"background-color: #995555;\">Errors from previous page:</div>";
			foreach ( $this->errorMessagesPrevious as $msg ) {
				$debug .= "<div style='background-color: #dddddd; margin: 5px;'>" . $msg . "</div>";
			}
		}
		
		if (! empty ( $this->debugMessagesPrevious )) {
			$debug .= "<br /><div style='background-color: #cccccc;'>Debug from previous page:</div>";
			foreach ( $this->debugMessagesPrevious as $msg ) {
				$debug .= "<div style='background-color: #dddddd; margin: 5px;'>" . $msg . "</div>";
			}
		}
		
		if (! empty ( $this->sqlMessagesPrevious )) {
			$debug .= "<br /><div style='background-color: #cccccc;'>Query log from previous page:</div>";
			foreach ( $this->sqlMessagesPrevious as $msg ) {
				$debug .= "<div style='background-color: #dddddd; margin: 5px;'>" . $msg . "</div>";
			}
		}
		
		if (! empty ( $this->errorMessages )) {
			$debug .= "<br /><div style='background-color: #995555;'>Errors:</div>";
			foreach ( $this->errorMessages as $msg ) {
				$debug .= "<div style='background-color: #dddddd; margin: 5px;'>" . $msg . "</div>";
			}
		}
		
		if (! empty ( $this->debugMessages )) {
			$debug .= "<br /><div style='background-color: #999999;'>Debug:</div>";
			foreach ( $this->debugMessages as $msg ) {
				$debug .= "<div style='background-color: #dddddd; margin: 5px;'>" . $msg . "</div>";
			}
		}
		
		if (! empty ( $this->sqlMessages )) {
			$debug .= "<br /><div style=\"background-color: #999999;\">Query log:</div>";
			foreach ( $this->def( $this->sqlMessages, array () ) as $msg ) {
				$debug .= "<div style='background-color: #dddddd; margin: 5px;'>" . $msg . "</div>";
			}
		}
		
		$debug .= "<br /><div style='background-color: #999999;'>Post Request:</div>";
		foreach ( $this->def( $_POST, array () ) as $k => $v ) {
			$debug .= "<div style='background-color: #dddddd; margin: 5px;'>" . $k . ": " . $v . "</div>";
		}
		$debug .= "<br /><div style='background-color: #999999;'>Session:</div>";
		foreach ( $this->def( $_SESSION ['Default'], array () ) as $k => $v ) {
			$debug .= "<div style='background-color: #dddddd; margin: 5px;'>" . $k . ": " . print_r ( $v, true ) . "</div>";
		}
		$debug .= "<br /><div style='background-color: #999999;'>Cookie:</div>";
		foreach ( $this->def( $_COOKIE, array () ) as $k => $v ) {
			$debug .= "<div style='background-color: #dddddd; margin: 5px;'>" . $k . ": " . $v . "</div>";
		}
		
		$debug .= "<div style='background-color: #bbdddd; text-align: center'>" . count ( $this->sqlMessages ) . " query in " . (round ( microtime () - $this->timeStart, 4 ) * 10000) . "ms - Enviroment: ". APPLICATION_ENV ." - MadaConsole </div>";
		
		$this->session->sqlMessages = array ();
		$this->session->debugMessages = array ();
		$this->session->errorMessages = array ();
		return $debug . "</div>";
	}
	
	function __destruct() {
		if ($this->logToFile)
			fclose ( $this->fp );
	}

	private function def($value, $default) {
		if(empty($value)) return $default;
		else return $value;
	}
}
?>
