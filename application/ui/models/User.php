<?
/**
 * Class and Function List:
 * Function list:
 * - getItems()
 * - updateUserLoginTime()
 * - getMultiselect()
 * Classes list:
 * - Ui_Model_User extends Zend_Db_Table_Abstract
 */
class Ui_Model_User extends Zend_Db_Table_Abstract {
	
	##########################################################################################
	public $_name = 'u_user';
	public $_primary = 'u_id';
	
	##########################################################################################
	public function getItems() {
		$select = $this->select();
		$select->order(array(
			'u_username',
			'u_lastname',
			'u_firstname'
		));
		return $this->fetchAll($select);
	}
	
	##########################################################################################
	public function updateUserLoginTime($userId) {
		
		// fetch the user's row
		$row = $this
			->find($userId)->current();
		$row->u_lastlogin = date('Y-m-d H:i:s');
		$row->save();
		
		return $row;
	}
	
	##########################################################################################
	public function getMultiselect($u_role) {
		$res = array();
		
		$select = $this->select('u_id', 'u_firstname');
		$select->where('u_role = ?', ucfirst($u_role));
		$items = $this
			->fetchAll($select)->toArray();
		
		foreach ($items as $id => $val) $res[$val['u_id']] = $val['u_firstname'];
		
		return $res;
	}
}
