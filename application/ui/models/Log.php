<?php
require_once("Util.php");

class Ui_Model_Log extends Zend_Db_Table_Abstract {

    public $_name       = 'l_log';
    public $_primary    = 'l_id';

    public function init()
    {
    }
    
    public function write( $params = array(), $raw = array() )
    {
        if(!empty($params))
        {
            $row = $this->createRow();
            if($row) 
            {
                foreach($params as $p_id => $p_val)
                {
                    #$row_name = "l_". 
                    $row->{"l_".$p_id} = $p_val;    
                }
                
                list($usec, $sec) = explode(" ", microtime()); 
                
                $request = Zend_Controller_Front::getInstance()->getRequest();
                $auth = Zend_Auth::getInstance();                
                
                $raw = json_encode($raw);

                if($params["result"] == "sql")
                {
                    $raw = trim($raw);
                }

                $row->l_date = date ( 'Y-m-d H:i:s' );
                $row->l_msec = (float)$usec + (float)$sec; 
                $row->l_ip = Util::getIp();
                $row->l_useragent = Util::getUserAgent();
                $row->l_url = $_SERVER["REQUEST_URI"];
                $row->l_module = MODULE_NAME;
                $row->l_action = $request->getActionName();
                $row->l_controller = $request->getControllerName();
                $row->l_username = ($auth->hasIdentity()) ? $auth->getIdentity()->u_username : "";
                $row->l_object = json_encode($request->getParams());
                $row->l_raw = $raw;
                
                $row->save();
                
                return $row;
                
            } else {
                throw new Zend_Exception("Could not create item!");
            }
        }
        
    }        

}  
?>
