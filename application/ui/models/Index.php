<?php
/**
 * Class and Function List:
 * Function list:
 * - init()
 * - getSmartfiltersNavbar()
 * - getSmartFilters()
 * - getSmartFilterID()
 * - getSmartfilters()
 * - countObjectsInSmartfilter()
 * - getObjectsInSmartFilter()
 * - getSmartfilterTypes()
 * - getObjectTypes()
 * Classes list:
 * - Ui_Model_Index extends Zend_Db_Table_Abstract
 */
class Ui_Model_Index extends Zend_Db_Table_Abstract
{
    
    public $_name = 'sf_smartfilters';
    public $_primary = 'sf_id';
    
    //#########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get('db');
        $locale = new Zend_Session_Namespace('locale');
        
        $this->lang = $locale->curlocale['lang'];
    }
    
    //#########################################################################################
    public function getSmartsNavbar()
    {
        $result = array();

        $db = Zend_Registry::get( 'db' );

        $data = $db->query("
            SELECT * 
            FROM sf_smartfilters AS sf
            LEFT JOIN ot_object_type AS ot
            ON sf.ot_id = ot.ot_id
            WHERE sf.sf_enable=1
            ORDER BY ot.ot_order, sf.sf_order_navbar
        ")->fetchAll();

        foreach($data as $d_id => $d_val)
        {
            $name = ( !empty($d_val["sf_name_navbar_" . $this->lang]) ) ? $d_val["sf_name_navbar_" . $this->lang] : $d_val["sf_name_" . $this->lang]; 

            $result[ $d_val["ot_name_" . $this->lang] ][] = array(
                "uid" => $d_val["sf_uid"],
                "name" => $name,
                "objects" => $d_val["sf_objects"],
                "minprice" => $d_val["sf_minprice"],
                "maxprice" => $d_val["sf_maxprice"],
            );   
        }

        return $result;
    }


    //#########################################################################################
    public function getSmartsPanel()
    {
        $result = array();

        $db = Zend_Registry::get( 'db' );

        $data = $db->query('
            SELECT  sf.sf_id, sf.sf_uid, sf.sf_level, sf.sf_order, sf.sf_name_' . $this->lang . ', sf.sf_objects, 
                    sf.sf_minprice, sf.sf_maxprice, sf.per_id, ot.ot_order, ot.ot_name_ru, sft.sft_cssclass,
                    f.f_name, per.per_pricename_' . $this->lang . '

            FROM sf_smartfilters AS sf

            LEFT JOIN ot_object_type AS ot
            ON sf.ot_id = ot.ot_id

            LEFT JOIN sft_smartfilters_type AS sft
            ON sf.sft_id = sft.sft_id

            LEFT JOIN f_files AS f
            ON sf.sf_id = f.f_uid

            LEFT JOIN per_period AS per
            ON sf.per_id = per.per_id

            WHERE sf.sf_enable=1
            AND f.f_ucid = "smartfilters"

            ORDER BY ot.ot_order, sf.sf_level, sf.sf_order
        ')->fetchAll();

        foreach($data as $d_id => $d_val)
        {
            $result[ $d_val["ot_name_" . $this->lang] ][ $d_val["sf_level"] ][ $d_val["sf_order"] ] = array(
                "uid" => $d_val["sf_uid"],
                "name" => $d_val["sf_name_" . $this->lang],
                "objects" => $d_val["sf_objects"],
                "minprice" => $d_val["sf_minprice"],
                "maxprice" => $d_val["sf_maxprice"],
                "cssclass" => $d_val["sft_cssclass"],
                "image" => "/upload/smartfilters/" . $d_val["sf_id"] ."/" . $d_val["f_name"],
                "period" => $d_val["per_pricename_" . $this->lang],
            );   
        }

        return $result;
    }

    //#########################################################################################
    public function getFakeCounter()
    {
        $tbl = new Zend_Db_Table(array(
            'name' => 'fc_fake_counter',
            'primary' => 'fc_id'
        ));

        $item = $tbl->fetchAll(
            $tbl->select()->from(array('fc_fake_counter') , array("fc_id", "fc_hour", "fc_val"))
        )->toArray();

        // $currDay = date("j");
        $currHour = date("G");
        $currHourDB = $item[0]["fc_hour"];
        $id = $item[0]["fc_id"];

        // $newCounterValue = (mt_rand(0, 1)) ? $item[0]["fc_val"] + mt_rand(0, 3) : $item[0]["fc_val"];
        $newCounterValue = $item[0]["fc_val"] + mt_rand(0, 3);

        $row = $tbl->find($id)->current();

        if($currHour == $currHourDB)
        {
            $row->setFromArray(array("fc_val" => $newCounterValue));
            $row->save();

            return $newCounterValue;

        } else {

            $row->setFromArray(array("fc_val" => 0, "fc_hour" => $currHour));
            $row->save();

            return 0;
        }

    }   

}
?>
