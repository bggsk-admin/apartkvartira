<?php
/**
* Class and Function List:
* Function list:
* - init()
* - getItems()
* - getPrices()
* - getImages()
* - getValues()
* - getStayPeriods()
* Classes list:
* - Ui_Model_Objects extends Zend_Db_Table_Abstract
*/
class Ui_Model_Objects extends Zend_Db_Table_Abstract {
    
    public $_name = 'obj_object';
    public $_primary = 'obj_id';
    
    ##########################################################################################
    public function init() {
        $this->db = Zend_Registry::get('db');
        $locale = new Zend_Session_Namespace('locale');
        
        $this->lang = $locale->curlocale['lang'];
        
        $this->pref = "obj_";
        $this->id = $this->pref . 'id';
        $this->name = $this->pref . 'name_' . $this->lang;

        $this->log = new Ui_Model_Log();
    }
    
    ##########################################################################################
    public function getSmartfilters($ot_code = "") 
    {
        $db = Zend_Registry::get( 'db' );

        $where_ot_code = (!empty($ot_code)) ? "AND ot.ot_code = '".$ot_code."' " : "";

        //SQL query
        //*****************************************************************
        $query = '
            SELECT sf.sf_uid, sf.sf_name_ru, sf.sf_name_en, sf.sf_minprice, sf.sf_maxprice 
            FROM sf_smartfilters AS sf
            LEFT JOIN ot_object_type AS ot
            ON sf.ot_id = ot.ot_id
            WHERE sf.sf_enable=1
            ' . $where_ot_code . '
            ORDER BY ot.ot_order, sf.sf_order_navbar
        ';

        $data = $db->query($query)->fetchAll();

        // Log        
        $this->log->write( array('status' => 'success', 'result' => 'sql'), $query );

        return $data;
    }

    ##########################################################################################
    public function getStreets($query) 
    {
        $db = Zend_Registry::get( 'db' );

        //SQL query
        //*****************************************************************
        $query = '
            SELECT *
            FROM kl_kladr        
            WHERE kl_name LIKE "' . $query . '%"
            ORDER BY kl_objects DESC, kl_name
        ';

        $data = $db->query($query)->fetchAll();

        // Log        
        $this->log->write( array('status' => 'success', 'result' => 'sql'), $query );

        return $data;
    }

    ##########################################################################################
    public function getItems($where = array() , $order = array()) 
    {
        
        $db = Zend_Registry::get( 'db' );

        //Add default WHERE conditions
        //*****************************************************************
        $where[] = "obj.obj_enable = 1";
        $where[] = "v.v_table = 'ot_object_type'";

        //Generate WHERE clause
        //*****************************************************************
        if (!empty($where) && count($where) > 0) 
        {
            $where = " WHERE " . implode(" AND ", $where);
        }

        //Generate ORDER clause
        //*****************************************************************
        if (!empty($order) && count($order) > 0) 
        {
            $order = " ORDER BY " . implode(", ", $order);
        } else {
            $order = "";
        }

        //SQL query
        //*****************************************************************
        $query = '
            SELECT DISTINCT obj.obj_id, 
            obj.*,
            sfo.*,
            sf.*,
            per.*,
            v.*,
            ot.*

            FROM obj_object AS obj

            LEFT JOIN sfo_smartfilters_objects AS sfo
            ON obj.obj_id = sfo.obj_id

            LEFT JOIN sf_smartfilters AS sf
            ON sf.sf_id = sfo.sf_id

            LEFT JOIN per_period AS per
            ON per.per_id = obj.obj_price_period

            LEFT JOIN v_value AS v
            ON v.obj_id = obj.obj_id

            LEFT JOIN ot_object_type AS ot
            ON ot.ot_id = v.v_key

            ' . $where . '
            ' . $order . '
        ';

        // Log        
        $this->log->write( array('status' => 'success', 'result' => 'sql'), $query );

        $data = $db->query($query)->fetchAll();

        return $data;
    }
    
    ##########################################################################################
    public function getImages($object_id = 0) {
        $tbl = new Zend_Db_Table(array(
            'name' => 'f_files',
            'primary' => 'f_id'
        ));
        
        $items = array();
        
        if ($object_id > 0) {
            $items = $tbl
                ->fetchAll($tbl
                ->select()
                ->from(array(
                'f_files'
            ) , array(
                'f_name'
            ))
                ->where('f_ucid = ?', 'object')
                ->where('f_uid = ?', $object_id))->toArray();
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function getValues($table, $obj_id) {
        $items = array();
        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));
        
        $rows = $tbl
            ->fetchAll($tbl
            ->select()
            ->from(array(
            'v_value'
        ) , array(
            'v_key'
        ))
            ->where('obj_id = ?', $obj_id)
            ->where('v_table = ?', $table))->toArray();
        
        foreach ($rows as $id => $val) {
            $items[] = $val['v_key'];
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function getValuesName($table, $prefix, $obj_id) {
        
        $items = array();

        $db = Zend_Registry::get( 'db' );

        //SQL query
        //*****************************************************************
        $query = '
            SELECT  *

            FROM v_value AS val

            LEFT JOIN ' . $table . ' AS tab_names
            ON val.v_key = tab_names.' . $prefix . '_id

            WHERE obj_id = ' . $obj_id . '
            AND v_table = "' . $table . '"
        ';

        $rows= $db->query($query)->fetchAll();
        
        foreach ($rows as $id => $val) {
            $items[] = $val[$prefix . '_name_ru'];
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function getStayPeriods() {
        $items = array();
        $tbl = new Zend_Db_Table(array(
            'name' => 'per_period',
            'primary' => 'per_id'
        ));
        
        $rows = $tbl
            ->fetchAll($tbl
            ->select()
            ->from(array(
            'per_period'
        ) , array(
            '*'
        ))
            ->where('per_enable = ?', "1"))
            ->toArray();
        
        foreach ($rows as $id => $val) {
            $items[$val['per_id']] = array(
                "name" => $val['per_pricename_' . $this->lang]
            );
        }
        
        return $items;
    }
}
?>
