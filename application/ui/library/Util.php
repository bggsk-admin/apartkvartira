<?php
/*
    Author: Marsel Alamdar
    Ver: 1.05
    Update: 9/04/2012
    
    - getHumanDate
    - removeFolder
    - fileCRC
    - humanFileSize
    - secondsToTime
    - generateHash
    - getIp
    - getUserAgent
    - getConfig
    - strtolower_utf8
    - downloadFile
    - winToUtf8
    - pluralForm
    - datesDiff
    - filterArrayByKey


*/
class Util  extends Zend_Db_Table_Abstract {

    protected $_name = 'u_user';
    protected $_primary = 'u_id';

    ##############################################################3
    function getHumanDate($date, $selector, $with_year, $with_time = 0)
    {
        $selector = addslashes($selector);
        $months = array("ru" => array("Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"),
                        "en" => array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
        );
        
        if($with_time) list($date, $time) = split(" ", $date);
        else $time = "";
        
        list($y, $m, $d) = split($selector, $date);
        
        if($d>0 && $m>0 && $y>0)
        {
            if($d<10) $d = substr($d, 1, 1);
            
            if($with_year)        $clear_date = $d." ".$months['ru'][intval($m-1)]." ".$y;
            else                $clear_date = $d." ".$months['ru'][intval($m-1)];
        
            return $clear_date." ".$time;
        }    else    {
            return "";
        }
    }    

    ##############################################################3
    function removeFolder($folder) 
    {
        if(file_exists($folder))
        {
            // Сканируем директорию
            $dir_scan = scandir($folder);

            // Удаляем все файлы с директории
            for($i = 0; $i <= count($dir_scan); $i++)
            {
                if($dir_scan[$i] != ".." && $dir_scan[$i] != '.' && !is_null($dir_scan[$i]) && $dir_scan[$i] != '')
                {
                    @unlink($folder."/".$dir_scan[$i]);
                } // if
            } //for

            // Удаляем саму директорию
            @rmdir($folder);
        } //if
    }

    ##############################################################3
    function fileCRC( $file ) 
    { 
        if(file_exists($file))
        {
            #if(function_exists('hash_file'))
            #{
            #    return hash_file ('CRC32', $file , FALSE ); 
            #} else {

                //Calculate CRC by PHP
                #$file_string = file_get_contents($file);
                #$crc = crc32($file_string);
                #unset($file_string);
                #return sprintf("%u", $crc);                
                
                //Calculate CRC by system
                $last_line = exec("cksum -o 3 ".$file);
                $ret = explode(" ", $last_line);
                return $ret[0];
                
            #}

        } 
    }

    ##############################################################3
    function humanFileSize($a_bytes)
    {
        if ($a_bytes < 1024) {
            return $a_bytes .' B';
        } elseif ($a_bytes < 1048576) {
            return round($a_bytes / 1024, 2) .' KB';
        } elseif ($a_bytes < 1073741824) {
            return round($a_bytes / 1048576, 2) . ' MB';
        } elseif ($a_bytes < 1099511627776) {
            return round($a_bytes / 1073741824, 2) . ' GB';
        } elseif ($a_bytes < 1125899906842624) {
            return round($a_bytes / 1099511627776, 2) .' TB';
        }
    }

    ##############################################################3
    function secondsToTime($seconds)
    {
        $h = (int)($seconds / 3600);
        $m = (int)(($seconds - $h*3600) / 60);
        $s = (int)($seconds - $h*3600 - $m*60);
        return (($h)?(($h<10)?("0".$h):$h):"00").":".(($m)?(($m<10)?("0".$m):$m):"00").":".(($s)?(($s<10)?("0".$s):$s):"00");
    }

    ##############################################################3
    function generateHash()
    {
        $result = "";
        $charPool = '0123456789abcdefghijklmnopqrstuvwxyz';
        for($p = 0; $p<15; $p++)    $result .= $charPool[mt_rand(0,strlen($charPool)-1)];
        return sha1(md5(sha1($result)));
    }

    ##############################################################3
    public static function getIp()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        return $ip;
    }
    
    ##############################################################3
    public static function getUserAgent()
    {
        $ua = $_SERVER['HTTP_USER_AGENT'];
        return $ua;
    }
    
    ##############################################################3
    public static function getConfig($config_names = array())
    {
        if(!empty($config_names))
        {
            $db = Zend_Registry::get( 'db' );        
            
            $select = $db->query("
                SELECT c_name, c_value
                FROM c_config
                WHERE c_name IN ('".implode("','", $config_names)."')
            ");    
            $rows = $select->fetchAll();

            $res = array();
            if(count($config_names) > 1)   foreach($rows as $row_id => $row_val)    $res[$row_val['c_name']] = $row_val['c_value'];
            else  $res = $rows[0]['c_value'];

            return $res;
        }
    }

    ##############################################################3
    function strtolower_utf8($string)
    { 
        $convert_to = array( 
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", 
            "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
            "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж", 
            "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", 
            "ь", "э", "ю", "я" 
        ); 
        $convert_from = array( 
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", 
            "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", 
            "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", 
            "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ", 
            "Ь", "Э", "Ю", "Я" 
        );     
        return str_replace($convert_from, $convert_to, $string);         
    }          

    ##############################################################3
    function downloadFile($path)
    { 
        if(file_exists($path))
        {            
            if (function_exists('mime_content_type')) {
                $mtype = mime_content_type($path);
            } 
            
            header('Content-Description: File Transfer');
            header('Content-Type: ' . $mtype);
            header('Content-Disposition: attachment; filename=' . basename($path));
            header("Content-Transfer-Encoding: binary"); 
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));             
            
            ob_clean();
            flush();
            
            readfile($path);
                    
        } else {
            
            echo "404";
            #$this->getResponse()->setHttpResponseCode(404);
        }            
    }

    ##############################################################3
    function winToUtf8($s)
    {
        // перекодировка из win в utf-8

        static $table= array
        (
            "\xC0"=>"\xD0\x90","\xC1"=>"\xD0\x91","\xC2"=>"\xD0\x92","\xC3"=>"\xD0\x93","\xC4"=>"\xD0\x94",
            "\xC5"=>"\xD0\x95","\xA8"=>"\xD0\x81","\xC6"=>"\xD0\x96","\xC7"=>"\xD0\x97","\xC8"=>"\xD0\x98",
            "\xC9"=>"\xD0\x99","\xCA"=>"\xD0\x9A","\xCB"=>"\xD0\x9B","\xCC"=>"\xD0\x9C","\xCD"=>"\xD0\x9D",
            "\xCE"=>"\xD0\x9E","\xCF"=>"\xD0\x9F","\xD0"=>"\xD0\xA0","\xD1"=>"\xD0\xA1","\xD2"=>"\xD0\xA2",
            "\xD3"=>"\xD0\xA3","\xD4"=>"\xD0\xA4","\xD5"=>"\xD0\xA5","\xD6"=>"\xD0\xA6","\xD7"=>"\xD0\xA7",
            "\xD8"=>"\xD0\xA8","\xD9"=>"\xD0\xA9","\xDA"=>"\xD0\xAA","\xDB"=>"\xD0\xAB","\xDC"=>"\xD0\xAC",
            "\xDD"=>"\xD0\xAD","\xDE"=>"\xD0\xAE","\xDF"=>"\xD0\xAF","\xAF"=>"\xD0\x87","\xB2"=>"\xD0\x86",
            "\xAA"=>"\xD0\x84","\xA1"=>"\xD0\x8E","\xE0"=>"\xD0\xB0","\xE1"=>"\xD0\xB1","\xE2"=>"\xD0\xB2",
            "\xE3"=>"\xD0\xB3","\xE4"=>"\xD0\xB4","\xE5"=>"\xD0\xB5","\xB8"=>"\xD1\x91","\xE6"=>"\xD0\xB6",
            "\xE7"=>"\xD0\xB7","\xE8"=>"\xD0\xB8","\xE9"=>"\xD0\xB9","\xEA"=>"\xD0\xBA","\xEB"=>"\xD0\xBB",
            "\xEC"=>"\xD0\xBC","\xED"=>"\xD0\xBD","\xEE"=>"\xD0\xBE","\xEF"=>"\xD0\xBF","\xF0"=>"\xD1\x80",
            "\xF1"=>"\xD1\x81","\xF2"=>"\xD1\x82","\xF3"=>"\xD1\x83","\xF4"=>"\xD1\x84","\xF5"=>"\xD1\x85",
            "\xF6"=>"\xD1\x86","\xF7"=>"\xD1\x87","\xF8"=>"\xD1\x88","\xF9"=>"\xD1\x89","\xFA"=>"\xD1\x8A",
            "\xFB"=>"\xD1\x8B","\xFC"=>"\xD1\x8C","\xFD"=>"\xD1\x8D","\xFE"=>"\xD1\x8E","\xFF"=>"\xD1\x8F",
            "\xB3"=>"\xD1\x96","\xBF"=>"\xD1\x97","\xBA"=>"\xD1\x94","\xA2"=>"\xD1\x9E","\®"=>"\®"
        );

        return strtr($s, $table);
    }
 
    ##############################################################3
    /*
        Эта функция, которая делает правильное склонение:
        $count = 22;
        $forms = array('минуту','минуты', 'минут');
        echo plural_fom($count, $forms);    
    */
    function pluralForm($n, $forms)
    {
        return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
    }   

    ##############################################################3
    /*
        выводит сколько прошло времени от одного события до другого.
        Код выводит количество минут, если прошло менее 1 часа.
        Если больше 3х часов, то напишет «сегодня».        
    */
    function datesDiff($post_date)
    {
        $d1 = strtotime($post_date);
        $d2 = strtotime(date("Y-m-d H:m:s"));
        $hours = round((($d2-$d1)/(60*60)));
        $minutes =floor(($d2-$d1)/60);

        if ($hours <= 1)
        {
                if ($minutes <= 60)
                {

                        if ($minutes < 1){$minutes = 1;}
                        $forms = array('минуту','минуты', 'минут');
                        $result = $minutes.' '.UDS_WriteRuMonth::plural_form($minutes, $forms).' назад';
                }
                else{
                        $result = "час назад";
                }
        }
        else if ($hours <= 2)
        {
                $result = "2 часа назад";
        }
        else if ($hours <= 3)
        {
                $result = "3 часа назад";
        }
        else if (( $hours >= 3 )&& ( $hours <= 25 ))
        {
                $result = "сегодня";
        }
        else if (( $hours >= 24 )&& ( $hours <= 48 ))
        {
                $result = "вчера";
        }
        else
        {
                $str_t   = strtotime ($post_date);
                $result  = date("d ", $str_t);
                $result .= answer(date("m", $str_t));
                if (date("Y") != date("Y", $str_t))
                {
                        $result .= date(" Y", $str_t);
                }
                $result .= date(" в H:m", $str_t);
        }

        echo $result;
    }

    ##############################################################3
    /*
        выводит массив отфильтрованный по названию ключа
    */
    function filterArrayByKey($key, $array)
    {
        $res = array();
        
        if(!empty($array))
        {
            foreach($array as $id => $val)
            {
                if(strstr($id, $key) && !empty($val)) $res[$id] = $val; 
            }
            return $res;
        }        
    }
    
}
?>
