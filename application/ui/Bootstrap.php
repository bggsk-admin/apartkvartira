<?php
/**
 * Class and Function List:
 * Function list:
 * - _initLogSettings()
 * - _initSession()
 * - _initAutoloader()
 * - _initPlugins()
 * - _initDefaultAction()
 * - _initDataBase()
 * - _initLocalesList()
 * - _initViewSettings()
 * Classes list:
 * - Bootstrap extends Zend_Application_Bootstrap_Bootstrap
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public $zfdebug_on = 0;
    
    protected function _initLogSettings()
    {
        $logger = new Zend_Log();
        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/data/logs/applicationException.log');
        $logger->addWriter($writer);
        $logger->registerErrorHandler();
        
        Zend_Registry::set('logger', $logger);
        
        return $logger;
    }
    
    protected function _initSession()
    {
        $options = $this->getOptions();
        $sessionOptions = array('save_path' => $options['resources']['session']['save_path'], 'cookie_lifetime' => $options['resources']['session']['remember_me_seconds'], 'remember_me_seconds' => $options['resources']['session']['remember_me_seconds'],
        
        /*            'bug_compat_42' => '',
                               'bug_compat_warn' => '',
                               'cache_expire' => '180',
                               'cache_limiter' => 'nocache',
                               'cookie_domain' => '',
                               'cookie_httponly' => '',
                               'cookie_path' => '/',
                               'cookie_secure' => '0',
                               'entropy_file' => '',
                               'entropy_length' => '0',
                               'gc_divisor' => '1000',
                               'gc_maxlifetime' => '1440',
                               'gc_probability' => '1',
                               'hash_bits_per_character' => '5',
                               'hash_function' => '0',
                               'name' => 'TaMeR_SESSID',
                               'referer_check' => '',
                               'save_handler' => 'user',
                               'save_path' => '',
                               'serialize_handler' => 'php',
                               'use_cookies' => '1',
                               'use_only_cookies' => 'on',
                               'use_trans_sid' => '0',
                               'strict' => false,
                               'throw_startup_exceptions' => true,
        */
        );
        Zend_Session::setOptions($sessionOptions);
        Zend_Session::start();
    }
    
    protected function _initAutoloader()
    {
        $options = $this->getOptions();
        
        $loader = new Zend_Loader_Autoloader_Resource(array('basePath' => APPLICATION_PATH, 'namespace' => 'Ui',));
        
        $loader
            ->addResourceType('form', 'forms', 'Form')
            ->addResourceType('model', 'models', 'Model')
            ->addResourceType('plugin', 'plugins', 'Plugin');
        
        return $loader;
    }
    
    protected function _initPlugins()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Ui_Plugin_AccessControl());
        $front->registerPlugin(new Ui_Plugin_LangSelector());
        $front->registerPlugin(new Zend_Controller_Plugin_ErrorHandler());
        
        return $front;
    }
    
    protected function _initDefaultAction()
    {
        $front = Zend_Controller_Front::getInstance();
        
        $front->setDefaultAction('intro');
        
        return $front;
    }
    
    protected function _initDataBase()
    {
        $this->bootstrap('db');
        $db = $this
            ->getPluginResource('db')
            ->getDbAdapter();
        
        Zend_Registry::set('db', $db);
        
        return $db;
    }
    
    protected function _initLocalesList()
    {
        $options = $this->getOptions();
        
        $registry = Zend_Registry::getInstance();
        $registry->set('locales', $options['locales']);
        
        return $registry;
    }
    
    protected function _initViewSettings()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        
        $view->addHelperPath('Ui/View/Helper', 'Ui_View_Helper');
        
        // ---------------------------- ACL --------------------------------- //
        $auth = Zend_Auth::getInstance();
        if ($auth
            ->hasIdentity()) $userType = $auth
            ->getIdentity()
            ->u_role;
        
        $view->acl = new Ui_Plugin_AccessControl();
        $view
            ->hasIdentity = $auth->hasIdentity();
        $view->userType = (isset($userType)) ? $userType : 'guest';
        
        return $view;
    }
}
