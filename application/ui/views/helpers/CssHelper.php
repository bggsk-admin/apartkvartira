<?php
class Zend_View_Helper_CssHelper extends Zend_View_Helper_Abstract
{
    function cssHelper() {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $file_uri = '/css/' . MODULE_NAME . '/'  . $request->getControllerName() . '/' . $request->getActionName() . '.css';

        if (file_exists(PUBLIC_PATH . $file_uri)) {
            $this->view->headLink()->appendStylesheet($file_uri);
        }

        return $this->view->headLink();

    }
}