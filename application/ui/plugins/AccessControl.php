<?php
    class Ui_Plugin_AccessControl extends Zend_Controller_Plugin_Abstract
    {

        public function preDispatch(Zend_Controller_Request_Abstract $request)
        {
            // set up acl
            $acl = new Zend_Acl();

            # ROLES
            $acl->addRole(new Zend_Acl_Role('guest'));
            $acl->addRole(new Zend_Acl_Role('customer'), 'guest');
            $acl->addRole(new Zend_Acl_Role('owner'), 'guest');
            $acl->addRole(new Zend_Acl_Role('administrator'), 'customer');


            # RESOURCES
            $acl->add(new Zend_Acl_Resource('mvc:customers'))
            ->add(new Zend_Acl_Resource('mvc:administrators'))
            ->add(new Zend_Acl_Resource('mvc:owners'));

            $acl
            ->add(new Zend_Acl_Resource('error'))
            ->add(new Zend_Acl_Resource('index'))
            ->add(new Zend_Acl_Resource('user'))
            ->add(new Zend_Acl_Resource('objects'))
            ->add(new Zend_Acl_Resource('help'))
            ->add(new Zend_Acl_Resource('page'))
            ;

            // -- GUEST --
            $acl->allow('guest', 'user', array('login', 'logout', 'ajax'));
            $acl->allow('guest', 'objects', array('index', 'view', 'checkout', 'pay', 'ajax'));
            $acl->allow('guest', 'index', array('index', 'intro', 'ajax'));
            $acl->allow('guest', 'help', array('index', 'uc'));
            $acl->allow('guest', 'page', array('index', 'about', 'contacts', 'securepay', 'personaldata', 'guests', 'owners'));

            // -- CUSTOMER --
            $acl->allow('customer', null);
/*            $acl->allow('customer', 'objects',  array('index', 'ajax'));
            $acl->allow('customer', 'index', array('index', 'ajax'));
            $acl->allow('customer', 'help', array('index'));
*/            // $acl->allow('customer', 'mvc:customers');

            // -- ADMINISTRATOR --
            $acl->allow('administrator', null);

            // fetch the current user
            $auth = Zend_Auth::getInstance();

            if($auth->hasIdentity()) {
                $identity = $auth->getIdentity();
                $role = strtolower($identity->u_role);
            }else{
                $role = 'guest';
            }

            $controller = $request->controller;
            $action = $request->action;

            if (!$acl->isAllowed($role, $controller, $action)) {
                if ($role == 'guest') {
                    $request->setControllerName('user');
                    $request->setActionName('login');
                } else {
                    $request->setControllerName('error');
                    $request->setActionName('noauth');
                }
            }

            # Link navigation with Acl
            Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
            Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole($role);

        }

    } #class
