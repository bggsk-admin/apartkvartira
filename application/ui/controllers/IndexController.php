<?php
/**
 * Class and Function List:
 * Function list:
 * - __call()
 * - init()
 * - introAction()
 * - indexAction()
 * - ajaxAction()
 * - ajaxUpdateCounter()
 * - domainsAction()
 * - combinations()
 * Classes list:
 * - IndexController extends Zend_Controller_Action
 */
require_once ("Util.php");

class IndexController extends Zend_Controller_Action
{
    
    public function __call($method, $args)
    {
        if ('Action' == substr($method, -6)) {
            if (method_exists($this, $method)) {
                $this->render();
            } else {
                
                $params['type'] = $this
                    ->getRequest()
                    ->getParam('action');
                $params['code'] = $this
                    ->getRequest()
                    ->getParam('type');
                
                $this->_forward('index', 'subscribe', 'default', $params);
            }
        }
    }
    
    //#########################################################################################
    public function init()
    {
        $this->model = new Ui_Model_Index();
        $this
            ->_helper
            ->layout
            ->setLayout('main');
        
        $ajaxContext = $this
            ->_helper
            ->getHelper('AjaxContext');
        
        $ajaxContext
            ->addActionContext('ajax', 'json')
            ->addActionContext('ajaxUpdateCounter', 'json')
            ->initContext('json');
    }
    
    //#########################################################################################
    public function introAction()
    {
        $this
            ->_helper
            ->layout
            ->setLayout('intro');
    }
    
    //#########################################################################################
    public function indexAction()
    {
        $smartspanel = $this
            ->model
            ->getSmartsPanel();
        
        $this
            ->view
            ->smartspanel = $smartspanel;
        
        // $this->_forward('intro');
        
    }
    
    //#########################################################################################
    public function ajaxAction()
    {
        
        $this
            ->_helper
            ->viewRenderer
            ->setNoRender();
        
        $action = $_REQUEST['action'];
        
        switch ($action) {
            case 'updatecounter':
                $this->ajaxUpdateCounter($_REQUEST);
                break;
        }
    }
    
    //#########################################################################################
    public function ajaxUpdateCounter($query)
    {
        $this
            ->view
            ->value = $this
            ->model
            ->getFakeCounter();
    }
    
    //#########################################################################################
    public function domainsAction()
    {
        $this->_helper->layout->setLayout('null-bootstrap');
        
        $comb_arr = array();

        if ($this->_request->isPost()) {
            
            $post = $this->_request->getPost();
            
            $p = (isset($post["domain_parts"]) && !empty($post["domain_parts"])) ? explode(";", $post["domain_parts"]) : false;

            if($p)
            {
                // $comb = $this->combinations($p);  
                $comb = $this->BackTrace($p);                

/*                foreach($comb as $val)
                {
                    $comb_arr[] = implode("", $val);
                }
*/
            }

            $this->view->req = $post;
        }

        // $this->view->comb = $comb_arr;
        $this->view->comb = $comb;
    }
    
    //#########################################################################################
    public function combinations($words)
    {
        if (count($words) == 1) {
            return [$words];
        };
        
        $combinations = [];
        $i = 0;

        for($i = 0; $i < count($words); $i++)
        {
            $first_word = array_shift($words);
            
            foreach ($this->combinations($words) as $cmb) {
                $combinations[] = array_merge([$first_word], $cmb);
            };
            
            array_push($words, $first_word);
        }
        
        return $combinations;
    }
    
    //#########################################################################################
    public function BackTrace(&$srcarr, $cnt = 0, $arr = array(), $mask = array()) 
    { 
        if (count($srcarr) == $cnt) 
        {
            echo implode('', $arr) . '<br />'; 
            return; 
        } 
        
        if(count($srcarr)>0)
        {
        for ($i = 0; $i < count($srcarr); $i++) 
        {
            if (!$mask[$i])
            {
                $mask[$i] = true; 
                $arr[$cnt] = $srcarr[$i];
                $this->BackTrace($srcarr, $cnt + 1, $arr, $mask);
                $mask[$i] = false;
            } 
        }

        }
    } 
 
    // {{ END }} #########################################################################################
    
} 
