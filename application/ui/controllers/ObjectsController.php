<?php
/**
 * Class and Function List:
 * Function list:
 * - init()
 * - indexAction()
 * - testmapAction()
 * - ajaxAction()
 * - ajaxGetObjects()
 * Classes list:
 * - ObjectsController extends Zend_Controller_Action
 */
require_once ("Util.php");

class ObjectsController extends Zend_Controller_Action {
    
    //#########################################################################################
    public function init() {
        
        $this->model = new Ui_Model_Objects();
        $this->log = new Ui_Model_Log();

        $this->_helper->layout->setLayout('objects');
        
        $locale = new Zend_Session_Namespace('locale');
        $this->lang = $locale->curlocale['lang'];

        $this->sessFilter = new Zend_Session_Namespace('filter');
        $this->sessBooking = new Zend_Session_Namespace('booking');
        $this->sessSignup = new Zend_Session_Namespace('signup');
        $this->sessSignin = new Zend_Session_Namespace('signin');

        $ajaxContext = $this
            ->_helper
            ->getHelper('AjaxContext');
        
        $ajaxContext
            ->addActionContext('ajax', 'json')
            ->addActionContext('ajaxGetObjects', 'json')
            ->addActionContext('ajaxGetSmarts', 'json')
            ->addActionContext('ajaxGetSmartsForSelect', 'json')
            ->initContext('json');

    }
    
    //#########################################################################################
    public function indexAction() {



    }
    
    //#########################################################################################
    public function viewAction() {

        $this->_helper->layout->setLayout('object-view');

        // print_r($_SESSION);die;

        // Filter by SmartFilter 
        //*****************************************************************
        $f_smart = $this->sessFilter->data['f_smart'];
        if(!empty($f_smart))
        {
            $where[] = "sf.sf_uid = \"" . $f_smart . "\"";
        }

        // Filter by UID 
        //*****************************************************************
        $f_uid = $this->_request->getParam('uid');
        if(!empty($f_uid))
        {
            $where[] = "obj.obj_uid = \"" . $f_uid . "\"";
        }

        // GET Object Info
        //*****************************************************************
        $item = $this->model->getItems($where);
        $item = $item[0];

        $obj_id = $item["obj_id"];
        
        // print_r($item);die;

        // GET Object Images
        //*****************************************************************
        $images_arr = $this->model->getImages($obj_id);
        $images = array();

        foreach ($images_arr as $i_id => $i_val) 
        {
            $images["x100"][] = "/upload/object/" . $obj_id . "/x100/" . $i_val["f_name"];
            $images["x200"][] = "/upload/object/" . $obj_id . "/thumbnail/" . $i_val["f_name"];
            $images["x300"][] = "/upload/object/" . $obj_id . "/x300/" . $i_val["f_name"];
            $images["x400"][] = "/upload/object/" . $obj_id . "/x400/" . $i_val["f_name"];
            $images["x500"][] = "/upload/object/" . $obj_id . "/x500/" . $i_val["f_name"];
            $images["x600"][] = "/upload/object/" . $obj_id . "/x600/" . $i_val["f_name"];
            $images["x800"][] = "/upload/object/" . $obj_id . "/x800/" . $i_val["f_name"];
        }

        $this->view->images = $images;

        $backlink = array('controller' => 'objects' ,'action' => 'index');
        
        if(!empty($this->sessFilter->data))
        {
            $backlink = array_merge($backlink, $this->sessFilter->data);
        } else {
            $backlink["f_smart"]  = $item["sf_uid"];
            $backlink["f_guests"]  = 1;
        }

        $this->view->filter = $this->sessFilter->data;
        $this->view->booking = $this->sessBooking->data;
        $this->view->item = $item;
        $this->view->equipment = $this->model->getValuesName("eq_equipment", "eq", $obj_id);
        $this->view->amenity = $this->model->getValuesName("at_amenity", "at", $obj_id);
        $this->view->restriction = $this->model->getValuesName("rst_restriction", "rst", $obj_id);

        $this->view->backlink = $backlink;

    }

    //#########################################################################################
    public function checkoutAction() {

        $this->_helper->layout->setLayout('object-view');

        // $obj_id = $this->_request->getParam('obj_id');

        // print_r($_REQUEST);
        // print_r($this->sessBooking->data);
        // die;

        // print_r($_REQUEST);

        if(count($_REQUEST) > 0 && isset($_REQUEST["obj_id"]))
        {
            $this->sessBooking->data = $_REQUEST;            
            $obj_id = $this->_request->getParam('obj_id');
    
            // print_r($this->sessBooking->data);
        } 
        elseif(isset($this->sessBooking->data["obj_id"]) && !empty($this->sessBooking->data["obj_id"])) 
        {
            $obj_id = $this->sessBooking->data["obj_id"];
        }

        // GET Object Info
        //*****************************************************************
        $where[] = "obj.obj_id = " . $obj_id;
        $item = $this->model->getItems($where);
        $item = $item[0];

        // Generate Backlink
        //*****************************************************************
        $backlink = array('controller' => 'objects' ,'action' => 'view', 'uid' => $item["obj_uid"]);


        // GET Object Images
        //*****************************************************************
        $images = array();
        $images_arr = $this->model->getImages($obj_id);

        foreach ($images_arr as $i_id => $i_val) 
        {
            $images["x100"][] = "/upload/object/" . $obj_id . "/x100/" . $i_val["f_name"];
            $images["x200"][] = "/upload/object/" . $obj_id . "/thumbnail/" . $i_val["f_name"];
            $images["x300"][] = "/upload/object/" . $obj_id . "/x300/" . $i_val["f_name"];
            $images["x400"][] = "/upload/object/" . $obj_id . "/x400/" . $i_val["f_name"];
            $images["x500"][] = "/upload/object/" . $obj_id . "/x500/" . $i_val["f_name"];
            $images["x600"][] = "/upload/object/" . $obj_id . "/x600/" . $i_val["f_name"];
            $images["x800"][] = "/upload/object/" . $obj_id . "/x800/" . $i_val["f_name"];
        }
        $this->view->images = $images;


        $this->view->booking = $this->sessBooking->data;
        $this->view->backlink = $backlink;
        $this->view->item = $item;

    }

    //#########################################################################################
    public function payAction() {

        $this->_helper->layout->setLayout('object-view');

/*        if(count($_REQUEST) > 0 && isset($_REQUEST["obj_id"]))
        {
            $this->sessBooking->data = $_REQUEST;            
            $obj_id = $this->_request->getParam('obj_id');
    
            // print_r($this->sessBooking->data);
        } 
        elseif(isset($this->sessBooking->data["obj_id"]) && !empty($this->sessBooking->data["obj_id"])) 
        {
            $obj_id = $this->sessBooking->data["obj_id"];
        }

*/            $obj_id = $this->sessBooking->data["obj_id"];

        // GET Object Info
        //*****************************************************************
        $where[] = "obj.obj_id = " . $obj_id;
        $item = $this->model->getItems($where);
        $item = $item[0];

        // Generate Backlink
        //*****************************************************************
        $backlink = array('controller' => 'objects' ,'action' => 'view', 'uid' => $item["obj_uid"]);


        // GET Object Images
        //*****************************************************************
        $images = array();
        $images_arr = $this->model->getImages($obj_id);

        foreach ($images_arr as $i_id => $i_val) 
        {
            $images["x100"][] = "/upload/object/" . $obj_id . "/x100/" . $i_val["f_name"];
            $images["x200"][] = "/upload/object/" . $obj_id . "/thumbnail/" . $i_val["f_name"];
            $images["x300"][] = "/upload/object/" . $obj_id . "/x300/" . $i_val["f_name"];
            $images["x400"][] = "/upload/object/" . $obj_id . "/x400/" . $i_val["f_name"];
            $images["x500"][] = "/upload/object/" . $obj_id . "/x500/" . $i_val["f_name"];
            $images["x600"][] = "/upload/object/" . $obj_id . "/x600/" . $i_val["f_name"];
            $images["x800"][] = "/upload/object/" . $obj_id . "/x800/" . $i_val["f_name"];
        }
        $this->view->images = $images;

        $this->view->signin = $this->sessSignin->data = $_REQUEST;
        $this->view->signup = $this->sessSignup->data = $_REQUEST;
        
        $this->view->booking = $this->sessBooking->data;
        $this->view->backlink = $backlink;
        $this->view->item = $item;

    }

    //#########################################################################################
    public function compareAction() {

        $this->_helper->layout->setLayout('object-view');

        // GET Object Info
        //*****************************************************************
        $where[] = "obj.obj_id IN (57, 67, 77)";


        $item = $this->model->getItems($where);

        $objects = array();
        foreach($item as $junk => $val)
        {
            $objects[ $val["obj_id"] ] = $val;
        }


        $this->view->item = $objects;
        $item = $objects;

        // GET Object Images
        //*****************************************************************
        $images = array();

        foreach($item as $junk => $val)
        {
            $obj_id = $val['obj_id'];

            $images_arr = $this->model->getImages($val['obj_id']);

            foreach ($images_arr as $i_id => $i_val) 
            {
                $images[$val['obj_id']]["x100"][] = "/upload/object/" . $obj_id . "/x100/" . $i_val["f_name"];
                $images[$val['obj_id']]["x200"][] = "/upload/object/" . $obj_id . "/thumbnail/" . $i_val["f_name"];
                $images[$val['obj_id']]["x300"][] = "/upload/object/" . $obj_id . "/x300/" . $i_val["f_name"];
                $images[$val['obj_id']]["x400"][] = "/upload/object/" . $obj_id . "/x400/" . $i_val["f_name"];
                $images[$val['obj_id']]["x500"][] = "/upload/object/" . $obj_id . "/x500/" . $i_val["f_name"];
                $images[$val['obj_id']]["x600"][] = "/upload/object/" . $obj_id . "/x600/" . $i_val["f_name"];
                $images[$val['obj_id']]["x800"][] = "/upload/object/" . $obj_id . "/x800/" . $i_val["f_name"];
            }

            $this->view->images = $images;

        }



        $backlink = array('controller' => 'objects' ,'action' => 'index');
        
        if(!empty($this->sessFilter->data))
        {
            $backlink = array_merge($backlink, $this->sessFilter->data);
        } else {
            // $backlink["f_smart"]  = $item["sf_uid"];
            $backlink["f_guests"]  = 1;
        }

        $this->view->backlink = $backlink;
    }


    //#########################################################################################
    public function testmapAction() {
    }
    
    //#########################################################################################
    public function ajaxAction() {
        
        $this->_helper->viewRenderer->setNoRender();
        
        $action = $_REQUEST['action'];
        
        switch ($action) {
            case 'getobjects':
                $this->ajaxGetObjects($_REQUEST);
            break;
            case 'getstreets':
                $this->ajaxGetStreets($_REQUEST["query"]);
            break;
            case 'getsmarts':
                $this->ajaxGetSmarts();
            break;
            case 'getsmartsselect':
                $this->ajaxGetSmartsForSelect($_REQUEST);
            break;
        }
    }

    //#########################################################################################
    function ajaxGetSmarts() {

        $response = array();
        $items = $this->model->getSmartfilters();

        foreach($items as $i_id => $i_val)
        {
            $response[ $i_val["sf_uid"] ]  = $i_val;
        }

        $this->view->items = $response;
    }

    //#########################################################################################
    function ajaxGetStreets($query) {

        $response = array();
        $items = $this->model->getStreets($query);

        foreach($items as $i_id => $i_val)
        {
            $objects = (!empty($i_val["kl_objects"])) ? $i_val["kl_objects"] : 0;

            $response[]  = array(
                "name" => $i_val["kl_name"],
                "gis_name" => $i_val["kl_2gis"],
                "district" => $i_val["kl_district"],
                "objects" => $objects,
                "type_short" => $i_val["kl_type_short"]
            );
        }

        $this->view->items = $response;
    }

    //#########################################################################################
    function ajaxGetSmartsForSelect($params) {

        $response = array();

        $ot_code = (isset($params["ot_code"]) && !empty($params["ot_code"])) ? $params["ot_code"] : "";

        $items = $this->model->getSmartfilters($ot_code);

        foreach($items as $i_id => $i_val)
        {
            $response[]  = array(
                "value" => $i_val["sf_uid"] ,
                "text" => $i_val["sf_name_ru"] ,
            );
        }

        $this->view->items = $response;
    }

    //#########################################################################################
    function ajaxGetObjects($params) {

        $clearsess = (isset($params['clearsess']) && !empty($params['clearsess'])) ? $params['clearsess'] : false;

        if($clearsess) {
            // unset($this->sessFilter->data);
            // echo "erase";
        }

        $this->_helper->viewRenderer->setNoRender();

        //Generate WHERE clause
        //*****************************************************************
        $where = array();

        // Filter by SmartFilter 
        //*****************************************************************
        $f_smart = $this->_request->getParam('f_smart');
        if(!empty($f_smart))
        {
            //Store in session
            $this->sessFilter->data['f_smart'] = $f_smart;

            $where[] = "sf.sf_uid = \"" . $f_smart . "\"";
        }

        // Filter by Price Min 
        //*****************************************************************
        $f_minprice = $this->_request->getParam('f_minprice');
        if(!empty($f_minprice))
        {
            //Store in session
            $this->sessFilter->data['f_minprice'] = $f_minprice;

            $where[] = "obj.obj_price >= " . $f_minprice;
        }

        // Filter by Price Max 
        //*****************************************************************
        $f_maxprice = $this->_request->getParam('f_maxprice');
        if(!empty($f_maxprice))
        {
            //Store in session
            $this->sessFilter->data['f_maxprice'] = $f_maxprice;

            $where[] = "obj.obj_price <= " . $f_maxprice;
        }

        // Filter by Guests 
        //*****************************************************************
        $f_guests = $this->_request->getParam('f_guests');
        if(!empty($f_guests))
        {
            //Store in session
            $this->sessFilter->data['f_guests'] = $f_guests;

            $where[] = "obj.obj_guests >= " . $f_guests;
        }

        // Filter by Street 
        //*****************************************************************
        $f_street = urldecode($this->_request->getParam('f_street'));
        if(!empty($f_street))
        {
            //Store in session
            $this->sessFilter->data['f_street'] = $f_street;

            $where[] = "obj.obj_addr_street LIKE '%" . $f_street . "%' ";
        }

        // Filter by Arrive Date 
        //*****************************************************************
        $f_arrdate = urldecode($this->_request->getParam('f_arrdate'));
        
        if(!empty($f_arrdate) && $f_arrdate != "Invalid date")
        {
            //Store in session
            $this->sessFilter->data['f_arrdate'] = $f_arrdate;
        } else {
            unset($this->sessFilter->data['f_arrdate']);
        }

        // Filter by Departure Date 
        //*****************************************************************
        $f_depdate = urldecode($this->_request->getParam('f_depdate'));
        
        if(!empty($f_depdate) && $f_depdate != "Invalid date")
        {
            //Store in session
            $this->sessFilter->data['f_depdate'] = $f_depdate;
        } else {
            unset($this->sessFilter->data['f_depdate']);
        }

        // Filter by UID 
        //*****************************************************************
        $f_uid = urldecode($this->_request->getParam('f_uid'));
        if(!empty($f_uid))
        {
            $where[] = "obj.obj_uid = '" . $f_uid . "' ";
        }

        //Generate ORDER clause
        //*****************************************************************
        $order = array();
        $order[] = "obj.obj_price ASC";
        
        // GET All Filtered Objects
        //*****************************************************************
        $items = $this->model->getItems($where, $order);
        
        // print_r($items);die;

        //Prepare JSON Locations
        //*****************************************************************
        $locations = array();
        
        $index = 0;
        
        foreach ($items as $item) 
        {
            
            $images_arr = $this->model->getImages($item['obj_id']);
            $images = array();

            foreach ($images_arr as $i_id => $i_val) 
            {
                $images["x100"][] = "/upload/object/" . $item['obj_id'] . "/x100/" . $i_val["f_name"];
                $images["x200"][] = "/upload/object/" . $item['obj_id'] . "/thumbnail/" . $i_val["f_name"];
                $images["x300"][] = "/upload/object/" . $item['obj_id'] . "/x300/" . $i_val["f_name"];
                $images["x400"][] = "/upload/object/" . $item['obj_id'] . "/x400/" . $i_val["f_name"];
                $images["x500"][] = "/upload/object/" . $item['obj_id'] . "/x500/" . $i_val["f_name"];
                $images["x600"][] = "/upload/object/" . $item['obj_id'] . "/x600/" . $i_val["f_name"];
            }
            
            // Get Location & Streetview Coordinates
            //*****************************************************************
            $lat = $item['obj_addr_lat'];
            $lon = $item['obj_addr_lon'];
            $lat_strview = (!empty($item['obj_addr_lat_view'])) ? $item['obj_addr_lat_view'] : $item['obj_addr_lat'];
            $lon_strview = (!empty($item['obj_addr_lon_view'])) ? $item['obj_addr_lon_view'] : $item['obj_addr_lon'];

            // Get Rating
            //*****************************************************************
            $obj_rating = (!empty($item['obj_rating'])) ? $item['obj_rating'] : 0;
            
            $locations[] = array(
                "lat" => round($lat, 6) ,
                "lng" => round($lon, 6) ,
                
                "title" => $item['obj_name_ru'],
                // "icon" => array(
                //     "url" => '/images/ui/map-pin-4.png'
                // ) ,
                "details" => array(

                    "index" => $index,
                    "id" => $item['obj_id'],
                    "uid" => $item['obj_uid'],
                    
                    // Object Type
                    "ot_name" => $item['ot_name_ru'],
                    "ot_code" => $item['ot_code'],
                    
                    // Parameters
                    "smart_uid"   => $item['sf_uid'],
                    "smart_name"=> $item['sf_name_ru'],
                    "rating"         => $obj_rating,
                    "guests"        => $item['obj_guests'],
                    "sqr"             => $item['obj_sqr'],
                    "rooms"        => $item['obj_rooms'],
                    
                    // Price
                    "price" => $item['obj_price'],
                    "stayperiod" => $item['per_pricename_ru'],
                    
                    // Images
                    "images" => $images,
                    
                    // Streetview
                    "lat_strview" => $lat_strview,
                    "lng_strview" => $lon_strview,
                ) ,
            );
            
            $index++;
        }
        
        // Log        
        $this->log->write( array('status' => 'success', 'result' => 'search') );

        $this->view->locations = $locations;

    }
}

