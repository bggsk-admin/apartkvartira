<?php
    class UserController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->model = new Ui_Model_User();
            $this->log = new Ui_Model_Log();

            $this->backurl = $this->view->url( array('action' => 'index') );

            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext->addActionContext('ajax', 'json')
                ->addActionContext('ajaxDoLogin', 'json')
            ->initContext('json');

        }

        ##########################################################################################
        public function loginAction ()
        {
            // echo "1"; die;
            $this->_helper->layout->setLayout('login');
            $form = new Ui_Form_UserLogin();

            if ($this->_request->isPost() && $form->isValid($_POST))
            {
                $formValues = $form->getValues();

                $db = Zend_Db_Table::getDefaultAdapter();

                //create the auth adapter
                $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'u_user', 'u_username', 'u_password', 'MD5(?) AND u_active = "1"');

                //set the username and password
                $authAdapter->setIdentity($formValues['u_username']);
                $authAdapter->setCredential($formValues['u_password']);

                //authenticate
                $result = $authAdapter->authenticate();

                if ($result->isValid()) {
                    // store the username, first and last names of the user
                    $auth = Zend_Auth::getInstance();
                    $storage = $auth->getStorage();
                    $storage->write($authAdapter->getResultRowObject( array('u_id', 'u_username' , 'u_firstname' , 'u_lastname', 'u_role') ));
                    $this->model->updateUserLoginTime( $auth->getIdentity()->u_id );

                    //------------------- Logger ---------------------
                    $this->log->write( array('status' => 'success', 'result' => 'login') );

                    return $this->_redirect( "/index/index", array('prependBase' => false) );

                } else {

                    //------------------- Logger ---------------------
                    $this->log->write( array('status' => 'error', 'result' => 'username or password was incorrect', 'username' => $formValues['u_username']) );

                    $this->view->alertErrorMessage = "Sorry, your username or password was incorrect";
                    $this->view->errorClass = "error";
                }
            }

            $this->view->form = $form;
            $this->view->title = Util::getConfig(array("project_title"));

        }

        ##########################################################################################
        function ajaxDoLogin($params)
        {
            $this->_helper->viewRenderer->setNoRender();

            $db = Zend_Db_Table::getDefaultAdapter();
            $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'u_user', 'u_username', 'u_password', 'MD5(?) AND u_active = "1"');

            if(!empty($params['u_username']) && !empty($params['u_password']))
            {
                $authAdapter->setIdentity($params['u_username']);
                $authAdapter->setCredential($params['u_password']);

                $result = $authAdapter->authenticate();

                if ($result->isValid()) {

                    $auth = Zend_Auth::getInstance();
                    $storage = $auth->getStorage();
                    $storage->write($authAdapter->getResultRowObject( array('u_id', 'u_username' , 'u_firstname' , 'u_lastname', 'u_role') ));

                    //------------------- Logger ---------------------
                    $this->log->write( array('status' => 'success', 'result' => 'login'),  $auth);

                    $this->view->redirect = "/index/index";

                } else {

                    //------------------- Logger ---------------------
                    $this->log->write( array('status' => 'error', 'result' => 'username or password was incorrect', 'username' => $params['u_username']) );

                    $this->view->error = array(
                        'text' => $this->view->translate('Incorrect username or password'),
                        'elements' => "#u_username, #u_password"
                    );
                }


            } else {
                $this->view->error = $this->view->translate('Incorrect username or password');

            }

        }

        ##########################################################################################
        public function ajaxAction()
        {
            $action = $_REQUEST['action'];

            switch ($action) {
                
                case 'dologin':
                    $this->ajaxDoLogin($_REQUEST);
                    break;
                
                case 'signup':
                    $this->ajaxSignup($_REQUEST);
                    break;
            }

        }

        ##########################################################################################
        public function ajaxSignup($params)
        {
            $this->_helper->viewRenderer->setNoRender();

            // $this->view->redirect = $params["redirect"];
            $this->view->error = "Sorry, your username or password was incorrect";
            // $this->view->errorClass = "error";

        }

        ##########################################################################################
        public function logoutAction ()
        {
            $auth = Zend_Auth::getInstance();

            //------------------- Logger ---------------------
            $this->log->write( array('status' => 'success', 'result' => 'logout') );

            $auth->clearIdentity();

            return $this->_forward('login', 'user');
        }

    }



