<?php
    class UserController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->model = new Ai_Model_User();
            $this->log = new Ai_Model_Log();

            $this->backurl = $this->view->url( array('action' => 'index') );

            #$this->_helper->AjaxContext()->addActionContext('ajax', 'json')->initContext('json');
            #$this->_helper->AjaxContext()->addActionContext('ajaxDoLogin', 'json')->initContext('json');

            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext->addActionContext('ajax', 'json')
                ->addActionContext('ajaxDoLogin', 'json')
            ->initContext('json');

        }

        ##########################################################################################
        public function indexAction()
        {
            $items = $this->model->getItems();

            $this->view->items = ($items->count() > 0) ? $items : null;
            $this->view->layout()->subnavbar = $this->view->partial('user/subnavbar.phtml');

        }

        ##########################################################################################
        public function createAction()
        {
            $form = new Ai_Form_User();
            //$form->setAction( $this->view->url( array('controller' => 'user', 'action' => 'create') ) );
            $form->getElement('u_password_open')->setRequired(true);

            if($this->_request->isPost())
            {
                if($form->isValid($_POST))
                {
                    $this->model->createItem( $form->getValues() );
                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }
            }

            $this->view->form = $form;
            $this->view->layout()->subnavbar = $this->view->partial('user/subnavbar.phtml');

        }

        ##########################################################################################
        public function updateAction()
        {
            $form = new Ai_Form_User();
            $form->removeElement('u_password');

            if ($this->_request->isPost())
            {

                if ($form->isValid( $this->_request->getPost() ))
                {
                    $this->model->updateItem( $form->getValues() );
                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }
            } else {

                $id = $this->_request->getParam('id');
                $row = $this->model->find($id)->current();
                $form->populate($row->toArray());
            }
            $this->view->form = $form;
        }



        ##########################################################################################
        public function passwordAction ()
        {
            $form = new Ai_Form_User();
            $form->setAction('/ai/user/password');

            $form->removeElement('u_active');
            $form->removeElement('u_firstname');
            $form->removeElement('u_lastname');
            $form->removeElement('u_username');
            $form->removeElement('u_role');

            if ($this->_request->isPost()) {
                if ($form->isValid($_POST)) {
                    $this->model->updatePassword( $form->getValues() );
                    return $this->_forward('index', 'user');
                }
            } else {
                $id = $this->_request->getParam('id');
                $currentUser = $this->model->find($id)->current();
                $form->populate($currentUser->toArray());
            }

            $this->view->form = $form;
        }

        ##########################################################################################
        public function deleteAction()
        {
            $id = $this->_request->getParam('id');
            $this->model->deleteUser($id);
            return $this->_forward('index', 'user');
        }

        ##########################################################################################
        public function loginAction ()
        {
            $this->_helper->layout->setLayout('login');
            $form = new Ai_Form_UserLogin();

            if ($this->_request->isPost() && $form->isValid($_POST))
            {
                $formValues = $form->getValues();

                $db = Zend_Db_Table::getDefaultAdapter();

                //create the auth adapter
                $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'u_user', 'u_username', 'u_password', 'MD5(?) AND u_active = "1"');

                //set the username and password
                $authAdapter->setIdentity($formValues['u_username']);
                $authAdapter->setCredential($formValues['u_password']);

                //authenticate
                $result = $authAdapter->authenticate();

                if ($result->isValid()) {
                    // store the username, first and last names of the user
                    $auth = Zend_Auth::getInstance();
                    $storage = $auth->getStorage();
                    $storage->write($authAdapter->getResultRowObject( array('u_id', 'u_username' , 'u_firstname' , 'u_lastname', 'u_role') ));
                    $this->model->updateUserLoginTime( $auth->getIdentity()->u_id );

                    //------------------- Logger ---------------------
                    $this->log->write( array('status' => 'success', 'result' => 'login') );

                    //return $this->_forward('index', 'index');
                    return $this->_redirect( "/ai/dashboard", array('prependBase' => false) );

                } else {

                    //------------------- Logger ---------------------
                    $this->log->write( array('status' => 'error', 'result' => 'username or password was incorrect', 'username' => $formValues['u_username']) );

                    $this->view->alertErrorMessage = "Sorry, your username or password was incorrect";
                    $this->view->errorClass = "error";
                }
            }

            $this->view->form = $form;
            $this->view->title = Util::getConfig(array("project_title"));

        }

        ##########################################################################################
        function ajaxDoLogin($params)
        {
            $this->_helper->viewRenderer->setNoRender();

            $db = Zend_Db_Table::getDefaultAdapter();
            $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'u_user', 'u_username', 'u_password', 'MD5(?) AND u_active = "1"');

            if(!empty($params['u_username']) && !empty($params['u_password']))
            {
                $authAdapter->setIdentity($params['u_username']);
                $authAdapter->setCredential($params['u_password']);

                $result = $authAdapter->authenticate();

                if ($result->isValid()) {

                    $auth = Zend_Auth::getInstance();
                    $storage = $auth->getStorage();
                    $storage->write($authAdapter->getResultRowObject( array('u_id', 'u_username' , 'u_firstname' , 'u_lastname', 'u_role') ));
                    
                    //------------------- Logger ---------------------
                    $this->log->write( array('status' => 'success', 'result' => 'login'),  $auth);

                    $this->view->redirect = "/ai/index";

                } else {

                    //------------------- Logger ---------------------
                    $this->log->write( array('status' => 'error', 'result' => 'username or password was incorrect', 'username' => $params['u_username']) );
                    
                    $this->view->error = array(
                        'text' => $this->view->translate('Incorrect username or password'),
                        'elements' => "#u_username, #u_password"
                    );
                }


            } else {
                $this->view->error = $this->view->translate('Incorrect username or password');

            }

        }

        ##########################################################################################
        public function ajaxAction ()
        {
            $action = $_REQUEST['action'];

            switch ($action) {
                case 'dologin':
                    $this->ajaxDoLogin($_REQUEST);
                    break;
            }

        }

        ##########################################################################################
        public function logoutAction ()
        {
            $auth = Zend_Auth::getInstance();

            //------------------- Logger ---------------------
            $this->log->write( array('status' => 'success', 'result' => 'logout') );

            $auth->clearIdentity();

            return $this->_forward('login', 'user');
        }

    }



