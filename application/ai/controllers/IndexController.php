<?php
require_once("Util.php");

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {

		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()) {
			$this->view->identity = $auth->getIdentity();
		}

        //$this->view->layout()->breadcrumb = $this->view->partial('index/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( 'index/subnavbar.phtml');
        //$this->view->headScript()->prependFile('/js/test.js');

   }


}


