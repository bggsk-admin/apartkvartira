<?php
    class ObjectController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->model = new Ai_Model_Object();
            $this->log = new Ai_Model_Log();

            $locale         = new Zend_Session_Namespace('locale');
            $this->curlang  = $this->view->curlang  =  $locale->curlocale;
            $this->langs    = $this->view->langs    =  $locale->locales;
            $this->ucid     = "object";

            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext->addActionContext('ajax', 'json')
            ->addActionContext('ajaxGetObjecttypeCode', 'json')
            ->addActionContext('upload', 'json')
            ->addActionContext('ajaxGetItems', 'json')
            ->addActionContext('ajaxLoadCalendar', 'json')
            ->addActionContext('ajaxRebuild', 'json')
            ->initContext('json');

            $this->backurl = $this->view->url( array('controller' => $this->ucid, 'action' => 'index'), NULL, true );
        }

        ##########################################################################################
        public function indexAction()
        {
            $items = $this->model->getItems();

            if(!empty($items))
            {
                foreach($items as $i_id => $i_val)
                {
                    $images = $this->model->getImages($i_val['obj_id']);
                    $items[$i_id]['obj_images'] = array();

                    foreach($images as $im_id => $im_val)
                    {
                        $items[$i_id]['obj_images'][] = "/upload/object/" . $i_val['obj_id'] . "/thumbnail/" . $im_val["f_name"];
                    }

                }

                $this->view->items = $items;
                $this->view->qty = count($items);

            } else {
                $this->view->items = null;
            }

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');
        }

        ##########################################################################################
        public function createAction()
        {

            $next_id = $this->model->getNextID();
            
            $prm_parameter = $this->model->getParamsFields('prm_parameter', 'prm');
            $per_period = $this->model->getMultiselect('per_period', 'per');

            $form = new Ai_Form_Object();
            $form->getElement('obj_codename')->setValue('XXX-' . date('dmy') . '-' . sprintf('%06d', $next_id));
            $form->getElement('obj_checkin')->setValue('14:00');
            $form->getElement('obj_checkout')->setValue('12:00');

            // Ставим значения пол умолчанию. Ограничения. "Не курить" и "Без домашних животных"
            $form_defaults = array(
                "rst_restriction" => array(1, 2),
            );
            $form->populate( $form_defaults );


            if($this->_request->isPost())
            {
                $post = $this->_request->getPost();

                if($form->isValid($post))
                {
                    $this->model->updateItem( $form->getValues() );
                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }
            }

            $this->view->form = $form;

            $this->view->obj_id = $next_id;
            $this->view->upload_id = $next_id;
            $this->view->upload_ucid = $this->ucid;
            $this->view->prm_parameter = $prm_parameter;
            $this->view->per_period = $per_period;

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-create.phtml');
            $this->view->layout()->infobar = $this->view->partial( $this->ucid. '/infobar-edit.phtml');

        }

        ##########################################################################################
        public function updateAction()
        {

            $form = new Ai_Form_Object();

            $id = $this->_request->getParam('id');

            $prm_parameter = $this->model->getParamsFields('prm_parameter', 'prm');
            $per_period = $this->model->getMultiselect('per_period', 'per');

            if ($this->_request->isPost())
            {
                $post = $this->_request->getPost();

                if($form->isValid($post))
                {
                    $this->model->updateItem( $form->getValues() );
                    $form_arr = $form->getValues();

                    // $this->log->write( array('status' => 'success', 'result' => 'update'), $form->getValues() );
                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }

            } else {

                $id = $this->_request->getParam('id');
                $item = $this->model->find($id)->current();

                $form_arr = $item->toArray();

                //Подгружаем свойства объекта
                $form_arr +=  $this->model->getValues(array(
                    'ot_object_type', 'rt_rent_type', 'srv_service', 'at_amenity', 'eq_equipment', 'rst_restriction'
                    ), $id);

                //Подгружаем смартфильтры
                $form_arr +=  $this->model->getSmartfiltersValues($id);

                //Подгружаем параметры объекта
                $form_arr +=  $this->model->getParamValues(array('prm_parameter'), $id);

                $form->populate( $form_arr );

                //Log action
                // $this->log->write( array('status' => 'success', 'result' => 'edit'), $form_arr );
            }

            $this->view->form = $form;

            $this->view->upload_id = $id;
            $this->view->upload_ucid = $this->ucid;
            $this->view->prm_parameter = $prm_parameter;
            $this->view->per_period = $per_period;

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-edit.phtml');
            $this->view->layout()->infobar = $this->view->partial( $this->ucid. '/infobar-edit.phtml', $form_arr);
        }

        ##########################################################################################
        public function deleteAction()
        {
            $id = $this->_request->getParam('id');

            if(!empty($id)) $this->model->deleteItems(array($id));

            //Log action
            $this->log->write( array('status' => 'success', 'result'=>'delete') );

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

        ##########################################################################################
        public function cloneAction()
        {
            $id = $this->_request->getParam('id');

            if(!empty($id)) $this->model->cloneItem($id);

            //Log action
            $this->log->write( array('status' => 'success', 'result'=>'clone') );

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

        ##########################################################################################
        public function importAction()
        {
            $this->_helper->viewRenderer->setNoRender();

            $import_data = $this->_request->getParam('import_data');

            if(!empty($import_data))
            {
                $import_data = explode("\n", $import_data);

                foreach($import_data as $data)
                {
                    $data = explode("|", $data);

                    $row = $this->model->createItem(
                        array(
                            'obj_enable' => 1,
                            'obj_order' => $data[0],
                            'obj_uid' => $data[1],
                            'obj_name_en' => $data[2],
                            'obj_description_en' => $data[3],
                            'obj_name_ru' => $data[4],
                            'obj_description_ru' => $data[5]
                        )
                    );

                }
                //$this->model->updateBrands( array($mdl_br_id) );

            }

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

        ##########################################################################################
        public function multiupdateAction()
        {
            $this->_helper->viewRenderer->setNoRender();

            if ($this->_request->isPost() || $this->_request->isGet())
            {

                $mode = $this->_request->getParam('mode');
                $items = $this->_request->getParam('customize_items');

                switch ( $mode )
                {
                    case 'enable':
                    {
                        $this->model->setValues($items, array("obj_enable"=>1));
                        break;
                    }

                    case 'disable':
                    {
                        $this->model->setValues($items, array("obj_enable" => 0));
                        break;
                    }

                    case 'delete':
                    {
                        $this->model->deleteItems($items);
                        break;
                    }

                }

            }

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

        ##########################################################################################
        public function ajaxAction ()
        {
            $method = $this->_request->getParam('method');
            $params = $_REQUEST;

            switch ($method) {
                case 'getobjtypecode':
                    $this->ajaxGetObjecttypeCode($params);
                    break;

                case 'getsmartsbyobjtype':
                    $this->ajaxGetSmartsByObjecttype($params);
                    break;

                case 'getprices':
                    $this->view->result = $this->model->ajaxGetObjectPrices($params);
                    break;

                case 'save':
                    $this->ajaxSave($params);
                    break;

                case 'get':
                    $this->ajaxGetItems($params);
                    break;

                case 'rebuild':
                    $this->ajaxRebuild();
                    break;

                case 'loadcalendar':
                    $this->ajaxLoadCalendar($params);
                    break;

                case 'savecalendar':
                    $this->ajaxSaveCalendar($params);
                    break;

                case 'loadeventstable':
                    $this->ajaxLoadEventsTable($params);
                    break;



            }

        }

        ##########################################################################################
        function ajaxGetObjecttypeCode($params)
        {
            $this->_helper->viewRenderer->setNoRender();

            if(isset($params['objtypeid']) && count($params['objtypeid']) > 0)
            {
                $this->view->objcode = $this->model->getObjecttypeCode($params['objtypeid']);

            } else {

                $this->view->alertErrorMessage = "Sorry, your username or password was incorrect";
                $this->view->errorClass = "error";

            }

        }

        ##########################################################################################
        function ajaxGetSmartsByObjecttype($params)
        {
            $this->_helper->viewRenderer->setNoRender();

            $result = array();

            if(isset($params['objtypeid']) && count($params['objtypeid']) > 0)
            {
                $items = $this->model->getSmartsByObjecttype($params['objtypeid']);
                
                if(!empty($items) && count($items) > 0)
                {
                    foreach($items as $i_id => $i_val)
                    {
                        $result[] = array(
                            "id" => $i_id,
                            "val" => $i_val
                            );
                    }

                }
                
                $this->view->objsmarts = $result;

            } else {

                $this->view->alertErrorMessage = "Sorry, your username or password was incorrect";
                $this->view->errorClass = "error";

            }

        }

        ##########################################################################################
        function ajaxSave($params)
        {
            $response = $this->model->updateItem( $params );
            $this->view->form = $response;
        }

        ##########################################################################################
        function ajaxGetItems($params)
        {
            $items = $this->model->getItems();


/*            $this->view->aoColumns = array(
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
                array( "sTitle" => "Engine"),
            );

            $this->view->aaData = $items;
*/

        }

        ##########################################################################################
        function ajaxRebuild()
        {
            $response = $this->model->rebuildItems();
            $this->view->response = $response;
        }

        ##########################################################################################
        function ajaxLoadCalendar($params)
        {
            $this->_helper->viewRenderer->setNoRender();

            $obj_id = $params['id'];
            $response =  array();

            $items = $this->model->getObjectCalendar($obj_id);

            foreach($items as $id => $val)
            {
                if(!empty($val['cal_startdate']))
                {
                    $className = (!empty($val['cal_status'])) ? "event-" . $val['cal_status'] : "";

                    $response[$id] =  array(
                        "id" => $val['cal_id'],
                        "allDay" => true,
                        "title" => $val['cal_title'],
                        "notes" => $val['cal_notes'],
                        "start" => $val['cal_startdate'],
                        "className" => $className,
                        "status" => $val['cal_status'],
                        "type" => "event",
                    );

                    if(!empty($val['cal_enddate']))
                    {
                        $response[$id]['end'] = $val['cal_enddate'];
                    }

                }
            }

            $this->view->data = $response;
        }

        ##########################################################################################
        /**
         * [ajaxLoadEventsTable description]
         * @param  [type] $params
         * @return [type]
         */
        function ajaxLoadEventsTable($params)
        {
            $this->_helper->viewRenderer->setNoRender();

            $obj_id = $params['id'];
            // $obj_id = 87;
            $response =  array();

            $items = $this->model->getObjectCalendar($obj_id);

            foreach($items as $id => $val)
            {
                if(!empty($val['cal_startdate']))
                {
                    $response[] =  array(
                        $val['cal_startdate'],
                        $val['cal_enddate'],
                        $val['cal_enable'],
                        $val['cal_status'],
                        $val['cal_title'],
                        $val['cal_notes'],
                    );

                }
            }

            $this->view->data = $response;
        }

        ##########################################################################################
        function ajaxSaveCalendar($params)
        {
            $this->_helper->viewRenderer->setNoRender();
            $tbl = new Zend_Db_Table(array('name' => 'cal_calendar', 'primary' => 'cal_id'));
            $auth = Zend_Auth::getInstance();                

            $obj_id = $params['id'];

            $json = json_decode($params['json']);

            $tbl->delete(array(
                'obj_id = ?' => $obj_id,
            ));

            if(count($json))
            {
                foreach($json as $jid => $event)
                {
                        
                    if(isset($event->type) && $event->type == "event")
                    {

                        $row = $tbl->createRow();
                        if ($row) 
                        {
                            $row->cal_enable = 1;
                            $row->obj_id = $obj_id;
                            $row->cal_startdate = date("Y-m-d H:i:s", strtotime($event->start));
                            $row->cal_enddate = date("Y-m-d H:i:s", strtotime($event->end));
                            $row->cal_status = $event->status;
                            $row->cal_title = $event->title;
                            $row->cal_owner = ($auth->hasIdentity()) ? $auth->getIdentity()->u_username : "";
                            $row->save();
                        } else {
                            throw new Zend_Exception("Could not create cal_calendar row!");
                        }
                    
                    }
                }
            }
            // die;

/*            $file = fopen(PUBLIC_PATH . '/_calendar.txt', 'w');
            fwrite($file, serialize($json));
            fclose($file);
*/
        }

        ##########################################################################################
        public function uploadAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            // A list of permitted file extensions
            $allowed = array('png', 'jpg', 'jpeg', 'gif','zip');

            if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

                $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

                if(!in_array(strtolower($extension), $allowed)){
                    //echo '{"status":"error"}';
                    $this->view->status = "error";
                    exit;
                }

                if(move_uploaded_file($_FILES['upl']['tmp_name'], '/upload/'.$_FILES['upl']['name'])){
                    //echo '{"status":"success"}';
                    $this->view->status = "success";
                    exit;
                }
            }

            //echo '{"status":"error"}';
            $this->view->status = "error";
            exit;
        }

}