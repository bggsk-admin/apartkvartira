<?php
class LogController extends Zend_Controller_Action
{

    ##########################################################################################
    public function init()
    {
        $this->model = new Ai_Model_Log();
        $this->ucid = $this->view->ucid = "log";
        $this->pref = "l_";
        $this->view->name = "Лог";
        $this->iperpage = Util::getConfig(array("log_perpage"));

        //Read url params
        $this->prm = $this->view->prm = $this->_getAllParams();
        $this->f_prm = $this->view->f_prm = Util::filterArrayByKey("f_", $this->prm);
        
        $this->backurl = $this->view->backurl = $this->view->url( array_merge($this->f_prm, array('action' => 'index')) );
    }

    ##########################################################################################
    public function indexAction()
    {

        //Filter Form
        $f_prm = $this->f_prm;
        
        $filter = new Ai_Form_LogFilter();
        $filter->populate($f_prm);
        
        //Generate WHERE element
        $where = array();
        if(!empty($f_prm['f_date_from']))    $where[] = "l_date >= '".$f_prm['f_date_from']." 00:00:00'";
        if(!empty($f_prm['f_date_to']))      $where[] = "l_date <= '".$f_prm['f_date_to']." 23:59:59'";
        if(!empty($f_prm['f_l_object']))      $where[] = "l_object LIKE '%".$f_prm['f_l_object']."%'";
        if(!empty($f_prm['f_l_username']))      $where[] = "l_username LIKE '%".$f_prm['f_l_username']."%'";
        if(!empty($f_prm['f_l_controller']))      $where[] = "l_controller LIKE '%".$f_prm['f_l_controller']."%'";
        if(!empty($f_prm['f_l_action']))      $where[] = "l_action LIKE '%".$f_prm['f_l_action']."%'";
        
        if(!empty($f_prm['f_l_io']))
        {
            if( $f_prm['f_l_io'] == 'internal')      $where[] = "l_io IS NULL";
            else $where[] = "l_io = '".$f_prm['f_l_io']."'";
        }
        
        //Generate ORDER element
        if(!empty($f_prm['f_sort_field']) && !empty($f_prm['f_sort_type'])) $order[] = $f_prm['f_sort_field']." ".$f_prm['f_sort_type'];
        else $order[] = "l_msec DESC";

        $items = $this->model->getItems( $where, $order );
        
        if(!empty($items)) 
        {

            //Paginate data
            Zend_View_Helper_PaginationControl::setDefaultViewPartial('/paginators/general_paginator.phtml');
            $pitems = Zend_Paginator::factory($items);        
            $pitems->setItemCountPerPage($this->iperpage);
            $pitems->setCurrentPageNumber($f_prm['f_page']);

            $this->view->paginator = Zend_View_Helper_PaginationControl::paginationControl($pitems);
            $this->view->items = $pitems;
            $this->view->qty = count($items);
            $this->view->filterActive = (!empty($f_prm)) ? TRUE : FALSE;

        } else {

            $this->view->items = null;
            $this->view->filterActive = (!empty($f_prm)) ? TRUE : FALSE;

        }
        $this->view->form = $filter;
        
    }

    ##########################################################################################
    public function deleteAction()
    {
        $id = $this->_request->getParam('id');
        
        if($id > 0)
        {
            $this->model->deleteItems(array($id));
        }
        
        return $this->_redirect($this->ucid."/index"); 
    }

    ##########################################################################################
    public function multiupdateAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_request->isPost()) 
        {

            $mode = $this->_request->getParam('mode');
            $items = $this->_request->getParam('customize_items');

            switch ( $mode )  
            {
                case 'delete':    
                {
                    $this->model->deleteItems($items);
                    break;
                }            
                
            }
        
        }    

        return $this->_redirect($this->ucid."/index"); 
    }    

}
