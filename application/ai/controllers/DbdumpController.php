<?
    class DbdumpController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->ucid = $this->view->ucid = "dbdump";
            $this->view->name = "Бэкап БД";
            $this->model = new Ai_Model_Dbdump();
        }

        ##########################################################################################
        public function indexAction()
        {
            $items = $this->model->getItems();

            if(count($items) > 0)
            {
                $total_size = 0;
                foreach($items as $val)
                {
                    $total_size += $val['size'];
                }


                $this->view->items = $items;
                $this->view->qty = count($items);
                $this->view->total_size = $total_size;
            } else {
                $this->view->items = null;
            }

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

        }

        ##########################################################################################
        public function createAction()
        {
            $this->model->createDump();
            return $this->_redirect($this->ucid."/index");
        }

        ##########################################################################################
        public function deleteAction()
        {
            $id = $this->_request->getParam('id');

            if(!empty($id))
            {
                $this->model->deleteItems(array($id));
            }

            return $this->_redirect($this->ucid."/index");
        }

        ##########################################################################################
        public function groupupdateAction()
        {
            $this->_helper->viewRenderer->setNoRender();

            if ($this->_request->isPost())
            {

                $mode = $this->_request->getParam('mode');
                $items = $this->_request->getParam('customize_items');

                switch ( $mode )
                {
                    case 'delete':
                    {
                        $this->model->deleteItems($items);
                        break;
                    }

                }

            }

            return $this->_redirect($this->ucid."/index");
        }

        ##########################################################################################
        public function downloadAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->setLayout('xmlrpc');
            $id = $this->_request->getParam('id');

            Util::downloadFile($this->model->dump_dir . $id);
        }

        ##########################################################################################
        public function restoreAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->setLayout('xmlrpc');

            $id = $this->_request->getParam('id');
            $this->model->restoreDump($id);
            return $this->_redirect($this->ucid."/index");

        }
}