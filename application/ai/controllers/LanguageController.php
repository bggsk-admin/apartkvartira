<?php
    class LanguageController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            #$this->model = new Ai_Model_Lang();
            $this->log = new Ai_Model_Log();

            $locale         = new Zend_Session_Namespace('locale');
            $this->curlang  = $this->view->curlang  =  $locale->curlocale;
            $this->langs    = $this->view->langs    =  $locale->locales;
            $this->ucid     = "language";

            $this->backurl = $this->view->url( array('controller' => $this->ucid, 'action' => 'index'), NULL, true );
        }

        ##########################################################################################
        public function indexAction()
        {
        }

        ##########################################################################################
        public function selectAction()
        {
            $this->_helper->viewRenderer->setNoRender();

            $backurl = str_replace("-", "/", $this->_request->getParam('backurl'));

            return $this->_redirect( $backurl, array('prependBase' => false) );
        }

    }//class

?>
