<?php
class ConfigController extends Zend_Controller_Action
{

    ##########################################################################################
    public function init()
    {
        $this->model = new Ai_Model_Config();
        $this->log = new Ai_Model_Log();
        $this->ucid     = "config";

        $this->backurl = $this->view->url( array('action' => 'index') );
    }

    ##########################################################################################
    public function indexAction()
    {
        $items = $this->model->getItems();

        if($items->count() > 0)
        {
            $this->view->items = $items;
            $this->view->qty = count($items);
        }
        else
        {
            $this->view->items = null;
            $this->view->qty = null;

        }
        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');
    }

    ##########################################################################################
    public function createAction()
    {
        $form = new Ai_Form_Config();
        $form->setAction('/ai/config/create');

        if($this->_request->isPOst()) {
            if($form->isValid($_POST)) {

                $this->model->createItem( $form->getValues() );

                return $this->_redirect( $this->backurl, array('prependBase' => false) );
            }
        }

        $this->view->form = $form;

        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-edit.phtml');
    }

    ##########################################################################################
    public function updateAction()
    {
        $form = new Ai_Form_Config();
        $form->setAction('/ai/config/update');

        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                $this->model->updateItem( $form->getValues() );

                return $this->_redirect( $this->backurl, array('prependBase' => false) );
            }
        } else {
            $id = $this->_request->getParam('id');
            $row = $this->model->find($id)->current();
            $form->populate($row->toArray());
        }
        $this->view->form = $form;

        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-edit.phtml');
    }

    ##########################################################################################
    public function deleteAction()
    {
        $id = $this->_request->getParam('id');
        $this->model->deleteItem($id);

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }

    ##########################################################################################
    public function cloneAction()
    {
        $id = $this->_request->getParam('id');

        if(!empty($id)) $this->model->cloneItem($id);

        //Log action
        $this->log->write( array('status' => 'success') );

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }


}