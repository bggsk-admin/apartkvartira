<?php
    // require_once('UploadHandler.php');
    require_once('CustomUploadHandler.php');

    class FilesController extends Zend_Controller_Action
    {

        private $uploads_rel = '/upload/';

        ##########################################################################################
        public function init()
        {
            #$this->model    = new Ai_Model_Files();
            $this->log      = new Ai_Model_Log();
            $this->auth     = Zend_Auth::getInstance()->getIdentity();

            $this->pref     = "f_";
            $this->ucid     = $this->view->ucid = "files";

            $this->view->name = "Файлы";

            $this->backurl = $this->view->backurl = $this->view->url( array('action' => 'index') );

            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext
            ->addActionContext('index', 'json')
            ->initContext('json');

        }

        ##########################################################################################
        public function indexAction()
        {
            $upload_id = $this->_request->getParam('upload_id');
            $upload_ucid = $this->_request->getParam('upload_ucid');

            // $upload_handler = new UploadHandler(
            $upload_handler = new CustomUploadHandler(
                array(
                    'upload_dir' => $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $upload_ucid . '/' . $upload_id . '/',
                    'upload_url' => '/upload/' . $upload_ucid . '/' . $upload_id . '/',
                    'script_url' => 'http://'.$_SERVER['HTTP_HOST'].'/ai/files/index/upload_ucid/' . $upload_ucid . '/upload_id/' . $upload_id . '/',
                )
            );
            
            

            return $upload_handler->get_server_var('SCRIPT_FILENAME');
            //return 1;
        }

    }
