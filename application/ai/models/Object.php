<?php
/**
 * Class and Function List:
 * Function list:
 * - init()
 * - getItems()
 * - getImages()
 * - createItem()
 * - updateItem()
 * - deleteItems()
 * - updateSelected()
 * - updateParameters()
 * - cloneItem()
 * - getValues()
 * - getParamValues()
 * - getMultiselect()
 * - getParamsFields()
 * - setValues()
 * - getObjecttypeCode()
 * - getObjecttypeNames()
 * - getNextID()
 * - ajaxGetObjectPrices()
 * Classes list:
 * - Ai_Model_Object extends Zend_Db_Table_Abstract
 */
class Ai_Model_Object extends Zend_Db_Table_Abstract {
    
    public $_name = 'obj_object';
    public $_primary = 'obj_id';
    
    ##########################################################################################
    public function init() {
        $this->db = Zend_Registry::get('db');
        $locale = new Zend_Session_Namespace('locale');
        
        $this->lang = $locale->curlocale['lang'];
        
        $this->pref = "obj_";
        $this->id = $this->pref . 'id';
        $this->name = $this->pref . 'name_' . $this->lang;

        $this->log = new Ai_Model_Log();
    }
    
    ##########################################################################################
    public function getItems() {
        $select = $this->select();
        $items = $this
            ->fetchAll($select)->toArray();
        
        foreach ($items as $id => $val) {
            $obj_typenames_arr = $this->getValues(array(
                'ot_object_type'
            ) , $val['obj_id']);
            if (count($obj_typenames_arr) > 0) {
                $obj_typenames_arr = $obj_typenames_arr['ot_object_type'];
                $items[$id]['obj_objtypename'] = implode(", ", $this->getObjecttypeNames($obj_typenames_arr));
            } else {
                $items[$id]['obj_objtypename'] = "";
            }
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function getImages($object_id = 0) {
        
        $tbl = new Zend_Db_Table(array(
            'name' => 'f_files',
            'primary' => 'f_id'
        ));
        
        $items = array();
        
        if ($object_id > 0) {
            $items = $tbl
                ->fetchAll($tbl
                ->select()
                ->from(array(
                'f_files'
            ) , array(
                'f_name'
            ))
                ->where('f_ucid = ?', 'object')
                ->where('f_uid = ?', $object_id))->toArray();
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function createItem($formValues) {
        $rowItem = $this->createRow($formValues);
        if ($rowItem) {
            
            $rowItem->obj_create = date('Y-m-d H:i:s');
            $rowItem->obj_update = date('Y-m-d H:i:s');
            $rowItem->save();
            
            $formValues['obj_id'] = $rowItem->obj_id;
            $this->updateSelected(array(
                'ot_object_type',
                'rt_rent_type',
                'srv_service',
                'at_amenity',
                'eq_equipment',
                'rst_restriction'
            ) , $formValues);
            $this->updateParameters(array(
                'prm_parameter'
            ) , $formValues);
            
            $this->log->write( array('status' => 'success', 'result' => 'create'), $formValues );
            
            return $rowItem;
        } else {
            throw new Zend_Exception("Could not create item!");
        }
    }
    
    ##########################################################################################
    public function updateItem($formValues) {
        
        if (isset($formValues['obj_id']) && !empty($formValues['obj_id'])) {
            
            $row = $this->find($formValues['obj_id'])->current();
            $row->setFromArray($formValues);

            //--------------- Logger--------------------
            $this->log->write( array('status' => 'success', 'result' => 'update'), $formValues );

        } else {

            $row = $this->createRow($formValues);
            $row->obj_create = date('Y-m-d H:i:s');
            $formValues['obj_create'] = date('d.m.Y H:i:s');

            //--------------- Logger--------------------
            $this->log->write( array('status' => 'success', 'result' => 'create'), $formValues );

        }
        
        if ($row) {
            $row->obj_update = $formValues['obj_update'] = date('Y-m-d H:i:s');
            $formValues['obj_update'] = date('d.m.Y H:i:s');
            
            //Проверки
            $null_fields_check = array(
                "obj_rooms",
                "obj_bedrooms",
                "obj_bathrooms",
                "obj_beds_sngl",
                "obj_beds_dbl",
                "obj_guests",
                "obj_order",
                "obj_sqr",
                "obj_addr_floor",
                "obj_addr_elevation",
                "obj_addr_lon",
                "obj_addr_lon_view",
                "obj_addr_lat",
                "obj_addr_lat_view",
                "obj_price",
                "obj_price_minstay",
                "obj_rating",
                "obj_deposit",
            );

            foreach($null_fields_check as $check_field)
            {
                $row
                ->{$check_field} = (!empty($row
                ->{$check_field})) ? $row->{$check_field} : 0;
            }
            
            $formValues['obj_id'] = $row->save();
            
            //13.11.2013 Исключили справочник - rt_rent_type
            $this->updateSelected(array(
                'ot_object_type',
                'srv_service',
                'at_amenity',
                'eq_equipment',
                'rst_restriction',
                // 'sf_smartfilters'
            ) , $formValues);
            
            $this->updateParameters(array(
                'prm_parameter'
            ) , $formValues);
            
            $this->updateSmartfiltersObjects($formValues);
            $this->updateSmartfiltersStat($formValues);

            //Update Prices
/*            $prc_price = new Zend_Db_Table(array(
                'name' => 'prc_price',
                'primary' => 'prc_id'
            ));
            $prc_price->delete(array(
                'obj_id = ?' => $formValues['obj_id']
            ));
            
            if (count($formValues['prc_price'])) {
                foreach ($formValues['prc_price'] as $prc_id => $prc_val) {
                    $price_row = $prc_price->createRow();
                    if ($price_row) {
                        
                        $price_row->obj_id = $formValues['obj_id'];
                        $price_row->prc_enable = 1;
                        $price_row->per_id = $formValues['per_id'][$prc_id];
                        $price_row->prc_min_period = (!empty($formValues['prc_min_period'][$prc_id])) ? $formValues['prc_min_period'][$prc_id] : 0;
                        $price_row->prc_price = (!empty($prc_val)) ? $prc_val : 0;
                        
                        $price_row->save();
                    } else {
                        throw new Zend_Exception("Could not create prc_price row!");
                    }
                }
            }*/
            
            return $formValues;
        } else {
            throw new Zend_Exception("Item update failed. Item not found!");
        }
    }
    
    ##########################################################################################
    public function deleteItems($items_id) {
        if (count($items_id) > 0) {
            foreach ($items_id as $id => $item_id) {
                
                $row = $this->find($item_id)->current();
                
                if ($row) {
                    
                    //------------------------------- Удаляем данные из v_value
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'v_value',
                        'primary' => 'v_id'
                    ));
                    $tbl->delete(array('obj_id = ?' => $item_id));
                    
                    //------------------------------- Удаляем данные из sfo_smartfilters_objects
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'sfo_smartfilters_objects',
                        'primary' => 'sfo_id'
                    ));
                    $tbl->delete(array('obj_id = ?' => $item_id));
                    
                    //------------------------------- Удаляем данные из f_files
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'f_files',
                        'primary' => 'f_id'
                    ));
                    $tbl->delete(array('f_uid = ?' => $item_id, 'f_ucid = ?' => "object"));
                    
                    $row->delete();

                } else throw new Zend_Exception("Could not delete item. Item not found!");
            }
        }
    }

    ##########################################################################################
    public function updateSmartfiltersStat($values = array()) {

        $tbl = new Zend_Db_Table(array('name' => 'sf_smartfilters', 'primary' => 'sf_id'));
        $sf_smartfilters = $values["sf_smartfilters"];

        if (isset($sf_smartfilters) && count($sf_smartfilters)) {
            foreach ($sf_smartfilters as $id => $key) {

                $sf_id = $key;
                $sf_price_range = $this->getSmarfilterPriceRange($sf_id);
                $sf_objects = $this->getSmarfilterObjCount($sf_id);

                $row = $tbl->find($sf_id)->current();
                if ($row) 
                {
                    $row->sf_minprice = $sf_price_range["minprice"];
                    $row->sf_maxprice = $sf_price_range["maxprice"];
                    $row->per_id = $sf_price_range["per_id"];
                    $row->sf_objects = $sf_objects;
                }
                $row->save();

            }
        }
    }

    ##########################################################################################
    public function updateSmartfiltersObjects($values = array()) {
        
        $obj_id = $values['obj_id'];
        $obj_enable = $values['obj_enable'];
        $sf_smartfilters = $values["sf_smartfilters"];
        
        $tbl = new Zend_Db_Table(array(
            'name' => 'sfo_smartfilters_objects',
            'primary' => 'sfo_id'
        ));
        
        $tbl->delete(array(
            'obj_id = ?' => $obj_id
        ));
        
        if (isset($sf_smartfilters) && count($sf_smartfilters)) {
            foreach ($sf_smartfilters as $id => $key) {

                //Update sfo_smartfilters_objects
                $row = $tbl->createRow();
                if ($row) {
                    $row->obj_id = $obj_id;
                    $row->obj_enable = $obj_enable;
                    $row->sf_id = $key;
                    $row->save();
                } else {
                    throw new Zend_Exception("Could not create prv_prm_values row!");
                }

            }
            //foreach - $values
        }
        // if - $values
    }

    ##########################################################################################
    public function getSmarfilterObjCount($sf_id) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($sf_id) && $sf_id > 0)
        {
            $data = $db->query("
                SELECT COUNT(obj.obj_id) AS obj_count
                FROM sfo_smartfilters_objects AS sfo
                LEFT JOIN obj_object AS obj
                ON sfo.obj_id = obj.obj_id
                WHERE sfo.sf_id = " . $sf_id ."
                AND obj.obj_enable = 1"
            )->fetchAll();
        }

        return $data[0]["obj_count"];
    }

    ##########################################################################################
    public function getSmarfilterPriceRange($sf_id) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($sf_id) && $sf_id > 0)
        {
            $data = $db->query("
                SELECT min(obj.obj_price) AS minprice, max(obj.obj_price) AS maxprice, obj.obj_price_period AS per_id
                FROM sfo_smartfilters_objects AS sfo
                LEFT JOIN obj_object AS obj
                ON sfo.obj_id = obj.obj_id
                WHERE sfo.sf_id = " . $sf_id ."
                AND obj.obj_enable = 1"
            )->fetchAll();
        }

        return $data[0];
    }

    ##########################################################################################
    public function getObjectCalendar($obj_id) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($obj_id) && $obj_id > 0)
        {
            $data = $db->query("
                SELECT *, DATEDIFF(cal.cal_enddate, cal.cal_startdate) AS cal_days
                FROM cal_calendar AS cal
                LEFT JOIN obj_object AS obj
                ON cal.obj_id = obj.obj_id
                WHERE cal.obj_id = " . $obj_id ."
                AND cal.cal_enable = 1
                ORDER BY cal_startdate DESC"
            )->fetchAll();
        }

        return $data;
    }

    ##########################################################################################
    public function updateSelected($tables = array() , $values = array()) {
        $item_id = $values['obj_id'];
        
        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));
        
        if (count($tables)) {
            foreach ($tables as $table) {
                $tbl->delete(array(
                    'obj_id = ?' => $item_id,
                    'v_table = ?' => $table
                ));
                
                if (isset($values[$table]) && count($values[$table])) {
                    foreach ($values[$table] as $id => $key) {
                        $row = $tbl->createRow();
                        if ($row) {
                            
                            $row->v_table = $table;
                            $row->obj_id = $item_id;
                            $row->v_key = $key;
                            $row->save();
                        } else {
                            throw new Zend_Exception("Could not create prv_prm_values row!");
                        }
                    }
                    //foreach - $values
                }
                // if - $values
            }
            //foreach - $tables
        }
        //if - $tables
    }
    
    ##########################################################################################
    public function updateParameters($tables = array() , $values = array()) {
        $item_id = $values['obj_id'];
        
        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));
        
        if (count($tables)) {
            foreach ($tables as $table) {
                $tbl->delete(array(
                    'obj_id = ?' => $item_id,
                    'v_table = ?' => $table
                ));
                
                foreach (array_keys($values) as $key) {
                    if (preg_match('/^' . $table . '_\d+/', $key, $matches)) {
                        $v_key = str_replace($table . '_', "", $key);
                        
                        if (!empty($values[$key])) {
                            $row = $tbl->createRow();
                            if ($row) {
                                
                                $row->v_table = $table;
                                $row->obj_id = $item_id;
                                $row->v_key = $v_key;
                                $row->v_val = $values[$key];
                                $row->save();
                            } else {
                                throw new Zend_Exception("Could not create prv_prm_values row!");
                            }
                        }
                        
                        //if
                        
                        
                    }
                    
                    //if - preg_match
                    
                    
                }
                
                //foreach
                
                
            }
            
            //foreach - $tables
            
            
        }
        
        //if - $tables
        
        
    }
    
    ##########################################################################################
    public function cloneItem($id) {
        if ($id > 0) {
            $row = $this
                ->find($id)
                ->current()
                ->toArray();
            unset($row[$this->id]);
            $row[$this->name].= " -COPY-";
            $this->createItem($row);
        }
    }
    
    ##########################################################################################
    public function getValues($tables = array() , $obj_id) {
        
        $items = array();
        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));
        
        foreach ($tables as $table) {
            $rows = $tbl
                ->fetchAll($tbl
                ->select()
                ->from(array('v_value') , array('v_key'))
                ->where('obj_id = ?', $obj_id)
                ->where('v_table = ?', $table))->toArray();
            
            foreach ($rows as $id => $val) {
                $items[$table][] = $val['v_key'];
            }
        }
        
        return $items;
    }

    ##########################################################################################
    public function getSmartfiltersValues($obj_id) {

        $items = array();
        
        $tbl = new Zend_Db_Table(array( 'name' => 'sfo_smartfilters_objects', 'primary' => 'sfo_id' ));

        if(!empty($obj_id))
        {
            $rows = $tbl
                ->fetchAll(
                    $tbl->select()
                    ->from(array('sfo_smartfilters_objects') , array('sf_id'))
                    ->where('obj_id = ?', $obj_id)
                )->toArray();
            
                foreach ($rows as $id => $val) {
                    $items["sf_smartfilters"][] = $val['sf_id'];
                }
        }

        return $items;
        
    }    

    ##########################################################################################
    public function getParamValues($tables = array() , $obj_id) {

        $items = array();
        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));
        
        foreach ($tables as $table) {
            $rows = $tbl
                ->fetchAll($tbl
                ->select()
                ->from(array('v_value') , array('v_key','v_val'))
                ->where('obj_id = ?', $obj_id)
                ->where('v_table = ?', $table))->toArray();
            
            foreach ($rows as $id => $val) {
                $items[$table . '_' . $val['v_key']] = $val['v_val'];
            }
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function getMultiselect($table, $prefix, $order = array()) {
        
        $items = array();
        
        $tbl = new Zend_Db_Table(array(
            'name' => $table,
            'primary' => $prefix . '_id'
        ));
        
        $select = $tbl
            ->select()
            ->from(array(
            $table
        ) , array(
            $prefix . '_id',
            $prefix . '_name_' . $this
                ->lang
        ))
            ->where($prefix . '_enable = ?', 1);
        
        if (!empty($order)) $select->order($order);
        else {
            
            $select
                ->order($prefix . '_order ASC')
                ->order($prefix . '_name_' . $this->lang . ' ASC');
        }
        
        $rows = $tbl
            ->fetchAll($select)->toArray();
        
        foreach ($rows as $id => $val) {
            $items[$val[$prefix . '_id']] = $val[$prefix . '_name_' . $this->lang];
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function getSmartfiltersMultiselect() {
        
        $items = array();

        $db = Zend_Registry::get( 'db' );

        $rows = $db->query("
            SELECT sf.sf_id, sf.sf_name_" . $this->lang . ", ot.ot_name_" . $this->lang . "
            FROM sf_smartfilters AS sf
            LEFT JOIN ot_object_type AS ot
            ON sf.ot_id = ot.ot_id
            WHERE sf.sf_enable = 1
            ORDER BY sf.sf_level ASC, sf.sf_order ASC"
        )->fetchAll();

        foreach ($rows as $id => $val) {
            $items[$val['sf_id']] = $val["ot_name_" . $this->lang] . " - " . $val['sf_name_' . $this->lang];
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function getParamsFields($table, $prefix) {
        $items = array();

        $tbl = new Zend_Db_Table(array(
            'name' => $table,
            'primary' => $prefix . '_id'
        ));
        
        $rows = $tbl
            ->fetchAll($tbl
            ->select()
            ->from(array(
            $table
        ) , array(
            $prefix . '_id',
            $prefix . '_name_' . $this
                ->lang,
            $prefix . '_unit_' . $this
                ->lang,
            $prefix . '_type'
        ))
            ->where($prefix . '_enable = ?', 1)
            ->order($prefix . '_order ASC')
            ->order($prefix . '_name_' . $this
            ->lang . ' ASC'))
            ->toArray();
        
        foreach ($rows as $id => $val) {
            $items[$val[$prefix . '_id']] = array(
                'name' => $val[$prefix . '_name_' . $this
                    ->lang],
                'type' => $val[$prefix . '_type'],
                'unit' => $val[$prefix . '_unit_' . $this->lang],
            );
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function setValues($items, $values) {
        $items = (count($items) == 1 && count($items) > 0) ? array(
            $items
        ) : $items;
        
        $mdl_br_ids = array();
        
        foreach ($items as $id => $item_id) {
            $row = $this->find($item_id)->current();
            if (count($values) > 0) {
                foreach ($values as $field => $value) {
                    $row->$field = $value;
                }
            }
            $row->save();
        }
        
        if (count($mdl_br_ids) > 0) $this->updateBrands($mdl_br_ids);
    }
    
    ##########################################################################################
    public function getObjecttypeCode($values) {
        $items = "";
        $tbl = new Zend_Db_Table(array(
            'name' => 'ot_object_type',
            'primary' => 'ot_id'
        ));
        
        $rows = $tbl
            ->fetchAll(
                $tbl->select()
                ->from(array('ot_object_type') , array('ot_code'))
                ->where('ot_id IN (?)', array($values))
            )->toArray();
        
        foreach ($rows as $id => $val) {
            $items.= $val['ot_code'];
        }
        
        return $items;
    }

    ##########################################################################################
    public function getSmartsByObjecttype($ot_ids = array()) {
        
        $items = array();

        $db = Zend_Registry::get( 'db' );

        // print_r($values); die;

        if(count($ot_ids) > 0 && !empty($ot_ids))
        {
            $rows = $db->query("
                SELECT sf.sf_id, sf.sf_name_" . $this->lang . ", ot.ot_name_" . $this->lang . "
                FROM sf_smartfilters AS sf
                LEFT JOIN ot_object_type AS ot
                ON sf.ot_id = ot.ot_id
                WHERE sf.sf_enable = 1
                AND sf.ot_id IN (" . implode(",", $ot_ids) . ")
                ORDER BY sf.sf_level ASC, sf.sf_order ASC"
            )->fetchAll();

            foreach ($rows as $id => $val) {
                $items[$val['sf_id']] = $val["ot_name_" . $this->lang] . " - " . $val['sf_name_' . $this->lang];
            }
        }
        
        return $items;
    }

    ##########################################################################################
    public function getObjecttypeNames($values) {
        $items = array();
        $tbl = new Zend_Db_Table(array(
            'name' => 'ot_object_type',
            'primary' => 'ot_id'
        ));
        
        $rows = $tbl
            ->fetchAll($tbl
            ->select()
            ->from(array(
            'ot_object_type'
        ) , array(
            'ot_name_ru'
        ))
            ->where('ot_id IN (?)', $values))->toArray();
        
        foreach ($rows as $id => $val) {
            $items[] = $val['ot_name_ru'];
        }
        
        return $items;
    }
    
    ##########################################################################################
    public function getNextID() {
        $res = $this
            ->getAdapter()
            ->query("SHOW TABLE STATUS LIKE 'obj_object';");
        $item = $res->fetchObject();
        
        return $item->Auto_increment;
    }
    
    ##########################################################################################
    public function ajaxGetObjectPrices($params) {
        $obj_id = $params['obj_id'];
        $prc_tbl = new Zend_Db_Table(array(
            'name' => 'prc_price',
            'primary' => 'prc_id'
        ));
        
        $rows = $prc_tbl
            ->fetchAll($prc_tbl
            ->select()
            ->from(array(
            'prc_price'
        ) , array(
            'per_id',
            'prc_min_period',
            'prc_price'
        ))
            ->where('obj_id = ?', $obj_id))->toArray();
        
        return $rows;
    }

    ##########################################################################################
    public function rebuildItems()
    {
        $db = Zend_Registry::get( 'db' );

        // $kl = $db->query("
        //     SELECT * FROM kl_kladr
        // ")->fetchAll();

        // foreach($kl as $kl_id => $kl_val)
        // {
        // }

        return "OK";
    }    
}
?>
