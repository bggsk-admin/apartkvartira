<?
class Ai_Model_User extends Zend_Db_Table_Abstract {

    ##########################################################################################
	public $_name       = 'u_user';
	public $_primary    = 'u_id';

    ##########################################################################################
	public function createItem($formValues)
	{
 		$auth = Zend_Auth::getInstance();

        $row = $this->createRow($formValues);
        if($row) 
        {
            $row->u_password = md5($formValues['u_password_open']);
            $row->u_create = date ( 'Y-m-d H:i:s' );
            $row->u_creater = $auth->getIdentity()->u_username;
            $row->u_update = date ( 'Y-m-d H:i:s' );
            $row->u_updater = $auth->getIdentity()->u_username;
                        
            $row->save();

            return $row;

        } else {
            throw new Zend_Exception("Could not create item!");
        }

		$row = $this->createRow();
		
	}
	
    ##########################################################################################
	public function getItems()
	{
		$select = $this->select();
		$select->order(array('u_username', 'u_lastname', 'u_firstname'));
		return $this->fetchAll($select);
	}
	
    ##########################################################################################
	public function updateItem($formValues)
	{
 		$auth = Zend_Auth::getInstance();

		// fetch the user's row
		$row = $this->find($formValues['u_id'])->current();
		
		if($row) 
        {
			// update the row values
			$row->u_active = $formValues['u_active'];
			$row->u_username = $formValues['u_username'];
			$row->u_firstname = $formValues['u_firstname'];
			$row->u_lastname = $formValues['u_lastname'];
			$row->u_role = $formValues['u_role'];
			$row->u_update = date ( 'Y-m-d H:i:s' );
			$row->u_updater = $auth->getIdentity()->u_username;
			$row->save();

			//return the updated user
			return $row;

		}else{
			throw new Zend_Exception("User update failed. User not found!");
		}

	}

    ##########################################################################################
	public function updateUserLoginTime($userId)
	{
		// fetch the user's row
		$row = $this->find($userId)->current();
		$row->u_lastlogin = date ( 'Y-m-d H:i:s' );
		$row->save();

		return $row;
	}

    ##########################################################################################
	public function updatePassword($formValues)
	{
 		$auth = Zend_Auth::getInstance();

		// fetch the user's row
		$row = $this->find($formValues['u_id'])->current();

		if($row) {
			
			//update the password
			$row->u_password = md5($formValues['u_password']);
			$row->u_update = date ( 'Y-m-d H:i:s' );
			$row->u_updater = $auth->getIdentity()->u_username;
			$row->save();

		}else{
			throw new Zend_Exception("Password update failed. User not found!");
		}
	}	

    ##########################################################################################
	public function deleteUser($id)
	{
		// fetch the user's row
		$row = $this->find($id)->current();
		
		if($row) {
			$row->delete();
		}else{
			throw new Zend_Exception("Could not delete user. User not found!");
		}
	}	
	
    ##########################################################################################
    public function getMultiselect($u_role)
    {
        $res = array();
        
        $select = $this->select('u_id', 'u_firstname');
        $select->where('u_role = ?', ucfirst($u_role));
        $items = $this->fetchAll($select)->toArray();
        
        foreach($items as $id=>$val) $res[$val['u_id']] = $val['u_firstname'];

        return $res;
    }
	
}
