<?php
require_once("Util.php");

class Ai_Model_Pages extends Zend_Db_Table_Abstract {

    public $_name       = 'p_pages';
    public $_primary    = 'p_id';

    ##########################################################################################
    public function init()
    {
        $this->prfx = "p_";
    }
    
    ##########################################################################################
    public function getItems( $where = array(), $order = array() )
    {
        $select = $this->select()          
                ->from(array('p_pages', array('*') ))
                ->order(array( $this->prfx . "date"));        

        if(!empty($where)) foreach ( $where AS $condition ) { $select->where( $condition ); }
        if(!empty($order)) $select->order( $order );
        
        return $this->fetchAll($select);
    }

    ##########################################################################################
    public function createItem($formValues)
    {
        $row = $this->createRow($formValues);
        if($row) 
        {
            $row->save();
            return $row;

        } else {
            throw new Zend_Exception("Could not create item!");
        }
    }

    ##########################################################################################
    public function updateItem($formValues)
    {
        $row = $this->find($formValues[$this->_primary])->current();
        $row->setFromArray($formValues);

        if($row) 
        {
            $row->save();
            return $row;

        } else {
            throw new Zend_Exception("Item update failed. Item not found!");
        }

    }

    ##########################################################################################
    public function deleteItem($id)
    {
        $row = $this->find($id)->current();
        
        if($row) {
            $row->delete();
        }else{
            throw new Zend_Exception("Could not delete item. Item not found!");
        }
    }    

    ##########################################################################################
    public function deleteItems($items_id)
    {
        if(count($items_id) > 0)
        {
            foreach($items_id as $id => $r_id)
            {
                $this->deleteItem($r_id);
            }

        }
    }

    ##########################################################################################
    public function setValues($items, $values)
    {
        if(count($items) > 0)
        {
            foreach($items as $id => $item_id)
            {
                $row = $this->find($item_id)->current();
                if(count($values) > 0)
                {
                    foreach($values as $field => $value)
                    {
                        $row->$field = $value;
                    }
                }
                $row->save();
            }        
        }
        
    }

}  
?>
