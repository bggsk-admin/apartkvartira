<?php
    class Ai_Model_Parameter extends Zend_Db_Table_Abstract {

        public $_name = 'prm_parameter';
        public $_primary = 'prm_id';

        ##########################################################################################
        public function init()
        {
            $this->db = Zend_Registry::get( 'db' );
            $locale  = new Zend_Session_Namespace('locale');

            $this->lang = $locale->curlocale['lang'];

            $this->pref     = "prm_";
            $this->id       = $this->pref . 'id';
            $this->name     = $this->pref . 'name_' . $this->lang;
        }
        ##########################################################################################
        public function getItems()
        {
            $select = $this->select();
            return $this->fetchAll($select);
        }

        ##########################################################################################
        public function createItem($formValues)
        {
            $prv_prm_tbl = new Zend_Db_Table(array('name'=>'prv_prm_values', 'primary'=>'prv_id'));

            $row_prm = $this->createRow($formValues);
            if($row_prm) {

                $row_prm->save();

                //Если поле типа "Список значений" то сохраняем значения параметров
                if($formValues['prv_type'] == 'list')
                {
                    foreach($formValues['prv_name_ru'] as $id => $val)
                    {
                        $row_prv = $prv_prm_tbl->createRow();
                        if($row_prv) {

                            $row_prv->prm_id = $row_prm->prm_id;
                            $row_prv->prv_enable = 1;
                            $row_prv->prv_order = 0;
                            $row_prv->prv_name_en = $formValues['prv_name_en'][$id];
                            $row_prv->prv_name_ru = $val;
                            $row_prv->save();

                        } else {
                            throw new Zend_Exception("Could not create prv_prm_values row!");
                        }
                    }
                }

                return $row_prm;

            } else {

                throw new Zend_Exception("Could not create item!");

            }
        }


        ##########################################################################################
        public function updateItem($formValues)
        {
            //Update prv_prm_values table. Parameters values.
            $prv_prm_tbl = new Zend_Db_Table(array('name'=>'prv_prm_values', 'primary'=>'prv_id'));
            $prv_prm_tbl->delete(array('prm_id = ?' => $formValues['prm_id']));

            if($formValues['prm_type'] == 'list')
            {
                foreach($formValues['prv_name_ru'] as $id => $val)
                {
                    $row = $prv_prm_tbl->createRow();
                    if($row) {

                        $row->prm_id = $formValues['prm_id'];
                        $row->prv_enable = 1;
                        $row->prv_order = 0;
                        $row->prv_name_en = $formValues['prv_name_en'][$id];
                        $row->prv_name_ru = $val;
                        $row->save();

                    } else {
                        throw new Zend_Exception("Could not create prv_prm_values row!");
                    }
                }
            }

            $row = $this->find($formValues['prm_id'])->current();
            $row->setFromArray($formValues);

            if($row)
            {
                $row->save();
                return $row;

            } else {
                throw new Zend_Exception("Item update failed. Item not found!");
            }

        }

        ##########################################################################################
        public function deleteItems($items_id)
        {
            if(count($items_id) > 0)
            {
                foreach($items_id as $id => $item_id)
                {
                    $row = $this->find($item_id)->current();

                    if($row)
                    {
                        //Update prv_prm_values table. Parameters values.
                        $prv_prm_tbl = new Zend_Db_Table(array('name'=>'prv_prm_values', 'primary'=>'prv_id'));
                        $prv_prm_tbl->delete(array('prm_id = ?' => $row->prm_id));

                        $row->delete();
                    }
                    else        throw new Zend_Exception("Could not delete item. Item not found!");
                }

            }
        }

        ##########################################################################################
        public function cloneItem($id)
        {
            if($id > 0)
            {
                $row = $this->find($id)->current()->toArray();
                unset($row[$this->id]);
                $row[$this->name] .= " -COPY-";
                $this->createItem($row);
            }
        }

        ##########################################################################################
        public function getParameterSelect()
        {
            $res = "";
            $items = array();

            $select =  $this->select()
            ->where( "prm_enable", 1 )
            ;

            $rows = $this->fetchAll($select)->toArray();
            foreach($rows as $id => $val)
            {
                $items[$val['prm_id']] = $val['prm_name_' . $this->lang];
            }

            return $items;

        }

        ##########################################################################################
        public function setValues($items, $values)
        {
            $items = (count($items) == 1 && count($items) > 0) ? array($items) : $items;

            $mdl_br_ids = array();

            foreach($items as $id => $item_id)
            {
                $row = $this->find($item_id)->current();
                if(count($values) > 0)
                {
                    foreach($values as $field => $value)
                    {
                        $row->$field = $value;
                    }
                }
                $row->save();
            }

            if(count($mdl_br_ids) > 0) $this->updateBrands( $mdl_br_ids );

        }

        ##########################################################################################
        public function getParameterValues($params)
        {
            $prm_id = $params['prm_id'];
            $prv_prm_tbl = new Zend_Db_Table(array('name'=>'prv_prm_values', 'primary'=>'prv_id'));

            $rows = $prv_prm_tbl->fetchAll(
                $prv_prm_tbl->select()
                ->from( array('prv_prm_values'), array('prv_name_ru', 'prv_name_en') )
                ->where('prm_id = ?', $prm_id)
                ->where('prv_enable = ?', 1)
                ->order("prv_order ASC")
            )->toArray();

            return $rows;
        }

    }
?>
