<?php
require_once("Util.php");

class Ai_Model_Reset extends Zend_Db_Table_Abstract {

    public $_name       = '';
    public $_primary    = '';

    public function init()
    {
    }

    public function dropTable( $table, $primary, $where = array() )
    {
        $this->$table = new Zend_Db_Table(array('name' => $table, 'primary' => $primary));
        return $this->$table->delete( $where );
    }
    
    
}  
?>
