<?php
class Ai_Model_Renttype extends Zend_Db_Table_Abstract {

    public $_name = 'rt_rent_type';
    public $_primary = 'rt_id';

    ##########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get( 'db' );
        $locale  = new Zend_Session_Namespace('locale');

        $this->lang = $locale->curlocale['lang'];

        $this->pref     = "rt_";
        $this->id       = $this->pref . 'id';
        $this->name     = $this->pref . 'name_' . $this->lang;
    }
    ##########################################################################################
    public function getItems()
    {
        $select = $this->select();
        return $this->fetchAll($select);
    }

    ##########################################################################################
    public function createItem($formValues)
    {
        $rowItem = $this->createRow($formValues);
        if($rowItem) {

            $rowItem->save();
            return $rowItem;

        } else {

            throw new Zend_Exception("Could not create item!");

        }
    }


    ##########################################################################################
    public function updateItem($formValues)
    {
        $row = $this->find($formValues['rt_id'])->current();
        $row->setFromArray($formValues);

        if($row)
        {
            $row->save();
            return $row;

        } else {
            throw new Zend_Exception("Item update failed. Item not found!");
        }

    }

    ##########################################################################################
    public function deleteItems($items_id)
    {
        if(count($items_id) > 0)
        {
            foreach($items_id as $id => $item_id)
            {
                $row = $this->find($item_id)->current();

                if($row)
                {
                    #$ctgTable = new Zend_Db_Table(array('name'=>'ctg_categories', 'primary'=>'ctg_id'));
                    #$ctgTable->update( array("ctg_tpl_id" => NULL), array("ctg_tpl_id = '".$item_id."'"));

                    $row->delete();
                }
                else        throw new Zend_Exception("Could not delete item. Item not found!");
            }

        }
    }

    ##########################################################################################
    public function cloneItem($id)
    {
        if($id > 0)
        {
            $row = $this->find($id)->current()->toArray();
            unset($row[$this->id]);
            $row[$this->name] .= " -COPY-";
            $this->createItem($row);
        }
    }

    ##########################################################################################
    public function getSelectList()
    {
        $res = "";
        $items = array();

        $select =  $this->select()
                    ->where( "rt_enable", 1 )
                    ;

        $rows = $this->fetchAll($select)->toArray();
        foreach($rows as $id => $val)
        {
            $items[$val['rt_id']] = $val['rt_name_' . $this->lang];
        }

        return $items;

    }

    ##########################################################################################
    public function setValues($items, $values)
    {
        $items = (count($items) == 1 && count($items) > 0) ? array($items) : $items;

        $mdl_br_ids = array();

        foreach($items as $id => $item_id)
        {
            $row = $this->find($item_id)->current();
            if(count($values) > 0)
            {
                foreach($values as $field => $value)
                {
                    $row->$field = $value;
                }
            }
            $row->save();
        }

        if(count($mdl_br_ids) > 0) $this->updateBrands( $mdl_br_ids );

    }



}
?>
