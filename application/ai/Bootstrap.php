<?php
    class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
    {
        public $zfdebug_on = 0;

        protected function _initLogSettings()
        {
            $logger = new Zend_Log();
            $writer = new Zend_Log_Writer_Stream( APPLICATION_PATH . '/data/logs/applicationException.log' );
            $logger->addWriter($writer);
            $logger->registerErrorHandler();

            Zend_Registry::set('logger',$logger);

            return $logger;
        }

        protected function _initSession()
        {
            $options = $this->getOptions();
            $sessionOptions = array(
                'save_path' => $options['resources']['session']['save_path'],
                'cookie_lifetime' => $options['resources']['session']['remember_me_seconds'],
                'remember_me_seconds' => $options['resources']['session']['remember_me_seconds'],

                /*            'bug_compat_42' => '',
                'bug_compat_warn' => '',
                'cache_expire' => '180',
                'cache_limiter' => 'nocache',
                'cookie_domain' => '',
                'cookie_httponly' => '',
                'cookie_path' => '/',
                'cookie_secure' => '0',
                'entropy_file' => '',
                'entropy_length' => '0',
                'gc_divisor' => '1000',
                'gc_maxlifetime' => '1440',
                'gc_probability' => '1',
                'hash_bits_per_character' => '5',
                'hash_function' => '0',
                'name' => 'TaMeR_SESSID',
                'referer_check' => '',
                'save_handler' => 'user',
                'save_path' => '',
                'serialize_handler' => 'php',
                'use_cookies' => '1',
                'use_only_cookies' => 'on',
                'use_trans_sid' => '0',
                'strict' => false,
                'throw_startup_exceptions' => true,
                */
            );
            Zend_Session::setOptions($sessionOptions);
            Zend_Session::start();
        }

        protected function _initAutoloader()
        {
            $options = $this->getOptions();

            $loader = new Zend_Loader_Autoloader_Resource(array(
                'basePath'  => APPLICATION_PATH,
                'namespace' => 'Ai',
            ));

            $loader->addResourceType('form', 'forms', 'Form')
            ->addResourceType('model', 'models', 'Model')
            ->addResourceType('plugin', 'plugins', 'Plugin');

            return $loader;
        }

        protected function _initPlugins()
        {
            $front = Zend_Controller_Front::getInstance();
            $front->registerPlugin(new Ai_Plugin_AccessControl());
            $front->registerPlugin(new Ai_Plugin_LangSelector());
            $front->registerPlugin(new Zend_Controller_Plugin_ErrorHandler());

            return $front;
        }

        protected function _initDataBase()
        {
            $this->bootstrap('db');
            $db = $this->getPluginResource('db')->getDbAdapter();

            Zend_Registry::set('db', $db);

            return $db;
        }

        protected function _initLocalesList()
        {
            $options = $this->getOptions();

            $registry = Zend_Registry::getInstance();
            $registry->set('locales',  $options['locales']);

            return $registry;
        }

        protected function _initViewSettings()
        {
            $this->bootstrap('view');
            $view = $this->getResource('view');

            $view->addHelperPath('Ai/View/Helper', 'Ai_View_Helper');

            // ---------------------------- ACL --------------------------------- //
            $auth = Zend_Auth::getInstance();
            if ( $auth->hasIdentity() )
                $userType = $auth->getIdentity()->u_role;

            $view->acl = new Ai_Plugin_AccessControl();
            $view->hasIdentity = $auth->hasIdentity();
            $view->userType = (isset($userType)) ? $userType : 'guest';

            return $view;
        }

        protected function _initZFDebug()
        {
            if($this->zfdebug_on)
            {
                $autoloader = Zend_Loader_Autoloader::getInstance();
                $autoloader->registerNamespace('ZFDebug');

                $options = array(
                    'plugins' => array('Variables',
                        'File' => array('base_path' => APPLICATION_PATH ),
                        'Memory',
                        'Time',
                        'Registry',
                        'Exception')
                );

                # Instantiate the database adapter and setup the plugin.
                # Alternatively just add the plugin like above and rely on the autodiscovery feature.
                if ($this->hasPluginResource('db')) {
                    $this->bootstrap('db');
                    $db = $this->getPluginResource('db')->getDbAdapter();
                    $options['plugins']['Database']['adapter'] = $db;
                }

                # Setup the cache plugin
                if ($this->hasPluginResource('cache')) {
                    $this->bootstrap('cache');
                    $cache = $this-getPluginResource('cache')->getDbAdapter();
                    $options['plugins']['Cache']['backend'] = $cache->getBackend();
                }

                $debug = new ZFDebug_Controller_Plugin_Debug($options);

                $this->bootstrap('frontController');
                $frontController = $this->getResource('frontController');
                $frontController->registerPlugin($debug);
            }

        }


        public function _initNavigation()
        {
            // Бутстрапим View
            $this->bootstrapView();
            $view = $this->getResource('view');

            $pages = array(

                // Главная
                array(
                    'controller'    => 'index',
                    'action'    => 'index',
                    'label'         => _('Homepage'),
                    'privilege'     => 'index',
                    'pages' =>
                    array (
                    ),
                ),

                // ***************************************** Manage objects
                array(
                    'controller'    => 'manageobjects',
                    'action'        => 'index',
                    'label'         => _('Manage objects'),
                    'privilege'     => 'index',
                    'params'        => array('dropdown' => true),
                    'pages' => array (
                    
                        // ***************************************** Objects
                        array (
                            'controller' => 'object',
                            'label'         => _('Objects'),
                            'privilege'     => 'object',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'object',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//object - create
                                array(
                                    'controller' => 'object',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//object - update
                            ),
                        ),//object

                        // ***************************************** Smartfilters
                        array (
                            'controller' => 'smartfilters',
                            'label'         => _('Smartfilters'),
                            'privilege'     => 'smartfilters',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'smartfilters',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//restriction - create
                                array(
                                    'controller' => 'smartfilters',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//restriction - update
                            ),
                        ),//renttype




                    ),
                ),//manageobjects

                // ***************************************** Data dictionaries
                array(
                    'controller'    => 'datadict',
                    'action'    => 'index',
                    'label'         => _('Data dictionaries'),
                    'privilege'     => 'datadict',
                    'params'        => array('dropdown' => true),
                    'pages' => array (

                        // ***************************************** Service
                        array (
                            'controller' => 'service',
                            'label'         => _('Services'),
                            'privilege'     => 'service',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'service',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//service - create
                                array(
                                    'controller' => 'service',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//service - update
                            ),
                        ),//service

                        // ***************************************** Equipment
                        array (
                            'controller' => 'equipment',
                            'label'         => _('Equipment'),
                            'privilege'     => 'equipment',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'equipment',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//equipment - create
                                array(
                                    'controller' => 'equipment',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//equipment - update
                            ),
                        ),//equipment

                        // ***************************************** Object parameters
                        array (
                            'controller' => 'parameter',
                            'label'         => _('Object parameters'),
                            'privilege'     => 'parameter',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'parameter',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//objecttype - create
                                array(
                                    'controller' => 'parameter',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//objecttype - update
                            ),
                        ),//objecttype

                        // ***************************************** Object type
                        array (
                            'controller' => 'objecttype',
                            'label'         => _('Object types'),
                            'privilege'     => 'objecttype',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'objecttype',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//objecttype - create
                                array(
                                    'controller' => 'objecttype',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//objecttype - update
                            ),
                        ),//objecttype

                        // ***************************************** Period
                        array (
                            'controller' => 'period',
                            'label'         => _('Periods'),
                            'privilege'     => 'period',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'period',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//period - create
                                array(
                                    'controller' => 'period',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//period - update
                            ),
                        ),//period

                        // ***************************************** Rent type
                        /*
                        array (
                        'controller' => 'renttype',
                        'label'         => _('Rent types'),
                        'privilege'     => 'renttype',
                        'resource'      => 'mvc:editors',
                        'pages' => array(
                        array(
                        'controller' => 'renttype',
                        'action' => 'create',
                        'label'         => _('Creating'),
                        'privilege'     => 'create',
                        ),//renttype - create
                        array(
                        'controller' => 'renttype',
                        'action' => 'update',
                        'label'         => _('Update'),
                        'privilege'     => 'update',
                        ),//renttype - update
                        ),
                        ),//renttype
                        */

                        // ***************************************** Amenity type
                        array (
                            'controller' => 'amenity',
                            'label'         => _('Amenities'),
                            'privilege'     => 'amenity',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'amenity',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//amenity - create
                                array(
                                    'controller' => 'amenity',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//amenity - update
                            ),
                        ),//renttype

                        // ***************************************** KLADR
                        array (
                            'controller' => 'kladr',
                            'label'         => _('KLADR'),
                            'privilege'     => 'kladr',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'kladr',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//amenity - create
                                array(
                                    'controller' => 'kladr',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//amenity - update
                                array(
                                    'controller' => 'kladr',
                                    'action' => 'recalc',
                                    'label'         => _('Recalculate objects'),
                                    'privilege'     => 'recalc',
                                ),//amenity - update
                            ),
                        ),//renttype

                        // ***************************************** Restrict type
                        array (
                            'controller' => 'restriction',
                            'label'         => _('Restrictions'),
                            'privilege'     => 'restriction',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'restriction',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//restriction - create
                                array(
                                    'controller' => 'restriction',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//restriction - update
                            ),
                        ),//renttype


                        // ***************************************** Smartfilters type
                        array (
                            'controller' => 'sftype',
                            'label'         => _('Smartfilters type'),
                            'privilege'     => 'sftype',
                            'resource'      => 'mvc:editors',
                            'pages' => array(
                                array(
                                    'controller' => 'sftype',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//restriction - create
                                array(
                                    'controller' => 'sftype',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//restriction - update
                            ),
                        ),//renttype

                    ),
                ),//datadict


                array(
                    'controller'    => 'sys',
                    'label'         => _('System'),
                    'privilege'     => 'sys',
                    'resource'      => 'mvc:editors',
                    'params'        => array('dropdown' => true),
                    'pages' => array (
                        array(
                            'controller' => 'user',
                            'label'         => _('Users'),
                            'privilege'     => 'user',
                            'pages' => array(
                                array(
                                    'controller' => 'user',
                                    'action' => 'create',
                                    'label'         => _('Create'),
                                    'privilege'     => 'create',
                                ),//user - create
                                array(
                                    'controller' => 'user',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//user - update
                            ),
                        ),//user

                        array(
                            'controller' => 'reset',
                            'label'         => _('Reset DB'),
                            'privilege'     => 'reset',
                        ),//reset

                        // ***************************************** Settings
                        array(
                            'controller' => 'config',
                            'label'         => _('Settings'),
                            'privilege'     => 'config',
                            'pages' => array(
                                array(
                                    'controller' => 'config',
                                    'action' => 'create',
                                    'label'         => _('Creating'),
                                    'privilege'     => 'create',
                                ),//config - create
                                array(
                                    'controller' => 'config',
                                    'action' => 'update',
                                    'label'         => _('Update'),
                                    'privilege'     => 'update',
                                ),//config - update
                            ),
                        ),//config

                        array(
                            'controller' => 'log',
                            'label'         => _('Log'),
                            'privilege'     => 'log',
                        ),//log
                        array(
                            'controller' => 'dbdump',
                            'label'         => _('DB Backup'),
                            'privilege'     => 'dbdump',
                        ),//dbdump
                    ),
                ),//sys

            );//pages array

            // Создаем новый контейнер на основе нашей структуры
            $container = new Zend_Navigation($pages);
            // Передаем контейнер в View
            $view->navigation($container);

            return $container;
        }


}