<?
    class Ai_Form_Renttype extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'rt_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'rt_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    //'attribs' => array('class' => 'pretty', 'data-label' => $this->getView()->translate('Enabled'), 'data-not-label' => $this->getView()->translate('Disabled')),
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'rt_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'rt_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'rt_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'rt_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'rt_from_days',
                array(
                    'label' => $this->getView()->translate('Rent start period'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'rt_from_days', 'placeholder' => $this->getView()->translate('From') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'rt_to_days',
                array(
                    'label' => $this->getView()->translate('Rent end period'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'rt_to_days', 'placeholder' => $this->getView()->translate('To') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'rt_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Object type name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'rt_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }



        }

    } #class
