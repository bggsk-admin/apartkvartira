<?
    class Ai_Form_ObjectType extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $ot_model = new Ai_Model_ObjectType();

            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'ot_id',       array('decorators' => array('ViewHelper')) );

            $ot_pid = $ot_model->getSelectCtgTree();
            $this->addElement(
                'select',
                'ot_pid',
                array(
                    'label' => $this->getView()->translate('Parent'),
                    'required' => false,
                    'multiOptions' => $ot_pid,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => '...'),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
                )
            );

            $this->addElement('checkbox',   'ot_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    //'attribs' => array('class' => 'pretty', 'data-label' => $this->getView()->translate('Enabled'), 'data-not-label' => $this->getView()->translate('Disabled')),
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'ot_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ot_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'ot_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ot_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'ot_code',
                array(
                    'label' => $this->getView()->translate('Code'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ot_code', 'placeholder' => $this->getView()->translate('Code') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            // Booking Fee (percent)
            $this->addElement('text', 'ot_bookfee_pct',
                array(
                    'label' => $this->getView()->translate('Percent from summ'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ot_bookfee_pct', 'placeholder' => $this->getView()->translate('Percent from summ') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            // Booking Fee (fix)
            $this->addElement('text', 'ot_bookfee_fix',
                array(
                    'label' => $this->getView()->translate('Fixed price'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ot_bookfee_fix', 'placeholder' => $this->getView()->translate('Fixed price') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'ot_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Object type name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'ot_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('text', 'ot_name_plural_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Object type name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'ot_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }

        }

    } #class
