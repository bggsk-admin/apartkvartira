<?
    class Ai_Form_Kladr extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden', 'kl_id', array('decorators' => array('ViewHelper')) );

            $this->addElement('text', 'kl_city',
                array(
                    'label' => $this->getView()->translate('City ID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'kl_city', 'placeholder' => $this->getView()->translate('City ID'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'kl_keyid',
                array(
                    'label' => $this->getView()->translate('Key ID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'kl_keyid', 'placeholder' => $this->getView()->translate('Key ID'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'kl_name',
                array(
                    'label' => $this->getView()->translate('Name'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'kl_name', 'placeholder' => $this->getView()->translate('Name'), 'readonly' => 'readonly'  ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'kl_zip',
                array(
                    'label' => $this->getView()->translate('ZIP'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'kl_zip', 'placeholder' => $this->getView()->translate('ZIP'), 'readonly' => 'readonly'  ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'kl_type',
                array(
                    'label' => $this->getView()->translate('Type'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'kl_type', 'placeholder' => $this->getView()->translate('Type'), 'readonly' => 'readonly'  ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'kl_type_short',
                array(
                    'label' => $this->getView()->translate('Type short'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'kl_type_short', 'placeholder' => $this->getView()->translate('Type short'), 'readonly' => 'readonly'  ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'kl_okato',
                array(
                    'label' => $this->getView()->translate('OKATO'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'kl_okato', 'placeholder' => $this->getView()->translate('OKATO'), 'readonly' => 'readonly'  ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'kl_district',
                array(
                    'label' => $this->getView()->translate('District'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'kl_district', 'placeholder' => $this->getView()->translate('District') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'kl_2gis',
                array(
                    'label' => $this->getView()->translate('2GIS Name'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'kl_2gis', 'placeholder' => $this->getView()->translate('2GIS Name') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

        }

    } #class
