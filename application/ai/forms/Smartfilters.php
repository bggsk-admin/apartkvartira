<?
    class Ai_Form_Smartfilters extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $sft_model = new Ai_Model_Sftype();
            $o_model = new Ai_Model_Object();

            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'sf_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'sf_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            // $this->addElement('checkbox',   'sf_istitle',
            //     array(
            //         'label' => $this->getView()->translate('Is title level'),
            //         'required' => false,
            //         'attribs' => array('class' => ''),
            //         'decorators' => array('HorizontalForm')
            // ));

            $this->addElement('text', 'sf_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'sf_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'sf_order_navbar',
                array(
                    'label' => $this->getView()->translate('Order for navbar'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'sf_order_navbar', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'sf_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'sf_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $sf_level = array();
            for($i_level = 1; $i_level <= 20; $i_level ++ )
            {
                $sf_level[$i_level] = $i_level;
            }

            $this->addElement(
                'select', 
                'sf_level',
                array(
                    'label' => $this->getView()->translate('Level'),
                    'required' => false,
                    'multiOptions' => $sf_level,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => $this->getView()->translate('Select one')),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                )
            );

            $sf_type = $sft_model->getItemsForSelect();

            $this->addElement(
                'select',
                'sft_id',
                array(
                    'label' => $this->getView()->translate('Smartfilter type'),
                    'required' => false,
                    'multiOptions' => $sf_type,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => $this->getView()->translate('Select one')),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                )
            );

            $ot_object_type = $o_model->getMultiselect('ot_object_type', 'ot');

            $this->addElement(
                'select',
                'ot_id',
                array(
                    'label' => $this->getView()->translate('Object type'),
                    'required' => false,
                    'multiOptions' => $ot_object_type,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => $this->getView()->translate('Select one')),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                )
            );


            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'sf_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Smartfilter name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'sf_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('text', 'sf_name_navbar_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Smartfilter name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'sf_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }



        }

    } #class
