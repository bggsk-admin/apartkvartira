<?
    class Ai_Form_Owner extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'own_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'own_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    //'attribs' => array('class' => 'pretty', 'data-label' => $this->getView()->translate('Enabled'), 'data-not-label' => $this->getView()->translate('Disabled')),
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $own_type = array(
                "fiz" => "Физическое лицо",
                "jur" => "Юридическое лицо",
                "ip" => "Индивидуальный предприниматель"
            );

            $this->addElement(
                'select',
                'own_type',
                array(
                    'label' => $this->getView()->translate('Own type'),
                    'required' => false,
                    'multiOptions' => $own_type,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => '...'),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
                )
            );

            $this->addElement('text', 'own_email',
                array(
                    'label' => $this->getView()->translate('Email'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_email', 'placeholder' => $this->getView()->translate('Email') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'own_phone',
                array(
                    'label' => $this->getView()->translate('Phone'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_phone', 'placeholder' => $this->getView()->translate('Phone') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));


            ############################################### Паспортные данные
            $this->addElement('text', 'own_firstname',
                array(
                    'label' => $this->getView()->translate('Firstname'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_firstname', 'placeholder' => $this->getView()->translate('Firstname') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'own_lastname',
                array(
                    'label' => $this->getView()->translate('Lastname'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_lastname', 'placeholder' => $this->getView()->translate('Lastname') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'own_middlename',
                array(
                    'label' => $this->getView()->translate('Middlename'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_middlename', 'placeholder' => $this->getView()->translate('Middlename') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'own_dob',
                array(
                    'label' => $this->getView()->translate('Date of birth'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_dob', 'placeholder' => $this->getView()->translate('Date of birth') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'own_pasport_sn',
                array(
                    'label' => $this->getView()->translate('Serial number'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_pasport_sn', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'own_pasport_num',
                array(
                    'label' => $this->getView()->translate('Number'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_pasport_num', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'own_pasport_doi',
                array(
                    'label' => $this->getView()->translate('Issue date'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_pasport_doi', 'placeholder' => $this->getView()->translate('Issue date') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'own_pasport_issue',
                array(
                    'label' => $this->getView()->translate('Issue place'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'own_pasport_issue', 'placeholder' => $this->getView()->translate('Issue place') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

        }

    } #class
