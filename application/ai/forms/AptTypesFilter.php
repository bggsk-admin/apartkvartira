<?
class Ai_Form_AptTypesFilter extends Zend_Form
{

    ##########################################################################################
    public function init()
    {
        $tplModel = new Ai_Model_AptTpl();

        $locale  = new Zend_Session_Namespace('locale');

        $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
        $this->setDecorators(array(
            array('ViewScript', array('viewScript'=>'apttypes/filter.phtml'))
        ));

        $this->setMethod("post");
        $this->setView($this->getView());

        //************************************ FORM FIELDS **********************************************************//
        $sort_field = array(
            '0' => '',
            'apttyp_name_' . $locale->curlocale['lang'] => $this->getView()->translate('Category name'),
        );
        $this->addElement('select', 'f_sort_field', array('label' => 'Sort by', 'multiOptions' => $sort_field, 'value' => '0', 'required' => false, 'attribs' => array('class' => 'span2 select2'), 'decorators' => array('HorizontalForm')  ));


        $apttyp_tpl_id = $tplModel->getTplSelector();
        $this->addElement('select',   'f_apttyp_tpl_id',   array('label' => $this->getView()->translate('Category template'), 'required' => false,
            'multiOptions' => $apttyp_tpl_id,
        'attribs' => array('class' => 'span3 select2' ), 'errorMessages' => array(''), 'decorators' => array('HorizontalForm') ));


        $this->getView()->setEscape('stripslashes');
    }

} #class
