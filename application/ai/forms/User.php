<?
class Ai_Form_User extends ZendX_JQuery_Form
{
    
    ##########################################################################################
    public function init()
    {
        
        $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
        $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

        $this->setMethod("post");

        $this->addElement('hidden',     'u_id',             array('decorators' => array('ViewHelper')) );

        $this->addElement('checkbox',   'u_active',         array('label' => $this->getView()->translate('Enable'), 'required' => false, 'attribs' => array('class' => ''), 'decorators' => array('HorizontalForm')) );
        $this->addElement('text',       'u_username',       array('label' => $this->getView()->translate('Username'), 'required' => true, 'filters' => array('StripTags'), 'attribs' => array('class' => 'span3', 'placeholder' => $this->getView()->translate('Username') ), 'errorMessages' => array(''), 'decorators' => array('HorizontalForm') ));
        $this->addElement('password',   'u_password',       array('label' => $this->getView()->translate('Password'), 'required' => false, 'attribs' => array('class' => 'span3', 'placeholder' => $this->getView()->translate('Password') ), 'errorMessages' => array(''), 'decorators' => array('HorizontalForm') ));
        $this->addElement('text',       'u_password_open',  array('label' => $this->getView()->translate('Password'), 'required' => false, 'attribs' => array('class' => 'span3', 'placeholder' => $this->getView()->translate('Password') ), 'errorMessages' => array(''), 'decorators' => array('HorizontalForm') ));
        
        $this->addElement('text',       'u_firstname',      array('label' => $this->getView()->translate('Firstname'), 'required' => false, 'filters' => array('StripTags'), 'attribs' => array('class' => 'span3', 'placeholder' => $this->getView()->translate('Firstname') ), 'errorMessages' => array(''), 'decorators' => array('HorizontalForm') ));
        $this->addElement('text',       'u_lastname',       array('label' => $this->getView()->translate('Lastname'), 'required' => false, 'filters' => array('StripTags'), 'attribs' => array('class' => 'span3', 'placeholder' => $this->getView()->translate('Lastname') ), 'errorMessages' => array(''), 'decorators' => array('HorizontalForm') ));

        
        $this->addElement('select',   'u_role',   array('label' => $this->getView()->translate('Role'), 'required' => true, 
            'multiOptions' => array(
                'customer'        => $this->getView()->translate('Customer'),
                'owner'        => $this->getView()->translate('Owner'),
                'editor'        => $this->getView()->translate('Editor'),
                'administrator' => $this->getView()->translate('Administrator'),
            ), 
        'attribs' => array('class' => 'span2 select2' ), 'errorMessages' => array(''), 'decorators' => array('HorizontalForm') ));

	}
	
} #class