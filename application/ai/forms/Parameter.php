<?
    class Ai_Form_Parameter extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'prm_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'prm_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'prm_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'prm_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'prm_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'prm_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('select', 'prm_type',
                array(
                    'label' => $this->getView()->translate('Parameter type'),
                    'required' => true,
                    'attribs' => array('class' => 'form-control select2', 'id' => 'prm_type', 'data-placeholder' => 'Select parameter type' ),
                    'multiOptions' => array(
                        ''     => '',
                        'text' => $this->getView()->translate('Text field'),
                        'list' => $this->getView()->translate('List of values'),
                        'number' => $this->getView()->translate('Number'),
                    ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'prm_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Parameter name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'prm_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('text', 'prm_unit_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Unit'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'prm_unit_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('text', 'prv_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Unit'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'prv_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }



        }

    } #class
