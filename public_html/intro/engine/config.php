<?php

/*	---------------------------------------
 *	SUBSCRIBE.PHP and FEEDBACK.PHP settings
 *	---------------------------------------
 */

global $conf;

# ---- SUBSCRIBE SETTINGS ---- #

# Subscribe mode [file OR email OR mailchimp]
$conf['subscribe-type'] = 'mailchimp';

# --- file mode	
	
	# emails can't repeat
	$conf['file-uniq'] = true;
	
	# path to file with email (the root directory is /engine)
	# IMPORTANT! Change the "secret_dir" on your own folder else you can gift a lot of emails for spam bases!
	$conf['file-path'] = 'secret_dir' . '/emails.txt';
	

# --- email mode
	
	# your email address
	$conf['email-address'] = 'info@apartkvartira.ru';
	
	# subject of mails
	$conf['email-subject'] = 'Обратная связь - Апартквартира.ру!';
	
	# owner of mail
	$conf['email-from'] = "From: Апартквартира.ру <info@{$_SERVER['HTTP_HOST']}>\r\n";
	
	
# --- mailchimp mode 

	# grab an API Key from Acount Settings -> Extras -> API keys -> Create A Key
	$conf['mailchimp-api'] = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-usx';
	
	# Uniq id for list (grab it from bottom of list setting's page)
	$conf['mailchimp-list-id'] = 'xxxxxxxxxx';
	
	
# ---- FEEDBACK SETTINGS ---- #

# your email address
// $conf['feedback-address'] = 'info@apartkvartira.ru';
$conf['feedback-address'] = '160961@mail.ru';

# subject of mails
$conf['feedback-subject-admin'] = "Сообщение от пользователя с Апартквартира.ру [{$_SERVER['SERVER_NAME']}]";
$conf['feedback-subject-user']  = "Спасибо за ваш отзыв [{$_SERVER['SERVER_NAME']}]";

# templates of mails (the root directory is /engine)
$conf['feedback-template-admin'] = 'templates/to_admin.tpl';
$conf['feedback-template-user']  = 'templates/to_user.tpl';