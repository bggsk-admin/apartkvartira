<?php
/////////////////////////////////////////////////////////////
// Admin Interace (AI) Enter Point.
// Alamdar Marsel (c) 2011
/////////////////////////////////////////////////////////////

date_default_timezone_set('Europe/Moscow');

define('APPLICATION_ZONE', 'AI');
define('IMG_PATH', '/images');

defined('PS') || define('PS', PATH_SEPARATOR);
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application/ai'));
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
define('PUBLIC_PATH', APPLICATION_PATH.'/../../public_html');
define('MODULE_NAME', 'ai');

$paths[] = APPLICATION_PATH;
$paths[] = APPLICATION_PATH.'/library';
$paths[] = APPLICATION_PATH.'/../../library';
$paths[] = APPLICATION_PATH.'/../../../library';
$paths[] = APPLICATION_PATH.'/../../../../library';
$app_path = implode(PS, $paths);
set_include_path($app_path.PS.get_include_path());

/** Zend_Application */
require_once 'Zend/Application.php';

/** Zend_Autoloader */
require_once 'Zend/Loader/Autoloader.php';

// Create application, bootstrap, and run
$application = new Zend_Application( APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini' );

// RUN
try {
	$application->bootstrap()->run();
} catch (Zend_Exception $e) {
	print_r($e->getMessage());
}