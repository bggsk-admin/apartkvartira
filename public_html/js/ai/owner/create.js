$(document).ready(function() {

    $("#own_phone").mask("+9 (999) 999-9999");
    $("#own_dob").mask("99 . 99 . 9999");
    $("#own_pasport_doi").mask("99 . 99 . 9999");
    $("#own_pasport_num").mask("9999 999999");

    //-------------------------------------------------- jQuery Validate -----------------------------------/
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z0-9_-]+$/i.test(value);
        }, "Please use only a-z0-9_-");


    if ($.fn.validate) {
        $('#main-form').validate({
            rules: {
                own_email: {
                    minlength: 7,
                    required: true,
                },
            },
            showErrors: function(errorMap, errorList) {

                $.each(this.validElements(), function (index, element) {
                    var $element = $(element);
                    $element.data("title", "").tooltip("destroy");

                    $element.closest('.form-group').removeClass('has-error').addClass('has-success');
                });

                $.each(errorList, function (index, error) {
                    var $element = $(error.element);

                    $element.tooltip("destroy")
                    .data("title", error.message)
                    .tooltip({placement:'right', trigger: 'manual', show: '100'}).tooltip('show');

                    $element.closest('.form-group').removeClass('has-success').addClass('has-error');
                });

            }

        });
    } else { alert("Validate method not loaded"); }


});// END JQUERY