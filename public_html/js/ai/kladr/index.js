$(document).ready(function() {

/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};

    //-------------------------------------------------- DataTable -----------------------------------/

var tableHeight = $(window).height() - 350;

    var datatable = $('#watable').dataTable({

        //"sDom": "<'row'<'col-xs-4'l><'col-xs-4'f><'col-xs-4'p>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
        "sDom": "<'row'<'col-xs-4'i><'col-xs-3'f><'col-xs-5'p>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
        "oLanguage": {
            "sLengthMenu": "_MENU_ записей на страницу "
        },
        "order": [
            // [11, "DESC"] //Default Order by obj_update
        ],
        // "bAutoWidth": false,
        "aoColumnDefs": [
            {
                "aTargets": [0],
                "bSortable": false
            }, 
        ],
        "oLanguage": {
            "sUrl": "/js/dataTables.russian.txt"
        },
        "iDisplayLength": 50,
        
        // "scrollY": tableHeight + "px", 
        // "scrollCollapse": true,        
    
    });



});
