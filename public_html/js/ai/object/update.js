$(document).ready(function() {

    if (typeof(DG) == 'object') {
        DG.autoload(function() {

            var myMap = new DG.Map('gismap');

            var lon = ($('#obj_addr_lon').val() !== "") ? parseFloat($('#obj_addr_lon').val()) : 0;
            var lat = ($('#obj_addr_lat').val() !== "") ? parseFloat($('#obj_addr_lat').val()) : 0;

            if (lon > 0 && lat > 0) {
                var geopoint = new DG.GeoPoint(lon, lat);

                myMap.setCenter(geopoint);

                var marker = new DG.Markers.Common({
                    geoPoint: geopoint,
                    clickCallback: function(clickEvent, marker) {
                        // alert('Привет :)');
                    }
                });
                var markerHint = $('#obj_addr_street').val() + ' ' + $('#obj_addr_number').val();
                marker.setHintContent(markerHint);
                myMap.markers.add(marker);


            } else {
                myMap.setCenter(new DG.GeoPoint(38.9787146513723, 45.0504739491062));
            }


            //myMap.setCenter(new DG.GeoPoint(43.99150,56.31534));
            myMap.disableScrollZoom();
            myMap.enableDragging();
            myMap.zoomTo(17);
            myMap.controls.add(new DG.Controls.Zoom());


            /******************************************** function searchAddr *************************************/

            function searchAddr(address, map) {

                DG.Geocoder.get('краснодар ' + address, {

                    types: ['city', 'settlement', 'district', 'street', 'living_area', 'place', 'house'],
                    limit: 10,
                    success: function(geocoderObjects) {

                        myMap.markers.removeAll();

                        var FirstObjGeoPoint = geocoderObjects[0].getCenterGeoPoint();
                        map.setCenter(new DG.GeoPoint(FirstObjGeoPoint.getLon(), FirstObjGeoPoint.getLat()), 17);

                        // Обходим циклом все полученные геообъекты
                        for (var i = 0, len = geocoderObjects.length; i < len; i++) {
                            var geocoderObject = geocoderObjects[i];

                            // Получаем маркер из геообъекта с помощью метода getMarker.
                            // Первый параметр - иконка маркера, второй параметр - функция, которая сработает при клике на маркер
                            var markerIcon = null; // иконка по умолчанию
                            var marker = geocoderObject.getMarker(
                                markerIcon, (function(geocoderObject) {
                                    return function() {
                                        var info = '';

                                        // Дополнительная информация о геообъекте
                                        var attributes = geocoderObject.getAttributes();

                                        //alert (attributes);

                                        /*
                                        for (var attribute in attributes) {
                                        if (attributes.hasOwnProperty(attribute)) {

                                        info += attribute + ': ' + attributes[attribute] + '\n';

                                        }
                                        }
                                        */

                                        //alert (info);
                                        //alert(attributes['index'].length);

                                        $('#obj_addr_city').val(("city" in attributes && attributes['city'].length) ? attributes['city'] : '');
                                        $('#obj_addr_zip').val(("index" in attributes && attributes['index'].length) ? attributes['index'] : '');
                                        $('#obj_addr_district').val(("district" in attributes && attributes['district'].length) ? attributes['district'] : '');
                                        $('#obj_addr_microdistrict').val(("microdistrict" in attributes && attributes['microdistrict'].length) ? attributes['microdistrict'] : '');
                                        $('#obj_addr_street').val(("street" in attributes && attributes['street'].length) ? attributes['street'] : '');
                                        $('#obj_addr_number').val(("number" in attributes && attributes['number'].length) ? attributes['number'] : '');
                                        $('#obj_addr_elevation').val(("elevation" in attributes && attributes['elevation'].length) ? attributes['elevation'] : '');
                                        $('#obj_addr_buildtype').val(("elevation" in attributes && attributes['purpose'].length) ? attributes['purpose'] : '');

                                        var centerGeoPoint = geocoderObject.getCenterGeoPoint();
                                        $('#obj_addr_lon').val(centerGeoPoint.getLon());
                                        $('#obj_addr_lat').val(centerGeoPoint.getLat());
                                        $('#obj_addr_lon_view').val(centerGeoPoint.getLon());
                                        $('#obj_addr_lat_view').val(centerGeoPoint.getLat());

                                        //Автозаполнение названия объекта
                                        $('#obj_name_ru').val(attributes['street'] + ' ' + attributes['number']).change();

                                        $('html,body').animate({
                                            scrollTop: $("#main-form").offset().top - 200
                                        }, 'slow');

                                        $("#fs_coordinates, #fs_address").stop().css("background-color", "#FFFF9C").animate({
                                            backgroundColor: "#FFFFFF"
                                        }, 1000);

                                    }
                                })(geocoderObject) //Создаем анонимную функцию и сразу же ее вызываем
                            );

                            var attributes = geocoderObject.getAttributes();
                            var markerHint = attributes['street'] + ' ' + attributes['number'];
                            marker.setHintContent(markerHint);

                            map.markers.add(marker);

                        } // for

                        $('#search_address').closest('.row').removeClass('has-error').addClass('has-success');
                    }, // on success

                    // Обработка ошибок
                    failure: function(code, message) {
                        $('#search_address').closest('.row').removeClass('has-success').addClass('has-error');
                    } // on failure

                }); //DG.Geocoder.get

            }

            //-------------------------------------------------- Bind search action with delay -----------------------------------/
            $("#search_address").bindWithDelay("keyup", function() {
                searchAddr($("#search_address").val(), myMap);
            }, 0);

            //-------------------------------------------------- Prevent 'enter' key press when in search input -----------------------------------/
            $("#search_address").focus(function() {
                $(this).keypress(function(event) {
                    if (event.keyCode == 13) {
                        event.preventDefault();
                    }
                });
            });


            $("#search_address").click(function() {
                $('html,body').animate({
                    scrollTop: $("#search_address").offset().top - 160
                }, 'slow');
            });


        });
        //-- autoload --

    }
    // -- if DG.autoload exist -- 

    //Обновляем UID объекта по его названию
    $('#obj_name_ru').on('keyup change', function() {
        var uid = setTranslit($('#obj_name_ru').val());
        $('#obj_uid').val(uid);
    });

    //Обновляем UID объекта при заполнении квартиры
    $('#obj_addr_room').on('keyup change', function() {
        var uid = setTranslit($('#obj_name_ru').val()) + "-kv-" + $('#obj_addr_room').val();
        $('#obj_uid').val(uid);
    });


    //-------------------------------------------------- Booking Calendar Pro -----------------------------------/
    /*    $('#booking-calendar').DOPBackendBookingCalendarPRO({
        'DataURL': '/ai/object/ajax/method/loadcalendar',
        'SaveURL': 'dopbcp/php-database/save.php',
        'DayNames': ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        'MonthNames': ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        'CloseLabel': 'Закрыть',
        'BookedText': 'Бронь',
        'DateType': 2,

    });
*/

    /*------------------------------------------------------------------------------------
    /* Bootstrap Datetimepicker
    --------------------------------------------------------------------------------------*/

    $('#pick_cal_startdate').datetimepicker({
        language: 'ru',
        defaultDate: moment().format('L') + " 14:00",
    });

    $('#pick_cal_enddate').datetimepicker({
        language: 'ru',
        defaultDate: moment().add(3, 'days').format('L') + " 12:00",
    });

    /*------------------------------------------------------------------------------------
    /* FullCalendar init
    --------------------------------------------------------------------------------------*/

    $("#event-info").hide();
    var bookCal = $('#booking-calendar');

    bookCal.fullCalendar({

        header: {
            left: 'prev next today',
            center: 'title',
            right: 'prevYear nextYear'
        },
        buttonText: {
            today: 'Сегодня',
            month: 'month',
            week: 'week',
            day: 'day',
        },
        timezone: "UTC",

        firstDay: 1,
        contentHeight: 300,
        selectable: true,
        selectHelper: true,

        select: function(start, end) {
            $('#modal-add-event').modal('show');

            var cal_startdate = moment(start).format("DD.MM.YYYY 14:00");
            var cal_enddate = moment(end).format("DD.MM.YYYY 12:00");

            $('#cal_startdate').val(cal_startdate);
            $('#cal_enddate').val(cal_enddate);
        },
        eventClick: function(calEvent, jsEvent, view) {

            $("#event-info-id").val(calEvent.id);
            $("#event-info-title").text(calEvent.title);

            var startdate = moment(calEvent.start);
            var startdate_label = startdate.format("DD MMMM YYYY HH:mm (ddd)");
            $("#event-info-startdate-label").text(startdate_label);

            var enddate = moment(calEvent.end);
            var enddate_label = enddate.format("DD MMMM YYYY HH:mm (ddd)");
            $("#event-info-enddate-label").text(enddate_label);

            var diff_days = Math.round(enddate.diff(startdate, 'days', true));
            $("#event-info-days").text(diff_days);

            var diff_days_label = decOfNum(diff_days, ['день', 'дня', 'дней']);
            $("#event-info-days-label").text(diff_days_label);

            $("#event-info").show();

        },

        editable: true,

        events: {

            url: '/ai/object/ajax/method/loadcalendar/',
            error: function() {
                $('#cal-warning').show();
            },
            success: function(data, status) {
                $('#cal-warning').hide();

                $.each(data.data, function(index, event) {
                    bookCal.fullCalendar('renderEvent', event);
                });
            },
            data: function() {
                return {
                    id: $('#obj_id').val()
                };
            },
        },

    });

    /*------------------------------------------------------------------------------------
    /* Calendar Events Table (list view)
    --------------------------------------------------------------------------------------*/
    var calendar_list = $('#calendar-list').dataTable({

        "ajax": {
            "url": "/ai/object/ajax/method/loadeventstable/",
            "type": "POST",
            "data": {
                id: $('#obj_id').val()
            },
        },

        "sDom": "<'row'<'col-xs-4'i><'col-xs-4'f><'col-xs-4'p>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
        "oLanguage": {
            "sLengthMenu": "_MENU_ записей на страницу "
        },
        "oLanguage": {
            "sUrl": "/js/dataTables.russian.txt"
        },
        "iDisplayLength": 50,

        "order": [
            // [11, "DESC"] //Default Order by obj_update
        ],

    });

    /*------------------------------------------------------------------------------------
    /* FullCalendar - Apply action
    --------------------------------------------------------------------------------------*/
    $("#btn-cal-apply").click(function(event) {

        var $btn = $(this);
        $btn.button('loading');

        var events = bookCal.fullCalendar('clientEvents');

        $.ajax({
            dataType: 'json',
            cache: false,
            type: 'post',
            url: '/ai/object/ajax/method/savecalendar/',
            data: "json=" + JSON.stringify(events) + "&id=" + $('#obj_id').val(),

        });

        $("#fs_calendar").stop().css("background-color", "#FFFF9C").animate({
            backgroundColor: "#FFFFFF"
        }, 1000);


        $btn.button('reset');

        event.preventDefault();
    });

    /*------------------------------------------------------------------------------------
    /* FullCalendar - Reload action
    --------------------------------------------------------------------------------------*/
    $("#btn-cal-reload").click(function(event) {

        event.preventDefault();

        bookCal.fullCalendar("removeEvents");
        bookCal.fullCalendar("refetchEvents");

        $("#fs_calendar").stop().css("background-color", "#FFFF9C").animate({
            backgroundColor: "#FFFFFF"
        }, 1000);

        // $('#calendar-list').dataTable().ajax.reload();        
    });

    /*------------------------------------------------------------------------------------
    /* FullCalendar - Delete Event action
    --------------------------------------------------------------------------------------*/
    $("#modal-delete-event .btn-danger").click(function(event) {

        var eventID = parseInt($("#event-info-id").val());

        bookCal.fullCalendar('removeEvents', [eventID]);


        //------------- Apply Events ------------------------------
        var events = bookCal.fullCalendar('clientEvents');
        $.ajax({
            dataType: 'json',
            cache: false,
            type: 'post',
            url: '/ai/object/ajax/method/savecalendar/',
            data: "json=" + JSON.stringify(events) + "&id=" + $('#obj_id').val(),

        });
        //------------- Apply Events - END  ------------------------------

        $("#event-info").hide();

        $("#fs_calendar").stop().css("background-color", "#FFFF9C").animate({
            backgroundColor: "#FFFFFF"
        }, 1000);



        $('#modal-delete-event').modal('hide');
        event.preventDefault();
    });

    /*------------------------------------------------------------------------------------
    /* FullCalendar - Add Event action
    --------------------------------------------------------------------------------------*/
    $("#modal-add-event .btn-primary").click(function(event) {

        var cal_startdate = $('#cal_startdate').val();
        cal_startdate = moment(cal_startdate, "DD.MM.YYYY HH:mm").format("YYYY-MM-DD HH:mm");

        var cal_enddate = $('#cal_enddate').val();
        cal_enddate = moment(cal_enddate, "DD.MM.YYYY HH:mm").format("YYYY-MM-DD HH:mm");

        var cal_status = $('#cal_status').val();
        var eventData = "";

        var cal_title = "";
        if (cal_status == "booked") cal_title += "Объект забронирован";
        if (cal_status == "closed") cal_title += "Объект недоступен";

        eventData = {
            id: Math.random(),
            title: cal_title,
            start: cal_startdate,
            end: cal_enddate,
            status: cal_status,
            type: "event",
            className: "event-" + cal_status
        };

        bookCal.fullCalendar('renderEvent', eventData, false); // stick? = true
        bookCal.fullCalendar('unselect');

        $('#modal-add-event').modal('hide');

        $("#fs_calendar").stop().css("background-color", "#FFFF9C").animate({
            backgroundColor: "#FFFFFF"
        }, 1000);


    });


    //-------------------------------------------------- Show First Tab (Geneal) & Calendar Tab -----------------------/
    $('#subnavbar-tabs input:first').tab('show');
    $('#subnavbar-tabs label:first').toggleClass('active');
    $('#calendar-view-tabs label:first').toggleClass('active');




    //-------------------------------------------------- Send Ajax Form -----------------------------------/
    $(".btn-apply").click(function(event) {
        var $form = $('form[data-async]');
        var $btn = $(this);

        $.ajax({
            context: $btn,
            dataType: 'json',
            cache: false,
            type: $form.attr('method'),
            url: '/ai/object/ajax',
            data: $form.serialize(),

            success: function(data, status) {
                if (data.error) {
                    //$(data.error.elements)
                    //.data("title", data.error.text)
                    //.tooltip({placement:'right', trigger: 'manual', show: '100'}).tooltip('show');

                    //$(data.error.elements).closest('.form-group').removeClass('has-success').addClass('has-error');
                }

                var form = data.form;

                //Update object-name in infobar
                $('#info_obj_name_ru').text(form.obj_name_ru);

                //Update address in infobar
                var addr1 = "",
                    addr2 = "";
                addr1 += (form.obj_addr_street) ? form.obj_addr_street + " " : "";
                addr1 += (form.obj_addr_number) ? form.obj_addr_number : "";
                $('#info_addr_1').text(addr1);

                addr2 += (form.obj_addr_entrance) ? "пд. " + form.obj_addr_entrance + " " : "";
                addr2 += (form.obj_addr_floor) ? "эт. " + form.obj_addr_floor + " " : "";
                addr2 += (form.obj_addr_room) ? "кв. " + form.obj_addr_room + " " : "";
                $('#info_addr_2').text(addr2);

                //Update dates in infobar
                $('#info_obj_update').text(form.obj_update);

                $(".navbar-infobar").stop().css("background-color", "#FFFF9C").animate({
                    backgroundColor: "#F7F7F9"
                }, 500);

                $("fieldset").stop().css("background-color", "#FFFF9C").animate({
                    backgroundColor: "#FFF"
                }, 500);

            },
            error: function(xhr, ajaxOptions, thrownError) {
                $btn.button('reset');
            }

        });


        event.preventDefault();
    });

    //------------- Get smartfilters By Object Type ID ------------------//
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/ai/object/ajax',
        data: {
            method: "getsmartsbyobjtype",
            objtypeid: $('#ot_object_type').val()
        },

        success: function(data, status) {
            if (data.error) {} else {

                //------------------- Update sf_smartfilters select2 -------------------
                var sf_smartfilters;

                if (data.objsmarts.length > 0) {
                    $.each(data.objsmarts, function(index, value) {
                        sf_smartfilters += '<option value="' + data.objsmarts[index].id + '">' + data.objsmarts[index].val + '</option>';
                    });
                    // $('#sf_smartfilters').html(sf_smartfilters);
                } else {
                    $('#sf_smartfilters').html("");
                }

            }
        },
        error: function(xhr, ajaxOptions, thrownError) {}
    });

    //Выставляем значения "все апартаменты" или "все квартиры" в зависимости от типа поля
    //Апартамент
    // if ($('#ot_object_type').val() == "9") $("#sf_smartfilters").select2("val", "11");
    //Квартира
    // if ($('#ot_object_type').val() == "2") $("#sf_smartfilters").select2("val", "20");


    //-------------------------------------------------- Fileupload -----------------------------------/

    $(function() {
        'use strict';

        $('#fileupload .fileupload-actions').hide();

        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
            url: '/ai/files/index/upload_ucid/' + $('#upload_ucid').val() + '/upload_id/' + $('#upload_id').val(),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        })
            .bind('fileuploadadd', function(e, data) {
                $('#fileupload .fileupload-actions').show({
                    duration: 500
                });
            });

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            'redirect',
            window.location.href.replace(
                /\/[^\/]*$/,
                '/cors/result.html?%s'
            )
        );

        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');

        $.ajax({
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function() {
            $(this).removeClass('fileupload-processing');
        }).done(function(result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {
                    result: result
                });
        });

    });




});


// ******************************************** Dec of Num ********************************
//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд']) 
function decOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}
