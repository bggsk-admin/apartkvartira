$(document).ready(function() {

    var main_form = $("#main-form");

    //** Hide infobar ********************************************************/
    $('.navbar-infobar').hide();
    $('body').addClass('no-infobar');

    /******************************************** Select2 *************************************/
    /*        $(".select2").select2({
                allowClear: true
        })
                .rules('add', 'required');
*/

    /******************************************** Cjeck default checkboxes *************************************/
/*    $('#subnavbar-tabs input[data-target="#tab-attributes"]').tab('show');
    $('#rst_restriction-1').attr('checked','checked');
    $('#rst_restriction-1').prettyCheckable('check');    
    alert($('#rst_restriction-1').attr('checked'));
*/


    /******************************************** 2GIS Map Autoloader *************************************/
    DG.autoload(function() {

        var myMap = new DG.Map('gismap');

        var lon = ($('#obj_addr_lon').val() !== "") ? parseFloat($('#obj_addr_lon').val()) : 0;
        var lat = ($('#obj_addr_lat').val() !== "") ? parseFloat($('#obj_addr_lat').val()) : 0;

        if (lon > 0 && lat > 0) {
            var geopoint = new DG.GeoPoint(lon, lat);

            myMap.setCenter(geopoint);

            var marker = new DG.Markers.Common({
                geoPoint: geopoint,
                clickCallback: function(clickEvent, marker) {
                    //alert('Привет :)');
                }
            });
            var markerHint = $('#obj_addr_street').val() + ' ' + $('#obj_addr_number').val();
            marker.setHintContent(markerHint);
            myMap.markers.add(marker);


        } else {
            myMap.setCenter(new DG.GeoPoint(38.9787146513723, 45.0504739491062));
        }

        myMap.disableScrollZoom();
        myMap.enableDragging();
        myMap.zoomTo(17);
        myMap.controls.add(new DG.Controls.Zoom());


        /******************************************** 2GIS - searchAddr *************************************/

        function searchAddr(address, map) {

            DG.Geocoder.get('краснодар ' + address, {

                types: ['city', 'settlement', 'district', 'street', 'living_area', 'place', 'house'],
                limit: 10,
                success: function(geocoderObjects) {

                    myMap.markers.removeAll();

                    var FirstObjGeoPoint = geocoderObjects[0].getCenterGeoPoint();
                    map.setCenter(new DG.GeoPoint(FirstObjGeoPoint.getLon(), FirstObjGeoPoint.getLat()), 17);

                    // Обходим циклом все полученные геообъекты
                    for (var i = 0, len = geocoderObjects.length; i < len; i++) {
                        var geocoderObject = geocoderObjects[i];

                        // Получаем маркер из геообъекта с помощью метода getMarker.
                        // Первый параметр - иконка маркера, второй параметр - функция, которая сработает при клике на маркер
                        var markerIcon = null; // иконка по умолчанию
                        var marker = geocoderObject.getMarker(
                            markerIcon,

                            //Действие при клике на маркер
                            (function(geocoderObject) {
                                return function() {
                                    var info = '';

                                    // Дополнительная информация о геообъекте
                                    var attributes = geocoderObject.getAttributes();

                                    $('#obj_addr_city').val(("city" in attributes && attributes['city'].length) ? attributes['city'] : '');
                                    $('#obj_addr_zip').val(("index" in attributes && attributes['index'].length) ? attributes['index'] : '');
                                    $('#obj_addr_district').val(("district" in attributes && attributes['district'].length) ? attributes['district'] : '');
                                    $('#obj_addr_microdistrict').val(("microdistrict" in attributes && attributes['microdistrict'].length) ? attributes['microdistrict'] : '');
                                    $('#obj_addr_street').val(("street" in attributes && attributes['street'].length) ? attributes['street'] : '');
                                    $('#obj_addr_number').val(("number" in attributes && attributes['number'].length) ? attributes['number'] : '');
                                    $('#obj_addr_room').val("0");
                                    $('#obj_addr_elevation').val(("elevation" in attributes && attributes['elevation'].length) ? attributes['elevation'] : '');
                                    $('#obj_addr_buildtype').val(("elevation" in attributes && attributes['purpose'].length) ? attributes['purpose'] : '');

                                    var centerGeoPoint = geocoderObject.getCenterGeoPoint();
                                    $('#obj_addr_lon').val(centerGeoPoint.getLon());
                                    $('#obj_addr_lat').val(centerGeoPoint.getLat());
                                    $('#obj_addr_lon_view').val(centerGeoPoint.getLon());
                                    $('#obj_addr_lat_view').val(centerGeoPoint.getLat());

                                    //Автозаполнение названия объекта
                                    $('#obj_name_ru').val(attributes['street'] + ' ' + attributes['number']).change();

                                    //Скроллим окно до адресного блока вверх
                                    $('html,body').animate({
                                        scrollTop: $("#main-form").offset().top - 200
                                    }, 'slow');

                                    //Подсвечиваем адресный блок
                                    $("#fs_coordinates, #fs_address").stop().css("background-color", "#FFFF9C").animate({
                                        backgroundColor: "#FFFFFF"
                                    }, 1000);

                                }
                            })(geocoderObject) //Создаем анонимную функцию и сразу же ее вызываем
                        );

                        //В хинт маркера помещаем адрес
                        var attributes = geocoderObject.getAttributes();
                        var markerHint = attributes['street'] + ' ' + attributes['number'];
                        marker.setHintContent(markerHint);

                        map.markers.add(marker);

                    } // for

                    $('#search_address').closest('.row').removeClass('has-error').addClass('has-success');
                }, // on success

                // Обработка ошибок
                failure: function(code, message) {
                    $('#search_address').closest('.row').removeClass('has-success').addClass('has-error');
                } // on failure

            }); //DG.Geocoder.get

        }
        /*********************************************************************************/


        //Срабатывание поиска при наборе запроса (с задержкой или нет)
        $("#search_address").bindWithDelay("keyup", function() {
            searchAddr($("#search_address").val(), myMap);
        }, 0);

        //Запрещаем клавишу "enter" когда фокус в поисковой строке карты
        $("#search_address").focus(function() {
            $(this).keypress(function(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
            });
        });


        //Скроллим окно до карты вниз, при клике на поисковую строку карты
        $("#search_address").click(function() {
            $('html,body').animate({
                scrollTop: $("#search_address").offset().top - 160
            }, 'slow');
        });

    }); //aotoload
    /*********************************************************************************/

    //Обновляем UID объекта по его названию
    $('#obj_name_ru').on('keyup change', function() {
        var uid = setTranslit($('#obj_name_ru').val());
        $('#obj_uid').val(uid);
    });

    //Обновляем UID объекта при заполнении квартиры
    $('#obj_addr_room').on('keyup change', function() {
        var uid = setTranslit($('#obj_name_ru').val()) + "-kv-" + $('#obj_addr_room').val();
        $('#obj_uid').val(uid);
    });

    //Обработка кода объекта - обновление кода типа объекта
    $("#sf_smartfilters").select2("enable", false);

    $('#ot_object_type').change(function() {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ai/object/ajax',
            data: {
                method: "getobjtypecode",
                objtypeid: $('#ot_object_type').val()
            },

            success: function(data, status) {
                if (data.error) {} else {
                    $('#obj_codename').val(objCodenameUpdate(data.objcode, 'objcode', $('#obj_codename').val()));
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {}
        });

        //------------- Get smartfilters By Object Type ID ------------------//
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ai/object/ajax',
            data: {
                method: "getsmartsbyobjtype",
                objtypeid: $('#ot_object_type').val()
            },

            success: function(data, status) {
                if (data.error) {} else {

                    //------------------- Update sf_smartfilters select2 -------------------
                    var sf_smartfilters;
                    $("#sf_smartfilters").select2("val", "");

                    if (data.objsmarts.length > 0) {
                        $.each(data.objsmarts, function(index, value) {
                            sf_smartfilters += '<option value="' + data.objsmarts[index].id + '">' + data.objsmarts[index].val + '</option>';
                        });
                        $('#sf_smartfilters').html(sf_smartfilters);
                        $("#sf_smartfilters").select2("enable", true);
                    } else {
                        $('#sf_smartfilters').html("");
                        $("#sf_smartfilters").select2("enable", false);
                    }

                    //В зависимости от типа выбранного жилья, выставляем дефолтные значения для полей:
                    // - смартфильтр
                    // - период оплаты
                    // - минимальный период проживания
                    
                    //_______________________________________Апартаменты_______________________________
                    if ($('#ot_object_type').val() == "9") 
                    {
                        $("#sf_smartfilters").select2("val", "11");
                        $("#obj_price_period").select2("val", "2");
                        $("#obj_price_minstay").val("2");
                    }

                    //_______________________________________Квартира_______________________________
                    if ($('#ot_object_type').val() == "2") 
                    {
                        $("#sf_smartfilters").select2("val", "20");
                        $("#obj_price_period").select2("val", "1");
                        $("#obj_price_minstay").val("6");
                    }

                }
            },
            error: function(xhr, ajaxOptions, thrownError) {}
        });

    });



    //-------------------------------------------------- Send Ajax Form -----------------------------------/
    $(".btn-apply").click(function(event) {
        var $form = $('form[data-async]');
        var $btn = $(this);

        if (main_form.valid()) {

            $.ajax({
                context: $btn,
                dataType: 'json',
                cache: false,
                type: $form.attr('method'),
                url: '/ai/object/ajax',
                data: $form.serialize(),

                success: function(data, status) {
                    //Update Infobar
                    $('#info_obj_name_ru').text(data.form.obj_name_ru);
                    $('#info_obj_codename').text(data.form.obj_codename);

                    //Update address in infobar
                    $('#info_addr_1').text(data.form.obj_addr_street + ' ' + data.form.obj_addr_number);

                    var addr2 = "";
                    addr2 += (data.form.obj_addr_entrance) ? "пд. " + data.form.obj_addr_entrance + " " : "";
                    addr2 += (data.form.obj_addr_floor) ? "эт. " + data.form.obj_addr_floor + " " : "";
                    addr2 += (data.form.obj_addr_room) ? "кв. " + data.form.obj_addr_room + " " : "";
                    $('#info_addr_2').text(addr2);

                    //Update dates in infobar
                    $('#info_obj_update').text(data.form.obj_update);
                    $('#info_obj_create').text(data.form.obj_create);

                    //Update ID
                    $('#obj_id').val(data.form.obj_id);

                    //Show infobar
                    $('.navbar-infobar').show();
                    $('body').removeClass('no-infobar');

                    $(".navbar-infobar").stop().css("background-color", "#FFFF9C").animate({
                        backgroundColor: "#F7F7F9"
                    }, 500);

                    $("fieldset").stop().css("background-color", "#FFFF9C").animate({
                        backgroundColor: "#FFF"
                    }, 500);


                },
                error: function(xhr, ajaxOptions, thrownError) {
                    new PNotify({
                        title: "Over Here",
                        text: thrownError,
                        type: "error",
                        // addclass: "stack-bar-top",
                        cornerclass: "",
                        width: "50%",
                        // stack: stack_bar_top
                    });

                    $btn.button('reset');
                }

            });
        }


        event.preventDefault();
    });


    function coordToCode(lon, lat) {
        lon = lon.replace(/\./g, '');
        lon = lon.substring(0, 6);

        lat = lat.replace(/\./g, '');
        lat = lat.substring(0, 6);

        return lon + '' + lat;

    }


    function objCodenameUpdate(data, data_name, form_val) {
        var arr = form_val.split('-');

        if (data_name == "objcode") {
            data = (data.length) ? data : 'XXX';
            return data + '-' + arr[1] + '-' + arr[2];
        }

    }


    //-------------------------------------------------- jQuery Validate -----------------------------------/

    // jQuery.validator.addMethod("lettersonly", function(value, element) {
    //         return this.optional(element) || /^[a-z0-9_-]+$/i.test(value);
    // }, "Please use only a-z0-9_-");

    if ($.fn.validate) {
        var validobj = main_form.validate({
            rules: {
                "ot_object_type[]": {
                    required: true,
                },
                obj_name_ru: {
                    required: true,
                }
            },
            messages: {
                "ot_object_type[]": {
                    required: "Укажите тип объекта",
                },
                obj_name_ru: {
                    required: "Укажите название объекта",
                }
            },
            showErrors: function(errorMap, errorList) {

                $.each(this.validElements(), function(index, element) {
                    var $element = $(element);
                    $element.data("title", "").tooltip("destroy");
                    $element.closest('.form-group').removeClass('has-error').addClass('has-success');
                });

                $.each(errorList, function(index, error) {
                    var $element = $(error.element);

                    //Show errors on Select2 fields
                    if ($element.hasClass("select2-offscreen")) {
                        $("#s2id_" + $element.attr("id") + " ul").addClass("has-error");

                        $("#s2id_" + $element.attr("id") + " ul").tooltip("destroy")
                            .data("title", error.message)
                            .tooltip({
                                placement: 'top',
                                trigger: 'manual',
                                show: '100'
                            }).tooltip('show');


                        //Show errors on other regular fields
                    } else {

                        $element.tooltip("destroy")
                            .data("title", error.message)
                            .tooltip({
                                placement: 'top',
                                trigger: 'manual',
                                show: '100'
                            }).tooltip('show');

                        $element.closest('.form-group').removeClass('has-success').addClass('has-error');
                    }

                });

            },
        });

    } else {
        alert("Validate method not loaded");
    }

    //Select2 Error processing
    //************************************************************
    $(document).on("change", ".select2-offscreen", function() {
        if (!$.isEmptyObject(validobj.submitted)) {
            $("#s2id_" + $(this).attr("id") + " ul").removeClass("has-error");
            validobj.form();
        }
    });

    $(document).on("select2-opening", function(arg) {
        $("#s2id_" + $(arg.target).attr("id") + " ul").toggleClass("has-error");
    });

    //-------------------------------------------------- Fileupload -----------------------------------/
    $(function() {
        'use strict';

        $('#fileupload .fileupload-actions').hide();

        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
            url: '/ai/files/index/upload_ucid/' + $('#upload_ucid').val() + '/upload_id/' + $('#upload_id').val(),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        })
            .bind('fileuploadadd', function(e, data) {
                $('#fileupload .fileupload-actions').show({
                    duration: 500
                });
            });

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            'redirect',
            window.location.href.replace(
                /\/[^\/]*$/,
                '/cors/result.html?%s'
            )
        );

        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');

        $.ajax({
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function() {
            $(this).removeClass('fileupload-processing');
        }).done(function(result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {
                    result: result
                });
        });

    });

});
