/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};




$(document).ready(function() {

    //-------------------------------------------------- Send Ajax Form -----------------------------------/
    $(".btn-rebuild-action").click(function(event) {

        var $btn = $(this);
        $btn.button('loading');

        $.ajax({
            context: $btn,
            dataType: 'json',
            cache: false,
            type: "POST",
            url: '/ai/object/ajax',
            data: {method: "rebuild"},

            success: function(data, status) {
                
                // console.log(data);

                $(".navbar-subnavbar, .navbar-breadcrumb").stop().css("background-color", "#FFFF9C").animate({
                    backgroundColor: "#F7F7F9"
                }, 500);

                $btn.button('reset');
            },
            
            error: function(xhr, ajaxOptions, thrownError) {
                new PNotify({
                    title: "Over Here",
                    text: thrownError,
                    type: "error",
                    // addclass: "stack-bar-top",
                    cornerclass: "",
                    width: "50%",
                    // stack: stack_bar_top
                });
                $btn.button('reset');
            }

        });

        event.preventDefault();
    });

    var tableHeight = $(window).height() - 350;

    var datatable = $('#watable').dataTable({

        //"sDom": "<'row'<'col-xs-4'l><'col-xs-4'f><'col-xs-4'p>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
        "sDom": "<'row'<'col-xs-4'i><'col-xs-4'f><'col-xs-4'p>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
        "oLanguage": {
            "sLengthMenu": "_MENU_ записей на страницу "
        },
        "order": [
            [11, "DESC"] //Default Order by obj_update
        ],
        // "bAutoWidth": false,
        "aoColumnDefs": [
            {
                "aTargets": [0, 1, 2, 3],
                "bSortable": false
            }, 
            {
                "aTargets": 11,
                "bVisible": false
            }, 
            {
                "aTargets": 12,
                "iDataSort": 11,
            }
        ],
        "oLanguage": {
            "sUrl": "/js/dataTables.russian.txt"
        },
        "iDisplayLength": 50,
        
        "scrollY": tableHeight + "px", 
        "scrollCollapse": true,        
    
    });

/*    new $.fn.dataTable.FixedHeader( datatable, {
        "offsetTop": 111,
         "top": true,
         "zTop": 11
    });
*/

});
