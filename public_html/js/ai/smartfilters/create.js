$(document).ready(function() {

    var main_form = $("#main-form");

    //** Hide infobar ********************************************************/
    $('.navbar-infobar').hide();
    $('body').addClass('no-infobar');


    //Обновляем UID объекта по его названию
    // $('#sf_name_ru').on('keyup change', function() {
    //     var uid = setTranslit($('#sf_name_ru').val());
    //     $('#sf_uid').val(uid);
    // });

    //-------------------------------------------------- Send Ajax Form -----------------------------------/
    $(".btn-apply").click(function(event) {
        var $form = $('form[data-async]');
        var $btn = $(this);

        if (main_form.valid()) {

            $.ajax({
                context: $btn,
                dataType: 'json',
                cache: false,
                type: $form.attr('method'),
                url: '/ai/smartfilters/ajax',
                data: $form.serialize(),

                success: function(data, status) {
                    //Update Infobar
                    $('#info_sf_name_ru').text(data.form.sf_name_ru);

                    //Update dates in infobar
                    $('#info_sf_update').text(data.form.sf_update);
                    $('#info_sf_create').text(data.form.sf_create);

                    //Update ID
                    $('#sf_id').val(data.form.sf_id);

                    //Show infobar
                    $('.navbar-infobar').slideDown();
                    $('body').removeClass('no-infobar');

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    new PNotify({
                        title: "Over Here",
                        text: thrownError,
                        type: "error",
                        // addclass: "stack-bar-top",
                        cornerclass: "",
                        width: "50%",
                        // stack: stack_bar_top
                    });

                    $btn.button('reset');
                }

            });
        }

        event.preventDefault();
    });

    //-------------------------------------------------- jQuery Validate -----------------------------------/
/*    if ($.fn.validate) {
        var validobj = main_form.validate({
            rules: {
                "ot_object_type[]": {
                    required: true,
                },
                sf_name_ru: {
                    required: true,
                }
            },
            messages: {
                "ot_object_type[]": {
                    required: "Укажите тип объекта",
                },
                sf_name_ru: {
                    required: "Укажите название объекта",
                }
            },
            showErrors: function(errorMap, errorList) {

                $.each(this.validElements(), function(index, element) {
                    var $element = $(element);
                    $element.data("title", "").tooltip("destroy");
                    $element.closest('.form-group').removeClass('has-error').addClass('has-success');
                });

                $.each(errorList, function(index, error) {
                    var $element = $(error.element);

                    //Show errors on Select2 fields
                    if ($element.hasClass("select2-offscreen")) {
                        $("#s2id_" + $element.attr("id") + " ul").addClass("has-error");

                        $("#s2id_" + $element.attr("id") + " ul").tooltip("destroy")
                            .data("title", error.message)
                            .tooltip({
                                placement: 'top',
                                trigger: 'manual',
                                show: '100'
                            }).tooltip('show');


                        //Show errors on other regular fields
                    } else {

                        $element.tooltip("destroy")
                            .data("title", error.message)
                            .tooltip({
                                placement: 'top',
                                trigger: 'manual',
                                show: '100'
                            }).tooltip('show');

                        $element.closest('.form-group').removeClass('has-success').addClass('has-error');
                    }

                });

            },
        });

    } else {
        alert("Validate method not loaded");
    }

    //Select2 Error processing
    //************************************************************
    $(document).on("change", ".select2-offscreen", function() {
        if (!$.isEmptyObject(validobj.submitted)) {
            $("#s2id_" + $(this).attr("id") + " ul").removeClass("has-error");
            validobj.form();
        }
    });

    $(document).on("select2-opening", function(arg) {
        $("#s2id_" + $(arg.target).attr("id") + " ul").toggleClass("has-error");
    });
*/

    //-------------------------------------------------- Fileupload -----------------------------------/
    $(function() {
        'use strict';

        $('#fileupload .fileupload-actions').hide();

        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
            url: '/ai/files/index/upload_ucid/' + $('#upload_ucid').val() + '/upload_id/' + $('#upload_id').val(),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        })
            .bind('fileuploadadd', function(e, data) {
                $('#fileupload .fileupload-actions').show({
                    duration: 500
                });
            });

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            'redirect',
            window.location.href.replace(
                /\/[^\/]*$/,
                '/cors/result.html?%s'
            )
        );

        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');

        $.ajax({
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function() {
            $(this).removeClass('fileupload-processing');
        }).done(function(result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {
                    result: result
                });
        });

    });

});
