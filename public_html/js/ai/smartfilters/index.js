$(document).ready(function() {

    //-------------------------------------------------- Send Ajax Form -----------------------------------/
    $(".btn-rebuild-action").click(function(event) {

        var $btn = $(this);
        $btn.button('loading');

        $.ajax({
            context: $btn,
            dataType: 'json',
            cache: false,
            type: "POST",
            url: '/ai/smartfilters/ajax',
            data: {method: "rebuild"},

            success: function(data, status) {
                
                $(".navbar-infobar").stop().css("background-color", "#FFFF9C").animate({
                    backgroundColor: "#F7F7F9"
                }, 500);

                $btn.button('reset');
            },
            
            error: function(xhr, ajaxOptions, thrownError) {
                new PNotify({
                    title: "Over Here",
                    text: thrownError,
                    type: "error",
                    // addclass: "stack-bar-top",
                    cornerclass: "",
                    width: "50%",
                    // stack: stack_bar_top
                });
                $btn.button('reset');
            }

        });

        event.preventDefault();
    });



});
