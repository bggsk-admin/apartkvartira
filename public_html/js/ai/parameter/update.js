$(document).ready(function(){

    initSheepIt();

    if($('#prm_type').val() === 'list')
    {
        $('#parameter-values').removeClass('hidden').addClass('show');
    }

    $('#prm_type').on('change', function(){

        if($(this).val() === 'list')
        {
            $('#parameter-values').fadeIn('slow').removeClass('hidden').addClass('show');

        } else {
            $('#parameter-values').fadeIn('slow').removeClass('show').addClass('hidden');
        }

    });


    function initSheepIt()
    {
        var sheepItForm = $('#clone').sheepIt({
            separator: '',
            allowRemoveLast: true,
            allowRemoveCurrent: true,
            allowRemoveAll: true,
            allowAdd: true,
            allowAddN: true,
            maxFormsCount: 20,
            minFormsCount: 0,
            iniFormsCount: 1,
            insertNewForms: 'after',
            afterAdd: function(source) {

                $.post( "/ai/parameter/ajax", { action: "getvals", prm_id: $("#prm_id").val() })
                .done(function( data ) {
                    //alert( "Data Loaded: " + data.result );
                    source.inject(data.result);

                });
            },
        });
    }

});