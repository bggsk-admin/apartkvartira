/***************************************************************************
jQuery START
****************************************************************************/
$(document).ready(function() {

    var item;

    //   Image Slider
    //*******************************************************************************
    if ($.fn.bxSlider) {
        var slider = $('.bxslider').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            slideMargin: 0,
            infiniteLoop: false,
            hideControlOnEnd: true,
            pager: true,
            auto: false,
            autoControls: false,
            pagerCustom: '.bx-pager',
            preloadImages: 'all',
        });
    } else {
        alert("bxSlider not installed");
    }

    //   Booking - Set Guests
    //*******************************************************************************


    //   Object Loction on Map
    //*******************************************************************************
    gmap = new GMaps({
        div: '#obj-view-map-canvas',
        fitZoom: true,
        lat: 45.037754,
        lng: 39.007491,
        click: function(e) {
            hideOverlays();
        },
        scrollwheel: false,
    });

    //Processing Data for Ajax
    var params = parseUrl();
    var ajax_data = {
        action: "getobjects",
        f_uid: params['uid'],
        clearsess: false
    };

    jQuery.ajax({
        url: '/objects/ajax',
        dataType: 'json',
        data: ajax_data,
        success: function(data) {

            var item = data.locations[0];

            // console.log(item);

            // Add Marker
            //*****************************************************************************//
            var iconColor;
            if (item.details.ot_code == "APT") iconColor = '#c80021'; //Апартаменты
            if (item.details.ot_code == "FLA") iconColor = '#165988'; //Квартиры

            var svgIcon = {
                path: 'M75.992,29.955C75.992,15.074,63.908,3,48.996,3C34.081,3,22,15.074,22,29.955   c0,5.57,1.689,10.74,4.582,15.039l22.804,53.191l22.018-53.191C73.742,40.746,75.992,35.525,75.992,29.955z M48.712,47.646   c-10.227,0-18.522-8.487-18.522-18.957c0-10.463,8.296-18.95,18.522-18.95c10.23,0,18.526,8.487,18.526,18.95   C67.238,39.16,58.942,47.646,48.712,47.646z',
                fillColor: iconColor,
                fillOpacity: 1,
                anchor: new google.maps.Point(48, 96),
                scale: 0.3,
                strokeWeight: 0
            };

            gmap.addMarker({
                lat: item.lat,
                lng: item.lng,
                title: item.name,
                icon: svgIcon,
                zIndex: 1,
            });

            gmap.setCenter(item.lat, item.lng);
            gmap.setZoom(17);

        }
    });

    //  Hide Billing Info
    //*******************************************************************************
    $(".obj-view-bill").hide();

    //  Claendar
    //*******************************************************************************
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth();
    var yyyy = today.getFullYear();

    $.datepicker.regional['ru-RU'] = {
        closeText: 'Готово',
        currentText: 'Сегодня',
        dateFormat: 'd M yy',
        dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayNamesShort: ['Вос', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб'],
        firstDay: 1,
        isRTL: false,
        minDate: new Date(yyyy, mm, dd),
        monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
        monthNamesShort: ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
        nextText: '&nbsp;',
        prevText: '&nbsp;',
        showMonthAfterYear: false,
        weekHeader: 'Wk',
        yearSuffix: '',
        numberOfMonths: 2,


        beforeShow: function() {
            $("#ui-datepicker-div").addClass("ui-calendar-default");
        },

        beforeShowDay: function(date) {

            // var unavailableDates = ["2014-10-17", "2014-10-18", "2014-10-19", "2014-10-20" ];
            var unavailableDates = [];
            // var checkinDates = ["2014-9-14"];
            // var checkoutDates = ["2014-9-19"];

            var
                dateFrom = $("#b_arrdate").datepicker("getDate"),
                dateTo = $("#b_depdate").datepicker("getDate");

            if (date >= dateFrom && date <= dateTo) {
                return [true, 'range_day', ''];
            }

            var dateAsString = date.getFullYear().toString() + "-" + (date.getMonth() + 1).toString() + "-" + date.getDate().toString();

            // console.log(dateAsString + ' ' + $.inArray(dateAsString, unavailableDates));

            // return [$.inArray(dateAsString, unavailableDates) == -1];

            if ($.inArray(dateAsString, unavailableDates) >= 0) {
                return [false, "ui-state-booked", ""];
            } else {
                return [true, '', ''];
            }

            /*            if( $.inArray(dateAsString, checkinDates) > 0 )
            {
                // return [false, "ui-state-checkin-day", ""];
                return [false, "ui-state-booked", ""];
            }

            if( $.inArray(dateAsString, checkoutDates) > 0 )
            {
                return [false, "ui-state-checkout-day", ""];
            }

*/


        },

        onSelect: function() {

            var a = $(this).attr("id");
            var dateFrom = $("#b_arrdate").datepicker("getDate");
            var dateTo = $("#b_depdate").datepicker("getDate");

            if (a == "b_arrdate" && !dateTo) {

                var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() + 2);

                console.log(newDate);
                $("#b_depdate").datepicker("setDate", newDate);
            }

            // console.log(dateFrom + ' ' + dateTo);

            if (a == "b_depdate") {
                calcBooking(dateFrom, dateTo);
            }

        },
    };

    $.datepicker.setDefaults($.datepicker.regional['ru-RU']);

    $("#b_arrdate").datepicker({

        onClose: function(selectedDate) {

            var dateFrom = $("#b_arrdate").datepicker("getDate");
            var dateTo = $("#b_depdate").datepicker("getDate");
            var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() + 2);

            $("#b_depdate").datepicker("option", "minDate", newDate);
            $("#b_depdate").datepicker("show");
            $("#b_depdate").datepicker("setDate", newDate);

        },

    });

    $("#b_depdate").datepicker({

        onClose: function(selectedDate) {

            var dateFrom = $("#b_arrdate").datepicker("getDate");
            var dateTo = $("#b_depdate").datepicker("getDate");
            var newDate = new Date(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate() - 2);

            if (!dateFrom) {
                $("#b_arrdate").datepicker("setDate", newDate);
                $("#b_arrdate").datepicker("show");
                // $("#b_arrdate").toggleClass("active");

            }

            $("#b_depdate").removeAttr("disabled");

/*            if (dateTo) {
                $("#book-now").removeAttr("disabled");
            }
*/

            //$("#search_date_from").datepicker("option", "maxDate", selectedDate);
        },

    });

    //  Calendar - END
    //*******************************************************************************

    //  Highlight clendar inputs on ficus
    //*******************************************************************************
    $('.input-range').focus(function() {
        $(this).addClass("active");
    }).focusout(function() {
        $(this).removeClass("active");
    });

    //   Validate Calendar 
    //*******************************************************************************
/*    $("#obj-view-booking").bootstrapValidator({
            fields: {
            b_arrdate: {
                trigger: "focus blur keyup",
                validators: {
                    notEmpty: {
                        message: 'Укажите дату заезда'
                    },
                }
            },
            b_depdate: {
                trigger: "focus blur keyup",
                validators: {
                    notEmpty: {
                        message: 'Укажите дату выезда'
                    },
                }
            },
        },
        submitButtons: '#book-now',
        live: 'enabled',
    });
*/




    //  Update Booking by Session (if exist)
    //*******************************************************************************
    updateBookingBySess();

    //  Inner links scroll
    //*******************************************************************************
    $(".inner-link").click(function(e) {
        e.preventDefault();

        $('body').scrollTo($(this).attr('href'), 500, {
            // easing: 'elasout'
            offset: -110
        });

        $("#object-info-navbar a").removeClass('active');
        $(this).toggleClass('active');

    });

});
//========================= JQuery END ====================

//   Calc Booking
//*******************************************************************************
function calcBooking(dateFrom, dateTo) {
    var dateFromMoment = moment(dateFrom);
    var dateToMoment = moment(dateTo);

    // console.log(dateFrom);

    if (!_.isUndefined(dateFrom) && !_.isUndefined(dateTo) && !_.isNaN(dateFrom) && !_.isNaN(dateTo) && dateFrom !== "" && dateTo !== "") {
        $("#b_arrdate").datepicker("setDate", new Date(dateFrom));
        $("#b_depdate").datepicker("setDate", new Date(dateTo));

        var diffDays = dateToMoment.diff(dateFromMoment, 'days');
        var price = parseInt($("input[name=obj_price]").val());
        var total = parseInt(diffDays * price);

        var bookfee_pct = $("input[name=ot_bookfee_pct]").val();
        var bookfee_fix = $("input[name=ot_bookfee_fix]").val();

        var total_now = parseInt((total / 100) * bookfee_pct);
        if (total_now > bookfee_fix) total_now = bookfee_fix;

        var total_checkin = total - total_now;

        $("#b-period").text(diffDays);
        $("input[name=b_period]").val(diffDays);

        $("#b-total").text(total);
        $("input[name=b_total]").val(total);

        $("#b-total-now").text(total_now);
        $("input[name=b_total_now]").val(total_now);

        $("#b-total-checkin").text(total_checkin);
        $("input[name=b_total_checkin]").val(total_checkin);

        // console.log(moment(dateFromMoment).format('YYYY-MM-DD'));

        $("input[name=b_arrdate_ymd]").val(moment(dateFromMoment).format('YYYY-MM-DD'));
        $("input[name=b_depdate_ymd]").val(moment(dateToMoment).format('YYYY-MM-DD'));

        $(".b-total-summ").text(total);

        var total_days = decOfNum(diffDays, ['день', 'дня', 'дней']);
        $("#b-total-days").text(total_days);
        $("input[name=b_total_days]").val(total_days);

        $(".obj-view-bill").slideUp("fast");
        $(".obj-view-bill").slideDown("fast");

        $("#book-now").removeAttr("disabled");

        $('.input-range').blur();
        // $(".input-range").removeClass("active");        
    }

}

//   Update Booking by Session
//*******************************************************************************
function updateBookingBySess() {

    var dateFrom = $("input[name=b_arrdate_ymd]").val();
    var dateTo = $("input[name=b_depdate_ymd]").val();

    // console.log(dateFrom);

    if (!_.isUndefined(dateFrom) && !_.isUndefined(dateTo) && !_.isNull(dateFrom) && !_.isNull(dateTo) && !_.isEmpty(dateFrom) && !_.isEmpty(dateTo)) {
        calcBooking(dateFrom, dateTo);
    } else {

        // $("#b_depdate").attr("disabled", "disabled");
        // $("#book-now").attr("disabled", "disabled");
    }

}

//   Parse URL
//*******************************************************************************
function parseUrl() {

    var url = document.location.href;

    var parser = document.createElement('a');
    parser.href = url;
    var pathname = parser.pathname;

    var params = pathname.split("/");
    var pairs = [];

    for (var i = 0; i < params.length; i++) {
        if (i % 2 !== 0) pairs[params[i]] = params[i + 1];
    }

    return pairs;
}

//   Dec of Num
//*******************************************************************************
//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд']) 
function decOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

function showPanoramaBox(index) {

    var lat = item[0]['details']['lat_strview'];
    var lng = item[0]['details']['lng_strview'];

    var box = "#panorama-box";

    $(box).show();

    panorama = GMaps.createPanorama({
        el: box,
        lat: lat,
        lng: lng,
    });

}
