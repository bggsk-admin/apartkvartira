/***************************************************************************
jQuery
****************************************************************************/
var gmap_locs;
var gmap;
var url_data;

$(document).ready(function() {


    //   Easing "Elasout" effect
    //*******************************************************************************
    $.easing.elasout = function(x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    };

    //   Typehead
    //*******************************************************************************
    $("#f_street").typeahead(
    {
          hint: true,
          highlight: true,
          minLength: 1
        },
        {
        // name: 'states',
        displayKey: 'name',    
        source: function (query, process) {
            return $.post(
               '/objects/ajax', 
               { action: "getstreets", query: query },
               function (data) {
                    return process(data.items);
            }, 'json');
        },

        templates: {
            empty: [
                '<div class="tty-empty-message">',
                'Ни одной улицы не найдено',
                '</div>'
            ].join('\n'),
            suggestion: Handlebars.compile('<p>{{type_short}}. <strong>{{name}}</strong></p>')
        },

    });

    //   Select Dropdown - Smartfilter
    //*******************************************************************************
    var smarts = $.getSmartsForSelect();
    
    $("#f_smart").heapbox({
        'effect': {
            'type': 'slide',
            'speed': 'fast'
        },
         "onChange": function(value){
            updatePricesBySmartID(value);
        },
    });

    var json = JSON.stringify(smarts.items);

    $("#f_smart").heapbox("set", json);

    //   Hide Em All
    //*******************************************************************************
    hidePanoramaBoxes();
    hideFilters();

    //   noUiSlider - Price slider
    //*******************************************************************************
    $(".f-price-slider").noUiSlider({
        start: [1500, 2500],
        step: 5,
        connect: true,
        range: {'min': [0],'max': [30000]},
        serialization: {
            lower: [
                $.Link({ target: $('#f-price-min'), method: "text" }),
            ],
            upper: [
                $.Link({
                    target: $('#f-price-max'),
                    method: "text"
                }),
            ],
            format: {
                decimals: 0,
                mark: ','
            }
        }
    });

    //  noUiSlider  - Guests slider
    //********************************
    $(".f-guests-slider").noUiSlider({
        start: [1],
        step: 1,
        connect: 'lower',
        range: {
            'min': [1],
            'max': [10]
        },
        serialization: {
            lower: [
                $.Link({
                    target: $('#f-guests-min'),
                    method: "text"
                }),
            ],
            format: {
                decimals: 0,
                mark: ','
            }
        }

    });


    //   Get Filters Params from URL
    //*******************************************************************************
    var ajax_data = updateFiletrsByUrl();

    //   GMap: Resize & Init actions
    //*******************************************************************************
    google.maps.event.addDomListener(window, 'load', gmapInit(ajax_data));
    google.maps.event.addDomListener(window, 'resize', function() {

        var height = $(window).height() - 59 - 50;
        var width = $("#objects-map").width() - 15 /* padding*/ ;

        $("#panorama-box").css("height", height).css("width", width).css("top", "0px").css("left", "15px");

        $("#objects-list, #objects-map").css("height", height);
        $("#map-canvas").css("width", width);
        $("#map-canvas").css("height", height);

        gmap.refresh;

    });





    //   ADD To Favorites
    //*******************************************************************************
    $(document).on('click', '.add-favorites', function(event) {
        event.preventDefault();

        var index = $(this).attr('data-obj-index');

/*        highlightObjectItem(index);
        showOverlay(index);
        showPanoramaBox(index);
*/

    });

    //   OPEN Object Map Bubble action
    //*******************************************************************************
    $(document).on('click', '.obj-title', function(event) {
        event.preventDefault();

        var index = $(this).attr('data-obj-index');

        highlightObjectItem(index);
        showOverlay(index);
        hidePanoramaBoxes();

    });

    //   CLOSE Object Map Bubble action
    //*******************************************************************************
    $(document).on('click', '.overlay-box-close', function(event) {
        event.preventDefault();
        hideOverlays();
    });


    //   OPEN StreetView Panorama action
    //*******************************************************************************
    $(document).on('click', '.open-panorama', function(event) {
        event.preventDefault();

        var index = $(this).attr('data-obj-index');

        highlightObjectItem(index);
        showOverlay(index);
        showPanoramaBox(index);

    });

    //   CLOSE StreetView Panorama action
    //*******************************************************************************
    $(document).on('click', '.close-panorama', function(event) {
        event.preventDefault();
        hidePanoramaBoxes();
    });

    //   APPLY Filters action
    //*******************************************************************************
    $(document).on('click', '#apply-filters', function(event) {
        event.preventDefault();

        var ajax_data = updateFiletrsByForm();

        //Reload Map & Objects
        gmapInit(ajax_data);
        closeFilters();
    });



    //   OPEN Filters Panel
    //*******************************************************************************
    $(".open-filters").click(function(e) {
        e.preventDefault();
        openFilters();
    });

    //   CLOSE Filters Panel
    //*******************************************************************************
    $(".close-filters").click(function(e) {
        e.preventDefault();
        closeFilters();
    });



    //  Claendar
    //*******************************************************************************
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth();
    var yyyy = today.getFullYear();

    $.datepicker.regional['ru-RU'] = {
        closeText: 'Готово',
        currentText: 'Сегодня',
        dateFormat: 'd M yy',
        dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayNamesShort: ['Вос', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб'],
        firstDay: 1,
        isRTL: false,
        minDate: new Date(yyyy, mm, dd),
        monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
        monthNamesShort: ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
        nextText: '&nbsp;',
        prevText: '&nbsp;',
        showMonthAfterYear: false,
        weekHeader: 'Wk',
        yearSuffix: '',

        beforeShow: function() {
            $("#ui-datepicker-div").addClass("ui-calendar-default")
        },

        beforeShowDay: function(a) {
            var b = "";

            try {
                var dateFrom = $("#f_arrdate").datepicker("getDate"),
                    dateTo = $("#f_depdate").datepicker("getDate");

                return a >= dateFrom && dateTo >= a && (b = "range_day"), [!0, b]

            } catch (e) {
                return [!0, b]
            }
        },

        onSelect: function() {

            var a = $(this).attr("id");
            var dateFrom = $("#f_arrdate").datepicker("getDate");
            var dateTo = $("#f_depdate").datepicker("getDate");

            if (a == "f_arrdate" && !dateTo) {

                var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() + 2);
                $("#f_depdate").datepicker("setDate", newDate);
            }


        },
    };

    $.datepicker.setDefaults($.datepicker.regional['ru-RU']);

    $("#f_arrdate").datepicker({
        changeMonth: true,
        numberOfMonths: 2,

        onClose: function(selectedDate) {

            var dateFrom = $("#f_arrdate").datepicker("getDate");
            var dateTo = $("#f_depdate").datepicker("getDate");
            var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() + 2);

            $("#f_depdate").datepicker("option", "minDate", newDate);
            $("#f_depdate").datepicker("show");
        },

    });

    $("#f_depdate").datepicker({
        changeMonth: true,
        numberOfMonths: 2,

        onClose: function(selectedDate) {

            var dateFrom = $("#f_arrdate").datepicker("getDate");
            var dateTo = $("#f_depdate").datepicker("getDate");
            var newDate = new Date(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate() - 2);

            if (!dateFrom) {
                $("#f_arrdate").datepicker("setDate", newDate);
                $("#f_arrdate").datepicker("show");

            }

            //$("#search_date_from").datepicker("option", "maxDate", selectedDate);
        },

    });


}); //========================= JQuery END ==============================================


// ******************************************** Dec of Num ********************************
//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд']) 
function decOfNum(number, titles)  
{  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}  

jQuery.extend
(
    {
        getSmarts: function() 
        {
            var result = null;

            $.ajax(
            {
                url: '/objects/ajax/',
                type: 'get',
                data: {action: "getsmarts"},
                dataType: 'json',
                async: false,
                cache: false,
                success: function(data) 
                {
                    result = data;
                }
            });            

            // console.log(result);
            return result;
    
        },
        getSmartsForSelect: function() 
        {
            var result = null;

            $.ajax(
            {
                url: '/objects/ajax/',
                type: 'get',
                data: {action: "getsmartsselect"},
                dataType: 'json',
                async: false,
                cache: false,
                success: function(data) 
                {
                    result = data;
                }
            });            

            // console.log(result);
            return result;
    
        }
    }
);

// ******************************************** Filters Panel functions ********************************
function updateFiletrsByUrl() {

    var params = parseUrl();
    var smarts = $.getSmarts();

    //Get Parameters
    var f_minprice = parseInt(params["f_minprice"]) || 0;
    var f_maxprice = parseInt(params["f_maxprice"]) || 0;
    var f_arrdate = params["f_arrdate"];
    var f_depdate = params["f_depdate"];
    var f_guests = params["f_guests"];
    var f_street = decodeURIComponent(params["f_street"]);
    var f_smart = params["f_smart"];
    var f_smart_name = smarts.items[f_smart]["sf_name_ru"];

    if(f_street !== "undefined" && f_street !== null) $("#f_street").val(f_street);

    f_minprice = (f_minprice > 0) ? f_minprice : parseInt(smarts.items[f_smart]["sf_minprice"]);
    f_maxprice = (f_maxprice > 0) ? f_maxprice : parseInt(smarts.items[f_smart]["sf_maxprice"]);

    //Если крайние цены равны (например когда в смарте один объект) то разнозим диапазон вручную
    if(f_minprice == f_maxprice)
    {
        f_minprice -= 500;
        f_maxprice += 500;
    }

    //Update Filters Panel - Dates
    if(f_arrdate === undefined && f_depdate === undefined && f_arrdate !== null && f_depdate !== null)
    {
        $("#filters-panel-dates .value-arr").text("Дата не указана");
        $("#filters-panel-dates").addClass("btn-warn");
    } 
    else 
    {
        $("#filters-panel-dates").removeClass("btn-warn");

        $("#filters-panel-dates .value-arr").text(moment(f_arrdate, "YYYY-MM-DD").format('D MMMM '));
        $("#filters-panel-dates .value-dep").text(moment(f_depdate, "YYYY-MM-DD").format('D MMMM'));
        $("#f_arrdate").val(moment(f_arrdate, "YYYY-MM-DD").format('D MMMM YYYY'));
        $("#f_depdate").val(moment(f_depdate, "YYYY-MM-DD").format('D MMMM YYYY'));
    }

    //Update Filters Panel - Prices
    $("#filters-panel-price .value-min, #f-price-min-label").text(f_minprice);
    $("#filters-panel-price .value-max, #f-price-max-label").text(f_maxprice);
    $("#f-price-mid-label").text(((f_maxprice - f_minprice)/2 + f_minprice));

    //Update Filters Panel - Guests
    $("#filters-panel-guests .value").text(f_guests);
    
    //Update Filters Panel - Smartfilters
    $("#f_smart").heapbox("select", f_smart);
    $("#filters-panel-smart .value").text(f_smart_name);

    //Update Sliders - Prices
    $(".f-price-slider").noUiSlider({
        start: [f_minprice, f_maxprice],
        step: 50,
        range: {
            'min': f_minprice,
            'max': f_maxprice
        },
    }, true);

    //Update Sliders - Guests
    $(".f-guests-slider").val([f_guests]);

    return params;
}

function updateFiletrsByForm() {

    //history.pushState("http://apt/");

    // var stateObj = { foo: "bar" };
    // history.pushState(stateObj, "page 2", "/objects/index/");

    // console.log(h);

        var smarts = $.getSmarts();

        var f_minprice =  $(".f-price-slider").val()[0];
        var f_maxprice =  $(".f-price-slider").val()[1];
        var f_guests =  $(".f-guests-slider").val();
        var f_street =  $("#f_street").val();
        var f_smart =  $("#f_smart").val();
        var f_smart_name = smarts.items[f_smart]["sf_name_ru"];
        var f_arrdate = $("#f_arrdate").datepicker("getDate");
        var f_depdate = $("#f_depdate").datepicker("getDate");


        if(f_street === "undefined" || f_street === null) f_street = "";

        //Update Filters Panel
        $("#filters-panel-price .value-min").text(f_minprice);
        $("#filters-panel-price .value-max").text(f_maxprice);
        $("#filters-panel-guests .value").text(f_guests);
        $("#filters-panel-smart .value").text(f_smart_name);

        //Update Filters Panel - Dates
        if( (f_arrdate === undefined && f_depdate === undefined) || (f_arrdate === null && f_depdate === null))
        {
            $("#filters-panel-dates").addClass("btn-warn");
            $("#filters-panel-dates .value-arr").text("Дата не указана");
        }
        else
        {
            $("#filters-panel-dates").removeClass("btn-warn");

            //Update Filters Panel - Dates
            $("#filters-panel-dates .value-arr").text(moment(f_arrdate).format('D MMMM '));
            $("#filters-panel-dates .value-dep").text(moment(f_depdate).format('D MMMM'));
        }

        var ajax_data = {
            f_minprice: f_minprice,
            f_maxprice: f_maxprice,
            f_guests: f_guests,
            f_smart: f_smart,
            f_street: f_street,
            f_arrdate: moment(f_arrdate).format('YYYY-MM-DD '),
            f_depdate: moment(f_depdate).format('YYYY-MM-DD'),
        };

    return ajax_data;
}

function updatePricesBySmartID(sf_uid)
{
        var smarts = $.getSmarts();

        var f_minprice =  parseInt(smarts.items[sf_uid]["sf_minprice"]);
        var f_maxprice =  parseInt(smarts.items[sf_uid]["sf_maxprice"]);

        //Если крайние цены равны (например когда в смарте один объект) то разнозим диапазон вручную
        if(f_minprice == f_maxprice)
        {
            f_minprice -= 500;
            f_maxprice += 500;
        }

        //Update Filters Panel - Prices
        $("#filters-panel-price .value-min, #f-price-min-label").text(f_minprice);
        $("#filters-panel-price .value-max, #f-price-max-label").text(f_maxprice);
        $("#f-price-mid-label").text(((f_maxprice - f_minprice)/2 + f_minprice));

        //Update Sliders - Prices
        $(".f-price-slider").noUiSlider({
            start: [f_minprice, f_maxprice],
            step: 50,
            range: {
                'min': f_minprice,
                'max': f_maxprice
            },
        }, true);

}

function parseUrl() {

    var url = document.location.href;

    var parser = document.createElement('a');
    parser.href = url;
    var pathname = parser.pathname;

    var params = pathname.split("/"); 
    var pairs = new Array();

    for (var i = 0; i < params.length; i++) 
    { 
        if(i % 2 !== 0) pairs[ params[i] ] = params[i+1];
    }     

    return pairs;
}

function updateFiltersPanel() {

    var f_found = gmap_locs.length;
    var f_found_label = decOfNum( f_found, ['объект', 'объекта', 'объектов']);

    $("#filters-panel-found .value").text(f_found);
    $("#filters-panel-found .found-label").text(f_found_label);
    
}

function hideFilters() {
    $("#objects-filter-full").hide();
}

function openFilters() {
    $("#objects-filter").animate({
        height: "250px"
    }, 400 );

    $("#objects-filter-short").hide();
    $("#objects-filter-full").fadeIn(10);
}

function closeFilters() {
    $("#objects-filter").animate({
        height: "50px"
    }, 400);
    
    $("#objects-filter-full").hide();
    $("#objects-filter-short").fadeIn().animate(10);
}

//******************************************** Google Map functions ********************************

//   OPEN Object Map Bubble action
//*******************************************************************************
function showOverlay(index) {

    var lat = gmap_locs[index]['lat'];
    var lng = gmap_locs[index]['lng'];

    gmap.setCenter(lat + 0.001, lng);
    gmap.setZoom(14);

    gmap.removeOverlays();
    gmap.drawOverlay({
        lat: lat,
        lng: lng,
        content: $("#obj-tooltip-" + index).html(),
        verticalAlign: 'top',
        horizontalAlign: 'center',
        layer: "floatPane",
    });

    $('#objects-list .wrapper').scrollTo("#obj-item-box-" + index, 500, {
        // easing: 'elasout'
    });

}

//   CLOSE Object Map Bubble action
//*******************************************************************************
function hideOverlays() {
    gmap.removeOverlays();
    $(".thumbnail").removeClass("selected");
}

function highlightObjectItem(index) {
    $(".thumbnail").removeClass("selected");
    $("#obj-item-box-" + index + " .thumbnail").toggleClass("selected");
}

function showPanoramaBox(index) {

    var lat = gmap_locs[index]['details']['lat_strview'];
    var lng = gmap_locs[index]['details']['lng_strview'];
    var height = $("#objects-map").height();
    var width = $("#objects-map").width();
    var box = "#panorama-box";

    $(box).show();
    $(box).css("height", height).css("width", width).css("top", "0px").css("left", "0px");

    panorama = GMaps.createPanorama({
        el: box,
        lat: lat,
        lng: lng,
    });

}

function hidePanoramaBoxes(index) {
    $("#panorama-box").hide();
}

function gmapResize() {
    var height = $(window).height() - 59 - 50;
    var width = $("#objects-map").width() - 15 /* padding*/ ;

    $("#panorama-box").css("height", height).css("width", width).css("top", "0px").css("left", "0px");

    $("#objects-list, #objects-map").css("height", height);
    $("#map-canvas").css("width", width).css("height", height);

    gmap.refresh;


}

// Merge second object into first
function merge(set1, set2){
  for (var key in set2){
    if (set2.hasOwnProperty(key))
      set1[key] = set2[key];
  }
  return set1;
}



function gmapInit( ajax_data_add ) {

    $('#preloader').show();

    var windowHeight = $(window).height();
    var windowWidth = $("#objects-map").width();
    var clear_h = windowHeight - 59 - 50; // Высота верхнего фиксированного меню
    var clear_w = windowWidth - 15; // Высота верхнего фиксированного меню

    $("#objects-list, #objects-map").css("height", clear_h);
    $("#map-canvas").css("width", clear_w);
    $("#map-canvas").css("height", clear_h);

    //Processing Data for Ajax
    var ajax_data = {action: "getobjects"};    

    // CONSOLE LOG 
    // console.log(ajax_data_add.length);

    if(ajax_data_add !== "")
    {
        merge(ajax_data, ajax_data_add);
    }

    // CONSOLE LOG 
    // console.log(JSON.stringify(ajax_data_add));

    gmap = new GMaps({
        div: '#map-canvas',
        fitZoom: true,
        lat: 45.037754,
        lng: 39.007491,
        click: function(e) {
            // hideOverlays();
        },
        scrollwheel: false,
    });

    jQuery.ajax({
        
        url: '/objects/ajax/',
        dataType: 'json',
        data: ajax_data,

        success: function(data) {

            gmap_locs = data.locations;

            var template = $('#object_list_template').html();

            $("#objects-list .wrapper").html("");

            for (var i = 0; i < gmap_locs.length; i++) {

                var item = gmap_locs[i];
                var obj_images = "";
                var obj_images_overlay = "";

                //Images Slider
                //*****************************************************************************//
                if (item.details.images.x600.length) {

                    obj_images = '<ul class="bxslider" id="bxslider' + item.details.index + '">';

                    $.each(item.details.images.x600, function(index, value) {
                        obj_images += "<li><a href='/objects/view/uid/" + item.details.uid + "'><img src='" + value + "'></a></li>";
                    });

                    obj_images += "</ul>";
                } else {

                    obj_images = '<ul class="bxslider" id="bxslider' + item.details.index + '"><li><img src="/images/ui/no-image.png"></li></ul>';
                }

                // Map Overlay Images Slider
                //*****************************************************************************//
                if (item.details.images.x200.length) {

                    obj_title_image = item.details.images.x200[0];

                } else {

                    obj_title_image = '/images/ui/no-image.png';
                }

                var content = template
                    .replace(/{{obj_name}}/g, item.title)
                    .replace(/{{obj_image}}/g, item.details.image)
                    .replace(/{{obj_id}}/g, item.details.id)
                    .replace(/{{obj_uid}}/g, item.details.uid)
                    .replace(/{{obj_index}}/g, i)
                    .replace(/{{obj_rating}}/g, item.details.rating)
                    .replace(/{{obj_lat}}/g, item.lat)
                    .replace(/{{obj_lng}}/g, item.lng)
                    .replace(/{{obj_images}}/g, obj_images)
                    .replace(/{{obj_title_image}}/g, obj_title_image)
                    .replace(/{{obj_guests}}/g, item.details.guests)
                    .replace(/{{obj_sqr}}/g, item.details.sqr)
                    .replace(/{{obj_price}}/g, item.details.price)
                    .replace(/{{obj_stayperiod}}/g, item.details.stayperiod)
                    .replace(/{{obj_rooms}}/g, item.details.rooms);

                $("#objects-list .wrapper").append(content);

                if ($.fn.bxSlider) {
                    var slider = $('#bxslider' + item.details.index).bxSlider({
                        minSlides: 1,
                        maxSlides: 1,
                        moveSlides: 1,
                        slideMargin: 0,
                        infiniteLoop: false,
                        hideControlOnEnd: true,
                        pager: false,
                        auto: false,
                        autoControls: false,
                    });
                } else {
                    alert("bxSlider not installed");
                }

                //Подсвечиваем булавки разным цветом в зависимости от типа объекта
                var iconColor;

                if (item.details.ot_code == "APT") iconColor = '#FF6E28'; //Апартаменты
                if (item.details.ot_code == "FLA") iconColor = '#51B64F'; //Квартиры

                var svgIcon = {
                    // path: 'M67.463,129.923C54.3,148.789,52.398,164.73,52.398,164.73c-0.292,1.316-1.462,1.316-1.755,0  c0,0-2.486-15.941-15.356-34.808c-10.676-14.918-15.502-20.769-15.502-32.907c0-17.404,13.894-31.298,31.444-31.298  c17.404,0,31.737,13.894,31.737,31.298C82.966,109.154,73.605,121.001,67.463,129.923z M63.66,97.601  c0-6.728-5.265-11.992-12.139-11.992c-6.436,0-11.847,5.265-11.847,11.992s5.411,12.432,11.847,12.432  C58.396,110.032,63.66,104.328,63.66,97.601z',
                    path: 'M75.992,29.955C75.992,15.074,63.908,3,48.996,3C34.081,3,22,15.074,22,29.955   c0,5.57,1.689,10.74,4.582,15.039l22.804,53.191l22.018-53.191C73.742,40.746,75.992,35.525,75.992,29.955z M48.712,47.646   c-10.227,0-18.522-8.487-18.522-18.957c0-10.463,8.296-18.95,18.522-18.95c10.23,0,18.526,8.487,18.526,18.95   C67.238,39.16,58.942,47.646,48.712,47.646z',
                    fillColor: iconColor,
                    fillOpacity: 1,
                    // anchor: new google.maps.Point(50, 168),
                    anchor: new google.maps.Point(48, 96),
                    scale: 0.3,
                    strokeWeight: 0
                };

                gmap.addMarker({
                    lat: item.lat,
                    lng: item.lng,
                    title: item.name,
                    icon: svgIcon,
                    zIndex: 1,
                    click: (function(t) {
                        return function() {

                            showOverlay(t.details.index);
                            highlightObjectItem(t.details.index);

                        };
                    })(item)
                });

            }

            gmap.fitZoom();

            $('#preloader').hide();

            // Count items below folding
            //************************************************************
/*            var $items = $('#objects-list .thumbnail');
            var items_all = $items.length;
            var win = $("#objects-list .wrapper");
            var winOffset = $(document).height() - win.height();
            var objCounter = $("#obj-items-counter .value");

            $( "#objects-list .wrapper" ).scroll(function() {

                var winPos = win.scrollTop() + win.height();

                $("#win_params").text("winScrlTop: " + win.scrollTop() + " winH: " + win.height() + " winPos: " + winPos );

                var items_in_view = 0;
                
                jQuery.each( $items, function() {
                    
                    var elPos = $(this).offset().top - winOffset + $(this).height() + win.scrollTop();
                    
                    $(".obj-pos" ,this).text(elPos);
 
                    if( winPos > elPos ) {
                        items_in_view++;
                    }

                });                
                
                objCounter.text(items_all - items_in_view);

            });
*/
            //   Update Filters panel
            //*******************************************************************************
            updateFiltersPanel();



        } // -- success --
    });


}
