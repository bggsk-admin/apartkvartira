/***************************************************************************
jQuery
****************************************************************************/
var gmap_locs;
var gmap;
$(document).ready(function() {
    //   Hide Em All
    //*******************************************************************************
    hidePanoramaBoxes();
    //   Typehead
    //*******************************************************************************
    $("#f_street").typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        // name: 'states',
        displayKey: 'name',
        source: function(query, process) {
            return $.post('/objects/ajax', {
                action: "getstreets",
                query: query
            }, function(data) {
                return process(data.items);
            }, 'json');
        },
        templates: {
            empty: ['<div class="tty-empty-message">', 'Ни одной улицы не найдено', '</div>'].join('\n'),
            suggestion: Handlebars.compile('<p>{{type_short}}. <strong>{{name}}</strong> <span class="tty-district">{{district}}</span> <span class="pull-right">{{objects}} </span></p>')
        },
    });
    //   HeapBox - Smartfilter
    //*******************************************************************************
    var smarts = $.getSmartsForSelect();
    $("#f_smart").heapbox({
        'effect': {
            'type': 'slide',
            'speed': 'fast'
        },
    });
    var json = JSON.stringify(smarts.items);
    $("#f_smart").heapbox("set", json);
    $("#f_smart").heapbox("select", "apt-all");
    
    //   HeapBox - Guests
    //*******************************************************************************
    $("#f_guests").heapbox({
        'effect': {
            'type': 'slide',
            'speed': 'fast'
        },
    });

    //   Validate Search form
    //*******************************************************************************
    var search_form = $("#main-search-form");
    if ($.fn.validate) {
        search_form.validate({
            rules: {
                f_arrdate: {
                    required: true
                },
                f_depdate: {
                    required: true
                },
            },
            showErrors: function(errorMap, errorList) {
                $.each(this.validElements(), function(index, element) {
                    var $element = $(element);
                    $element.data("title", "").tooltip("destroy");
                });
                $.each(errorList, function(index, error) {
                    var $element = $(error.element);
                    $element.tooltip("destroy").data("title", error.message).tooltip({
                        placement: 'top',
                        trigger: 'manual',
                        show: '100'
                    }).tooltip('show');
                });
            },
        });
    } else {
        alert("Validate method not loaded");
    }
    //   Generate URL when search form submit action
    //*******************************************************************************
    search_form.submit(function(event) {
        // event.preventDefault();
        if (search_form.valid()) {
            // var location = search_form.attr("action");
            var location = "/objects/index";
            var f_street = $("#f_street").val();
            var f_arrdate = $("#f_arrdate").datepicker("getDate");
            f_arrdate = moment(f_arrdate).format("YYYY-MM-DD");
            var f_depdate = $("#f_depdate").datepicker("getDate");
            f_depdate = moment(f_depdate).format("YYYY-MM-DD");
            var f_guests = $("#f_guests").val();
            var f_smart = $("#f_smart").val();
            if (f_street) location = location + "/f_street/" + f_street;
            if (f_arrdate) location = location + "/f_arrdate/" + f_arrdate;
            if (f_depdate) location = location + "/f_depdate/" + f_depdate;
            if (f_guests) location = location + "/f_guests/" + f_guests;
            if (f_smart) location = location + "/f_smart/" + f_smart;
            // console.log(location);
            search_form.attr("action", location);
        }
        return true;
    });
    //   Scrollspy
    //*******************************************************************************
    $('body').scrollspy({
        target: '.navbar-collapse',
        offset: 55
    });
    //   Inner Link scroll
    //*******************************************************************************
    $(".inner-link").click(function(e) {
        e.preventDefault();
        $('.dropdown.open .dropdown-toggle').dropdown('toggle');
        $('html,body').animate({
            scrollTop: $($(this).attr("href")).offset().top - 55
        }, 500);
        return false;
    });
    $(".my-collapse").click(function(e) {
        e.preventDefault();
        $($(this).attr('data-target')).slideToggle();
    });


    //   Dates Range Calendar
    //*******************************************************************************
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth();
    var yyyy = today.getFullYear();

    $.datepicker.regional['ru-RU'] = {
        closeText: 'Готово',
        currentText: 'Сегодня',
        dateFormat: 'd M yy',
        dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayNamesShort: ['Вос', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб'],
        firstDay: 1,
        isRTL: false,
        minDate: new Date(yyyy, mm, dd),
        monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
        monthNamesShort: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
        nextText: '&nbsp;',
        prevText: '&nbsp;',
        showMonthAfterYear: false,
        weekHeader: 'Wk',
        yearSuffix: '',

        beforeShow: function() {
            $("#ui-datepicker-div").addClass("ui-calendar-default");
        },

        beforeShowDay: function(date) {

            // var dateCurr = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            var
                dateFrom = $("#f_arrdate").datepicker("getDate"),
                dateTo = $("#f_depdate").datepicker("getDate");

            if (date >= dateFrom && date <= dateTo) {
                return [true, 'range_day', ''];
            }

            return [true, '', ''];

        },

        onSelect: function() {

            var a = $(this).attr("id");
            var dateFrom = $("#f_arrdate").datepicker("getDate");
            var dateTo = $("#f_depdate").datepicker("getDate");

            if (a == "f_arrdate" && !dateTo) {

                var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() + 2);
                $("#f_depdate").datepicker("setDate", newDate);
            }


        },
    };

    $.datepicker.setDefaults($.datepicker.regional['ru-RU']);

    $("#f_arrdate").datepicker({
        // changeMonth: false,
        numberOfMonths: 2,
        inline: true,
        // showOtherMonths: true,

        onClose: function(selectedDate) {

            var dateFrom = $("#f_arrdate").datepicker("getDate");
            var dateTo = $("#f_depdate").datepicker("getDate");

            if (dateFrom) {
                var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() + 2);

                $("#f_depdate").datepicker("option", "minDate", newDate);
                $("#f_depdate").datepicker("show");
            }
        },

    });
    // .datepicker('widget').wrap('<div class="ll-skin-melon"/>');

    $("#f_depdate").datepicker({
        // changeMonth: true,
        numberOfMonths: 2,
        inline: true,
        // showOtherMonths: true,

        onClose: function(selectedDate) {

            var dateFrom = $("#f_arrdate").datepicker("getDate");
            var dateTo = $("#f_depdate").datepicker("getDate");
            var newDate = new Date(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate() - 2);

            if (!dateFrom) {
                $("#f_arrdate").datepicker("setDate", newDate);
                $("#f_arrdate").datepicker("show");

            }

            //$("#search_date_from").datepicker("option", "maxDate", selectedDate);
        },

    });
    // .datepicker('widget').wrap('<div class="ll-skin-melon"/>');

    $('.input-range').focus(function() {
        $(this).addClass("active");
    }).focusout(function() {
        $(this).removeClass("active");
    });

    /* $(function() {
        
        var cur = -1, prv = -1;

        $dp_box = $("#datepicker-overlay");

        var dp = $dp_box.datepicker({

            numberOfMonths: 3,
            showButtonPanel: true,

            dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            dayNamesShort: ['Вос', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб'],

            monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
            monthNamesShort: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],

            firstDay: 1,
            isRTL: false,
            minDate: new Date(yyyy, mm, dd),

            beforeShowDay: function(date) {
                return [true, ((date.getTime() >= Math.min(prv, cur) && date.getTime() <= Math.max(prv, cur)) ? 'date-range-selected' : '')];
            },

            onSelect: function(dateText, inst) {
                
                var d1, d2, arrdate, depdate;
                prv = cur;
                cur = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();
                
                if (prv == -1 || prv == cur) {
                    
                    prv = cur;

                    arrdate = moment(dateText).format("D MMMM YY");
                    $('#f_arrdate').val(arrdate);

                } else {
                    
                    d1 = $.datepicker.formatDate('dd M yy', new Date(Math.min(prv, cur)), {});
                    d2 = $.datepicker.formatDate('mm/dd/yy', new Date(Math.max(prv, cur)), {});
                    
                    arrdate = moment(new Date(Math.min(prv, cur))).format("D MMMM YYYY");
                    depdate = moment(new Date(Math.max(prv, cur))).format("D MMMM YYYY");

                    $('#f_arrdate').val(arrdate);
                    $('#f_depdate').val(depdate);

                }
            },
            onChangeMonthYear: function(year, month, inst) {
                //prv = cur = -1;
            },
            
            onAfterUpdate: function(inst) {
                $('<button type="button" class="btn btn-success" data-handler="hide" data-event="click">Применить</button>').appendTo($('#select-date-range div .ui-datepicker-buttonpane')).on('click', function() {
                    $('#select-date-range div').hide();
                });
            }

        }).position({
        
            my: 'left top',
            at: 'left bottom',
            of: $('#f_arrdate')
        
        }).hide();

        $('#f_arrdate').on('focus', function(e) {
            
            var v = this.value, d;
            
            try {
                if (v.indexOf(' - ') > -1) {
            
                    d = v.split(' - ');
                    prv = $.datepicker.parseDate('mm/dd/yy', d[0]).getTime();
                    cur = $.datepicker.parseDate('mm/dd/yy', d[1]).getTime();
            
                } else if (v.length > 0) {
            
                    prv = cur = $.datepicker.parseDate('mm/dd/yy', v).getTime();
            
                }
            } catch (e) {
                cur = prv = -1;
            }



            if (cur > -1) 
                $("#datepicker-overlay").datepicker('setDate', new Date("2018/09/19"));
            
            $("#datepicker-overlay").datepicker('refresh').show();
        });

    });*/

    //   GMap: Init action
    //*******************************************************************************
    google.maps.event.addDomListener(window, 'load', gmapInit());
    google.maps.event.addDomListener(window, 'resize', function() {
        var height = $("#main-map-canvas").height();
        var width = $("#main-map-canvas").width();
        $("#panorama-box").css("height", height).css("width", width).css("top", "32px").css("left", "95px");
        // gmap.refresh;
    });

    //   OPEN Object Map Bubble action
    //*******************************************************************************
    $(document).on('click', '.obj-title', function(event) {
        event.preventDefault();
        var index = $(this).attr('data-obj-index');
        showOverlay(index);
        hidePanoramaBoxes();
    });

    //   CLOSE Object Map Bubble action
    //*******************************************************************************
    $(document).on('click', '.overlay-box-close', function(event) {
        event.preventDefault();
        hideOverlays();
    });
    //   OPEN StreetView Panorama action
    //*******************************************************************************
    $(document).on('click', '.open-panorama', function(event) {
        event.preventDefault();
        var index = $(this).attr('data-obj-index');
        showOverlay(index);
        showPanoramaBox(index);
    });
    //   CLOSE StreetView Panorama action
    //*******************************************************************************
    $(document).on('click', '.close-panorama', function(event) {
        event.preventDefault();
        hidePanoramaBoxes();
    });


}); //JQuery END


jQuery.extend({
    getSmarts: function() {
        var result = null;
        $.ajax({
            url: '/objects/ajax/',
            type: 'get',
            data: {
                action: "getsmarts"
            },
            dataType: 'json',
            async: false,
            cache: false,
            success: function(data) {
                result = data;
            }
        });
        // console.log(result);
        return result;
    },
    getSmartsForSelect: function() {
        var result = null;
        $.ajax({
            url: '/objects/ajax/',
            type: 'get',
            data: {
                action: "getsmartsselect",
                ot_code: "APT",
            },
            dataType: 'json',
            async: false,
            cache: false,
            success: function(data) {
                result = data;
            }
        });
        // console.log(result);
        return result;
    }
});

//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд']) 
function decOfNum(number, titles)  
{  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}  

// ******************************************** Google Map functions ********************************
//   OPEN Object Map Bubble action
//*******************************************************************************
function showOverlay(index) {
    var lat = gmap_locs[index]['lat'];
    var lng = gmap_locs[index]['lng'];
    gmap.setCenter(lat + 0.001, lng);
    gmap.setZoom(14);
    gmap.removeOverlays();
    gmap.drawOverlay({
        lat: lat,
        lng: lng,
        content: $("#obj-tooltip-" + index).html(),
        verticalAlign: 'top',
        horizontalAlign: 'center',
        layer: "floatPane",
    });
}

//  CLOSE Object Map Bubble action
//*******************************************************************************
function hideOverlays() {
    gmap.removeOverlays();
}

function showPanoramaBox(index) {
    var lat = gmap_locs[index]['details']['lat_strview'];
    var lng = gmap_locs[index]['details']['lng_strview'];
    var height = $("#main-map-canvas").height();
    var width = $("#main-map-canvas").width();
    var box = "#panorama-box";
    console.log(height);
    console.log(width);
    $(box).show();
    $(box).css("height", height).css("width", width).css("top", "32px").css("left", "95px");
    panorama = GMaps.createPanorama({
        el: box,
        lat: lat,
        lng: lng,
    });
}

function hidePanoramaBoxes(index) {
    $("#panorama-box").hide();
}

function gmapInit() {
    gmap = new GMaps({
        div: '#main-map-canvas',
        fitZoom: true,
        lat: 45.037754,
        lng: 39.007491,
        click: function(e) {
            hideOverlays();
        },
        scrollwheel: false,
    });
    jQuery.ajax({
        url: '/objects/ajax',
        dataType: 'json',
        data: {
            action: 'getobjects'
        },
        success: function(data) {
            gmap_locs = data.locations;
            var template = $('#object_list_template').html();
            $("#objects-list .wrapper").html("");
            for (var i = 0; i < gmap_locs.length; i++) {
                var item = gmap_locs[i];
                var obj_images = "";
                var obj_images_overlay = "";
                // Map Overlay Images Slider
                //*****************************************************************************//
                if (item.details.images.x300.length) {
                    obj_title_image = item.details.images.x300[0];
                } else {
                    obj_title_image = '/images/ui/no-image.png';
                }
                var content = template.replace(/{{obj_name}}/g, item.title).replace(/{{obj_image}}/g, item.details.image).replace(/{{obj_id}}/g, item.details.id).replace(/{{obj_uid}}/g, item.details.uid).replace(/{{obj_index}}/g, i).replace(/{{obj_lat}}/g, item.lat).replace(/{{obj_lng}}/g, item.lng).replace(/{{obj_images}}/g, obj_images).replace(/{{obj_title_image}}/g, obj_title_image).replace(/{{obj_guests}}/g, item.details.guests).replace(/{{obj_sqr}}/g, item.details.sqr).replace(/{{obj_price}}/g, item.details.price).replace(/{{obj_stayperiod}}/g, item.details.stayperiod).replace(/{{obj_rooms}}/g, item.details.rooms);
                $("#objects-list .wrapper").append(content);
                var iconColor;
                
                if (item.details.ot_code == "APT") iconColor = '#FF6E28'; //Апартаменты
                if (item.details.ot_code == "FLA") iconColor = '#51B64F'; //Квартиры
                // if (item.details.ot_code == "AFL") iconColor = '#f15a23'; //Апартамент - Квартира
                // if (item.details.ot_code == "HTL") iconColor = '#e12f49'; //Отель
                // if (item.details.ot_code == "HSL") iconColor = '#63cac5'; //Хостел
                
                var svgIcon = {
                    path: 'M75.992,29.955C75.992,15.074,63.908,3,48.996,3C34.081,3,22,15.074,22,29.955   c0,5.57,1.689,10.74,4.582,15.039l22.804,53.191l22.018-53.191C73.742,40.746,75.992,35.525,75.992,29.955z M48.712,47.646   c-10.227,0-18.522-8.487-18.522-18.957c0-10.463,8.296-18.95,18.522-18.95c10.23,0,18.526,8.487,18.526,18.95   C67.238,39.16,58.942,47.646,48.712,47.646z',
                    fillColor: iconColor,
                    fillOpacity: 1,
                    anchor: new google.maps.Point(48, 96),
                    scale: 0.3,
                    strokeWeight: 0
                };
                gmap.addMarker({
                    lat: item.lat,
                    lng: item.lng,
                    title: item.name,
                    icon: svgIcon,
                    zIndex: 1,
                    click: (function(t) {
                        return function() {
                            var lat_shift = t.lat + 0.008;
                            gmap.setCenter(lat_shift, t.lng);
                            gmap.setZoom(14);
                            gmap.removeOverlays();
                            gmap.drawOverlay({
                                lat: t.lat,
                                lng: t.lng,
                                content: $("#obj-tooltip-" + t.details.index).html(),
                                verticalAlign: 'top',
                                horizontalAlign: 'center',
                                layer: "floatPane",
                            });
                        };
                    })(item)
                });
            }
            gmap.fitZoom();
        } // -- success --
    });
}
