/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

        $(".btn-submit").click(function(e) {
                $('#main-form').submit();
        });

        /*    bootbox.setDefaults({
        locale: "ru",
    });
*/


        //-------------------------------------------------- Delete confirmation -----------------------------------/
        /*    $(".delete-confirm").click(function(e) {
        e.preventDefault();

        var location = $(this).attr('href');
        var source = $(this).attr('data-source');

        bootbox.confirm("<i class='ico-trash'></i> Это действие не может быть отменено. Вы уверены что хотите удалить этот элемент ?", function(confirmed) {
            console.log("Confirmed: " + confirmed);

            if(confirmed)
            {
                if(typeof source === "undefined") {
                    window.location.replace(location);
                } else {

                    //$.post(location, $('#' + source).serialize(),
                    //    function(data, status, xhr){}
                    //);
                    window.location.replace(location + '?' + $('#' + source).serialize());
                }
            }
        });
    });
*/
        //-------------------------------------------------- Delete confirmation -----------------------------------/


        /*    $(".import-data").click(function(e) {
        e.preventDefault();

        var location = $(this).attr('href');
        var tmpl = '#' + $(this).attr('data-tmpl');
        var title = ($(tmpl + ' .title').html());
        var message = ($(tmpl + ' .message').html());

        bootbox.dialog({
            title: title,
            message: message,
            animate: true,
            buttons: {
                success: {
                    label: "Импортировать",
                    className: "btn-success",
                    callback: function() {

                        $(tmpl + ' form').attr("action", location).submit();
                        window.location.replace(location + '?' + $('#import_data').serialize());
                    }
                },
                "Отмена": {
                    label: "Отмена",
                    className: "btn-danger",
                    callback: function() {}
                },
            },

        });



    });
*/

        //-------------------------------------------------- Ajax context setup -----------------------------------/
        /*    $.ajaxSetup({
        beforeSend:function (xhr, settings) {
            if (settings.context != undefined && settings.context.hasClass('btn')) {
                settings.context.button('loading');
            }
        },
        complete:function () {
            this.button('reset');
        }
    });
    */


        //-------------------------------------------------- Append modals to the end -----------------------------------/
        $('.modal').appendTo("body");
        /*    $('.modal button[type=submit]').click(function(ev){
    $("#" + $(this).attr('action-form')).submit();
    return false;
    });
    */
        //-------------------------------------------------- Preloader -----------------------------------/
        $(window).load(function() { // makes sure the whole site is loaded
                $('#status').fadeOut(); // will first fade out the loading animation
                $('#preloader').delay(500).fadeOut('fast'); // will fade out the white DIV that covers the website.
                $('body').delay(500).css({
                        'overflow': 'visible'
                });
        });


        //-------------------------------------------------- jQuery Navgoco Menus Plugin v0.1.5 -----------------------------------/
        $(".gn-menu").navgoco({
                accordion: true,
                caret: '<i class="ico-chevron-down"></i>',
                save: true,
                cookie: {
                        name: 'navgoco',
                        expires: false,
                        path: '/'
                },

                slide: {
                        duration: 400,
                        easing: 'swing'
                }
        });

        //-------------------------------------------------- jQuery Select2 -----------------------------------/
        $(".select2").select2({
                allowClear: true
        });

        //-------------------------------------------------- Sub-action link -----------------------------------/
        $("a.sub-action-link").click(function() {
                $("#sub-action-form").attr("action", $(this).attr("href"));
                $("#sub-action-form").submit();
                return false;
        });

        //-------------------------------------------------- Bootstrap Modal -----------------------------------/
        /*    $('a.modal-triger').click(function(ev){

    $('#sub-action-form').attr('action', $(this).attr('href'));
    $('#'+$(this).attr('data-modal')).modal({show:true, keyboard: true});
    return false;
    });
    */

        //-------------------------------------------------- Tabs-Radio-Selector -----------------------------------/
        //Set first tab active
        // $('#subnavbar-tabs input:first').tab('show');
        // $('#subnavbar-tabs label:first').toggleClass('active');

        $('#subnavbar-tabs label, #calendar-view-tabs label').click(function(e) {
                $("input", this).tab('show');
        });

        //-------------------------------------------------- Shift+Click Select checkboxes -----------------------------------/
        var lastChecked = null;
        var $chkboxes = $('.chkbx');

        $chkboxes.click(function(e) {
                if (!lastChecked) {
                        lastChecked = this;
                        return;
                }

                if (e.shiftKey) {
                        var start = $chkboxes.index(this);
                        var end = $chkboxes.index(lastChecked);

                        $chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).attr('checked', lastChecked.checked);

                }

                lastChecked = this;

                $.each($chkboxes, function() {
                        if ($(this).is(":checked")) {
                                $(this).parent().parent().addClass('success');
                        } else {
                                $(this).parent().parent().removeClass('success');
                        }
                });

        });


        //-------------------------------------------------- jQuery Pretty Checkable -----------------------------------/
        $('input.pretty').prettyCheckable({
                labelPosition: 'right'
        });


        $('.form-help').tooltip({
                trigger: 'hover focus',
                placement: 'auto',
                container: 'body'
        });

        $('.toggle_item').hide();
        $('.toggle').click(function() {
                $(this).nextAll(".toggle_item:first").slideToggle('slow', function() {});
                return false;
        });

}); //JQuery END

/***************************************************************************
Pure JS
****************************************************************************/

//-------------------------------------------------- Toggle items -----------------------------------/

function toggleItem(className) {
        $('.' + className).slideToggle('slow', function() {});
}

//-------------------------------------------------- Google Nexus Menu -----------------------------------/
new gnMenu(document.getElementById('gn-menu'));

//-------------------------------------------------- Ajax Loader -----------------------------------/
/*var opts = {
size: 20,           // Width and height of the spinner
factor: 0.1,       // Factor of thickness, density, etc.
color: "#000",      // Color #rgb or #rrggbb
speed: 1.0,         // Number of turns per second
clockwise: true     // Direction of rotation
};
var ajaxLoader = new AjaxLoader("spinner", opts);
ajaxLoader.show();
*/

//-------------------------------------------------------------------------------------/

function repeat(s, n) {
        var a = [];
        while (a.length < n) {
                a.push(s);
        }
        return a.join('');
}

//-------------------------------------------------------------------------------------/

function checkAll(oForm, cbName, checked) {
        if (oForm[cbName].length == undefined) oForm[cbName].checked = checked;
        else {
                for (var i = 0; i < oForm[cbName].length; i++) oForm[cbName][i].checked = checked;
        }
}

//-------------------------------------------------------------------------------------/

function getURLVar(a) {
        var b = String(document.location).toLowerCase().split('?'),
                c = '';
        if (b[1]) {
                var d = b[1].split('&');
                for (var i = 0; i <= d.length; i++) {
                        if (d[i]) {
                                var e = d[i].split('=');
                                if (e[0] && e[0] == a.toLowerCase()) {
                                        c = e[1];
                                }
                        }
                }
        }
        return c;
}

//-------------------------------------------------------------------------------------/
var ru2en = {
        ru_str: 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя,.; "/',
        en_str: ['a', 'b', 'v', 'g', 'd', 'e', 'jo', 'zh', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't',
                'u', 'f', 'h', 'c', 'ch', 'sh', 'shh', '', 'i', '', 'je', 'ju',
                'ja', 'a', 'b', 'v', 'g', 'd', 'e', 'jo', 'zh', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f',
                'h', 'c', 'ch', 'sh', 'shh', '', 'i', '', 'je', 'ju', 'ja', '', '', '', '-', '', '-'
        ],
        translit: function(org_str) {
                var tmp_str = "";
                for (var i = 0, l = org_str.length; i < l; i++) {
                        var s = org_str.charAt(i),
                                n = this.ru_str.indexOf(s);
                        if (n >= 0) {
                                tmp_str += this.en_str[n];
                        } else {
                                tmp_str += s;
                        }
                }
                return tmp_str;
        }
}

//-------------------------------------------------------------------------------------/

        function setTranslit(text) {
                if (text.length) return ru2en.translit(text);
                else return false;
        }
