/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

    //   Close dropdown on click outside
    //*******************************************************************************
    var $menu = $('.dropdown-menu');
    var $menu_toggle = $('.dropdown-toggle');
    
    $("body").on('click', function(e) {
        if ($menu.is(':visible') && !$menu_toggle.is(e.target)) {
            $(".dropdown").removeClass("open");
        }
    });


    $('.home-dropdown').dropit({ action: 'mouseenter' });

}); //JQuery END

/***************************************************************************
Pure JS
****************************************************************************/
